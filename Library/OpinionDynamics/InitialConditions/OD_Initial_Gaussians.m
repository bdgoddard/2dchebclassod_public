function rhoI = OD_Initial_Gaussians(y,params)
%rhoI = M_1*exp(-alpha_1*(y-y0_1).^2) + M_2*exp(-alpha_2*(y-y0_2).^2);

    alpha_1 = params.alpha_1;
    alpha_2 = params.alpha_2;
    y0_1 = params.y0_1;
    y0_2 = params.y0_2;
    M_1 = params.M_1;
    M_2 = params.M_2;

    yTilde_1 = y-y0_1;
    yTilde_2 = y-y0_2;
    
    if(isfield(params,'periodic') && params.periodic)
        yTilde_1 = periodicDistance(yTilde_1,params.periodicL);
        yTilde_2 = periodicDistance(yTilde_2,params.periodicL);
    end

    rhoI = M_1*exp(-alpha_1*yTilde_1.^2) + M_2*exp(-alpha_2*yTilde_2.^2);
    
    
end