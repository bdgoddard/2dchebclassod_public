function rhoI = OD_Initial_Gaussian(y,params)
%rhoI = exp(-alpha*(y-y0).^2);

    alpha = params.alpha;
    y0 = params.y0;
    
    yTilde = y-y0;
    
    if(isfield(params,'periodic') && params.periodic)
        yTilde = periodicDistance(yTilde,params.periodicL);
    end

    rhoI = exp(-alpha*yTilde.^2);
    
    
end