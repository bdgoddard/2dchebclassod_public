 function MOD = OD_Generate_Example(Example,Phys_Area,Params)
% OD_GENERATE_EXAMPLE Generate opinion dynamics example
% 
% Outputs:
% MOD      - a MultiOpinionDynamics containing the results
%
% Inputs:
% Example   - function outputting opts, optsPlot and doBCs
% Phys_Area - structure to set up the computation domain 
% Params    - parameters to be passed to Example
 
    %----------------------------------------------------------------------
    % Determine output sub-directory
    %----------------------------------------------------------------------

    if(strcmp(Phys_Area.shape,'SpectralLine'))
        BC = 'NoFlux';
    elseif(Phys_Area.even2periodic)
        BC = 'Even2Periodic';
    else
        BC = 'Periodic';
    end

    %----------------------------------------------------------------------
    % Get structure of options from Example
    %----------------------------------------------------------------------

    ExampleFn = str2func(Example);
    Params.BC = BC;
    optsFull = ExampleFn(Params);

    %----------------------------------------------------------------------
    % Set up computations for each set of options from Example
    %----------------------------------------------------------------------

    % number of computations
    nOpts = length(optsFull);  
        
    % output options
    optsOut = struct;
    
    % loop over computations
    for iOpts = 1:nOpts
    
        %------------------------------------------------------------------
        % Set options or the default values for optsNum
        %------------------------------------------------------------------
        
        % get computation optsion
        opts = optsFull(iOpts);
        
        % set Plot_Area, including a default
        if(isfield(opts,'Plot_Area'))
            Plot_Area = opts.Plot_Area;
        else
            Plot_Area = struct('N',400,'yMin',0,'yMax',1);
        end
        
        % set global directories for PDE kernel convolutions and SDE 
        % initial and radical sampling.  These are independent of the
        % dyamics and relatively expensive to compute
        kernelDirGlobal = ['..' filesep '2DChebClassData' filesep 'OpinionDynamicsPaper' ...
                           filesep '_Kernels' filesep BC];

        samplingDirGlobal = ['..' filesep '2DChebClassData' filesep 'OpinionDynamicsPaper' ...
                           filesep '_Sampling'];
                       
        % set ODE tolerances
        if(~isfield(opts,'relTol'))
            relTol = 10^-6;
        else
            relTol = opts.relTol;
        end
        if(~isfield(opts,'absTol'))
            absTol = 10^-6;
        else
            absTol = opts.absTol;
        end
        
        % set number of points for PDE kernel intervals
        if(~isfield(opts,'kernelN'))
            kernelN = 100;
        else
            kernelN = opts.kernelN;
        end
        
        % set up optsNum
        optsNum = struct('PhysArea',Phys_Area, ...
                         'PlotArea',Plot_Area, ...                  
                         'plotTimes',opts.plotTimes, ...
                         'dataDir','', ...
                         'dataSubDir',BC, ... % subdirectory labelled by BC
                         'kernelDirGlobal',kernelDirGlobal, ...
                         'relTol',relTol,'absTol',absTol, ...
                         'kernelN',kernelN);

        % plotTimes now in optsNum
        opts = rmfield(opts,'plotTimes');
        
        % add global directory for sampling
        if(isfield(opts,'optsStoc'))
            optsNum.samplingDirGlobal = samplingDirGlobal;
        end
                        
        % determine if we're stopping PDE computations once they've reached
        % equilibrium, and set the tolerance (default is 100*absTol)
        if(isfield(opts,'toEq'))
            optsNum.toEq = opts.toEq;
            if(optsNum.toEq && isfield(opts,'eqTol'))
                optsNum.eqTol = opts.eqTol;
            end
        end
        
        % set optsNum output
        opts.optsNum = optsNum;

        %------------------------------------------------------------------
        % Set options or the default values for optsPhys
        %------------------------------------------------------------------

        % set default for optsPhys, if it doesn't exist
        if(~isfield(opts,'optsPhys'))
            opts.optsPhys = struct;
        end    
        
        % ensure that the IC and radicals are specified as if on a periodic
        % domain, i.e. that the distributions are the same for all BCs
        opts.optsPhys.optsInitial.periodic = true;
        if(isfield(opts.optsPhys,'optsRadicals'))
            opts.optsPhys.optsRadicals.periodic = true;
        end
        
        %------------------------------------------------------------------
        % Assign output
        %------------------------------------------------------------------
                
        % determine if computations have loopVars for generating a set of
        % options in MultiOpinionDynamics
        doLoopVars = isfield(optsFull(1),'loopVars'); 
 
        if(doLoopVars && nOpts>1)
            error('Can only define loopVars for one set of options');
        end
        
        if(doLoopVars) % options have loopVars
            % put into alphabetic order for save/load consistency
            fieldList = {opts.loopVars.field};
            [~,sortOrder]=sort(fieldList);
            opts.loopVars = opts.loopVars(sortOrder);
            optsOut = opts;
        else % multiple sets of options
            optsOut.OD(iOpts) = opts;
            optsOut.optsNum.dataDir = '';
        end
    end
    
        
    %----------------------------------------------------------------------
    % Run and save MultiOpinionDynamics
    %----------------------------------------------------------------------
    
    MultiDataDir = [optsNum.dataSubDir filesep 'Multi'];
    optsOther.SkipLoadQuestion = true;
    MOD = DataStorage(MultiDataDir,@DoMultiOpinionDynamics,optsOut,optsOther,[],[],false);
    
end                 

