function OD_Plot_BC_Examples(MOD,optsPlot)
% OD_PLOT_BC_EXAMPLES Generate plots for OD_Generate_BC_Examples
%
% Inputs:
% MOD      - MultiOpinionDynamics structure
% optsPlot - structure of plotting options
 
    %----------------------------------------------------------------------
    % List of functions
    %----------------------------------------------------------------------

    % list of functions that run on the individual computations, stored in
    % MOD(iMOD).Comps(iComp)
    fns = {'PlotSnapshotsDensityRadicals','PlotOrderParameter','PlotMass','PlotDensityOrderParameter', ...
           'PlotAsymptotics','PlotTimeSpaceDensity', ...
           'PlotMeanOpinion','PlotModeOpinion', 'PlotMeanModeOpinion'...
           'PlotDensityMeanOpinion','PlotDensityModeOpinion','PlotDensityMeanModeOpinion'};

    % list of functions that run on the whole MOD(iMOD)
    fnsMulti = {'PlotOrderParameterFinal', 'PlotEqTime','PlotOrderParameterFinalEqTime', ...
           'PlotDensityOrderParameterFinal','PlotDensityOrderParameterFinalEqTime', ...
           'PlotMeanOpinionFinal','PlotModeOpinionFinal', ...
           'PlotDensityOrderParameterMeanOpinionFinal','PlotDensityOrderParameterModeOpinionFinal'};

	% force saving these outputs as pngs (due to file size)
    pngList = {'PlotTimeSpaceDensity'};
       
    % number of function of each type
    nFns = length(fns);
    nFnsMulti = length(fnsMulti);
    
    %----------------------------------------------------------------------
    % Determine if we're saving the output, and which computations we're
    % doing fns for
    %----------------------------------------------------------------------

    % determine if we're saving the output (also the flag for doing pngs)
    if(isfield(optsPlot,'doPdfs') && optsPlot.doPdfs)
        doPdfs = true;
    else
        doPdfs = false;
    end
    
    % determine which of the loopVars we're using to do the plots for
    % individual computations in fns
    if(isfield(optsPlot,'loopVarsPlot'))
        plotIDs = MOD(1).GetToPlot(optsPlot);
    else
        plotIDs = 1:MOD(1).nComps;
    end

    %----------------------------------------------------------------------
    % Default sizes
    %----------------------------------------------------------------------

    if(~isfield(optsPlot,'fontsize'))
        optsPlot.fontsize = 30;
    end
    if(~isfield(optsPlot,'linewidth'))
        optsPlot.linewidth = 3;
    end

    %----------------------------------------------------------------------
    % Save legend choice
    %----------------------------------------------------------------------

    if(isfield(optsPlot,'legendType'))
        legendType = optsPlot.legendType;
    else
        legendType = [];
    end
        
    
    %----------------------------------------------------------------------
    % Single computation plots
    %----------------------------------------------------------------------

    % loop over functions
    for iFn = 1:nFns

        % determine if the function is flagged true in optsPlot
        fni = fns{iFn};
        if(isfield(optsPlot,fni) && optsPlot.(fni))
        
            if(isfield(optsPlot,'addSnapshotLabels') && optsPlot.addSnapshotLabels)
                [~,annotations] = MOD(1).GetToPlot(optsPlot);
            else
                annotations = [];
            end
            
            % only plot those computations in plotIDs
            for plotID = plotIDs

                % set computation to be plotted
                optsPlot.plotID = plotID;
                
                % determine annotations for automatic labelling of
                % individual plots
                if(~isempty(annotations))
                    optsPlot.annotation = annotations{plotID};
                end

                % optionally only plot the legend in the first or last snapshot
                if(isfield(optsPlot,'loopVarsLegend'))
                    if(plotID > plotIDs(1) && strcmp(optsPlot.loopVarsLegend,'first'))
                        optsPlot.legendType = 'off';
                    elseif(plotID < plotIDs(end) && strcmp(optsPlot.loopVarsLegend,'last'))
                        optsPlot.legendType = 'off';
                    else
                        optsPlot.legendType = legendType;
                    end
                end                
                
                % call the function (see below)
                % each take inputs MOD and optsPlot
                feval(str2func(fni),MOD,optsPlot);

                % save output
                if(doPdfs)

                    % set up output file name
                    if(~isempty(MOD(1).compNames{plotID}))
                        compName = MOD(1).compNames{plotID};
                        compName = compName(3:end-1);
                        set(gcf,'Name',MOD(1).compNames{plotID});
                    else
                        compName = num2str(plotID);
                    end
                    compName = strrep(compName,', ','_');
                    fileName = [fni ...
                                '_' optsPlot.name ...
                                '_' compName];
                    if(isfield(optsPlot,'annotation'))
                        fileName = [optsPlot.annotation '_' fileName]; %#ok
                    end
                    fileName = [optsPlot.outputDir filesep fileName]; %#ok

                    % determine if we should output png rather than pdf
                    % and save
                    doPngs = any(strcmp(fni,pngList));
                    SavePlot(fileName,gcf,doPngs)
                end

            end
                
            % clean up optsPlot
            if(isfield(optsPlot,'annotation'))
                optsPlot = rmfield(optsPlot,'annotation');
            end
            if(~isempty(legendType))
                optsPlot.legendType = legendType;
            end

        end
 
    end
    
    %----------------------------------------------------------------------
    % MultiOpinionDynamics plots
    %----------------------------------------------------------------------

    for iFn = 1:nFnsMulti
        % set parameters for plotting function
        optsPlot.multi = true;
        optsPlot.square = true;
        fni = fnsMulti{iFn};
        
        % determine if the function is flagged true in optsPlot
        if(isfield(optsPlot,fni) && optsPlot.(fni))
            
            if(isfield(optsPlot,'separate') && optsPlot.separate)
            % plot the BCs in separate figures
                for iBC = 1:optsPlot.nBCs
                    % call relevant function from MOD
                    MOD(iBC).(fni)(optsPlot);

                    % save output
                    if(doPdfs)
                        % determine save file
                        set(gcf,'Name',optsPlot.BCText{iBC}); 
                        fileName = [optsPlot.outputDir filesep fni ...
                                       '_' optsPlot.name ...
                                       '_' optsPlot.BCText{iBC}];
                    
                        % determine if we should output png rather than pdf
                        % and save
                        doPngs = any(strcmp(fni,pngList));
                        SavePlot(fileName,gcf,doPngs)
                    end
                end
            
            else
            % plot in the same figure for all BCs
                
                % call the relevant function from below
                feval(str2func(fni),MOD,optsPlot);

                if(doPdfs)
                    % determine save file
                    fileName = [optsPlot.outputDir filesep fni ...
                                   '_' optsPlot.name];                    
                    
                    % determine if we should output png rather than pdf
                    % and save
                    doPngs = any(strcmp(fni,pngList));
                    SavePlot(fileName,gcf,doPngs)
                end            
            end
        end
    end
    
    %----------------------------------------------------------------------
    % Custom plots
    %----------------------------------------------------------------------

    if(isfield(optsPlot,'Custom'))
        fnsCustom = optsPlot.Custom;
        nFnsCustom = length(fnsCustom);

        % do each of the custom plots
        for iFn = 1:nFnsCustom
            fni = fnsCustom{iFn};
            for iBC = 1:optsPlot.nBCs
                % custom plot should take inputs MOD and optsPlot
                hf = feval(str2func(fni),MOD(iBC),optsPlot);
                
                % save
                if(doPdfs)
                    if(length(hf)>1)
                        for ihf = 1:length(hf)
                            pdfFile = [optsPlot.outputDir filesep fni '_' optsPlot.BCText{iBC} '_' num2str(ihf) '.pdf'];
                            save2pdf(pdfFile,hf(ihf));
                            fprintf(['File saved in ' pdfFile '\n']);
                            close(hf(ihf));
                        end
                    else
                        pdfFile = [optsPlot.outputDir filesep fni '_' optsPlot.BCText{iBC} '.pdf'];
                        save2pdf(pdfFile,hf);   
                        fprintf(['File saved in ' pdfFile '\n']);
                        close(hf);
                    end
                end
            end
         end
    end
        
            
    
 end                 

%--------------------------------------------------------------------------
% Plotting functions
%--------------------------------------------------------------------------

function optsPlot = PlotSnapshotsDensityRadicals(MOD,optsPlot) %#ok

    % separate limits for each different snapshot
    if(isfield(optsPlot,'snapshotYLims') && optsPlot.snapshotYLims)
        optsPlot.plotIDs = optsPlot.plotID;
    end

    % get limits
    if(~isfield(optsPlot,'yLim'))
        if(isfield(optsPlot,'keepYLabels'))
            % separate lims for each BC
            optsPlot = GetYLimsSeparate(MOD,'rho_t',optsPlot);
            optsPlot.yLim(1,:) = 0;
        else
            optsPlot = GetYLims(MOD,'rho_t',optsPlot);        
            optsPlot.yLim(1) = 0;
        end
    end
            
    if(isfield(optsPlot,'snapshotTimes'))
        optsPlot.nColors = length(optsPlot.snapshotTimes);
    end
    
    % legends outside axes
    %optsPlot.legendType = 'right';
    
    PlotFunctions(MOD,optsPlot,'PlotSnapshotsDensityRadicals');    
 end

function optsPlot = PlotAsymptotics(MOD,optsPlot) %#ok
    
    % set default line colour and style
    optsPlotTemp = optsPlot;
    optsPlotTemp.linecolor = 'k';
    optsPlotTemp.linestyle = '--';
    optsPlotTemp.legend    = 'Asymptotics';
    
    PlotFunctions(MOD,optsPlotTemp,'PlotAsymptotics');    
 end
 
 
function optsPlot = PlotOrderParameter(MOD,optsPlot) %#ok

    % determine limits for order parameter
    if(~isfield(optsPlot,'QLim'))
        if(isfield(optsPlot,'snapshotYLims') && optsPlot.snapshotYLims)
            optsPlot.plotIDs = optsPlot.plotID;
        end
        if(isfield(optsPlot,'keepYLabels'))
            % separate lims for each BC
            optsPlotTemp = GetYLimsSeparate(MOD,'Q',optsPlot);
        else
            optsPlotTemp = GetYLims(MOD,'Q',optsPlot);        
        end

        %optsPlotTemp = GetYLims(MOD,'Q',optsPlot);
        optsPlot.QLim = optsPlotTemp.yLim;
    end
        
    % set time limits if given
    if(isfield(optsPlot,'tLim'))
        optsPlot.xLim = optsPlot.tLim;
    end

    % option to keep y labels when the limits are different for different
    % BCs
    if(isfield(optsPlot,'keepYLabelsQ'))
        optsPlot.keepYLabels = optsPlot.keepYLabelsQ;
    end
    
    % set the colour to black if there's only one plot
    % and turn off the legend in FixTightSubplots
    if(isfield(optsPlot,'plotID') && (length(optsPlot.plotID)==1) ...
            && ~isfield(optsPlot,'linecolor') )
        optsPlot.linecolor = 'k';
        optsPlot.setColor = true;
        optsPlot.legendType = 'off';
    end
       
    PlotFunctions(MOD,optsPlot,'PlotOrderParameter');
        
end

function optsPlot = PlotOrderParameterFinal(MOD,optsPlot) %#ok
    
    % get the order parameter limits over all computations, not just for
    % those in plotIDs and snapshotTimes
	optsPlot.useFull = true;
    optsPlot = GetOrderParameterColours(MOD,optsPlot);
    optsPlot = rmfield(optsPlot,'useFull');
    optsPlot.legendType = 'off';        
    PlotFunctions(MOD,optsPlot,'PlotOrderParameterFinal');
    
end

function optsPlot = PlotOrderParameterFinalEqTime(MOD,optsPlot) %#ok
            
    % add space for a colorbar in TightSubplots
    optsPlot.colorbar = true;
 
    % all other options set within the called functions
    PlotMultipleFunctions(MOD,optsPlot,{'PlotOrderParameterFinal','PlotEqTime'});
end

function optsPlot = PlotMeanOpinionFinal(MOD,optsPlot) %#ok

    % get the mean opinion limits over all computations, not just for
    % those in plotIDs and snapshotTimes
	optsPlot.useFull = true;
    optsPlot = GetMeanOpinionColours(MOD,optsPlot);
    optsPlot = rmfield(optsPlot,'useFull');
            
    PlotFunctions(MOD,optsPlot,'PlotMeanOpinionFinal');
    
end


function optsPlot = PlotModeOpinionFinal(MOD,optsPlot) %#ok

    % get the mode opinion limits over all computations, not just for
    % those in plotIDs and snapshotTimes
	optsPlot.useFull = true;
    optsPlot = GetModeOpinionColours(MOD,optsPlot);
    optsPlot = rmfield(optsPlot,'useFull');
        
    PlotFunctions(MOD,optsPlot,'PlotModeOpinionFinal');
    
end

function optsPlot = PlotDensityOrderParameter(MOD,optsPlot) %#ok

    % keep x axis labels as they are different for the two plots
    optsPlot.keepXLabels = true;
    
    ha = PlotMultipleFunctions(MOD,optsPlot,{'PlotSnapshotsDensityRadicals','PlotOrderParameter'});
    
    % add dots to the order parameter plots at the times corresponding to
    % the snapshots
    optsPlot.haDots = ha(2,:);
    AddTimeDots(MOD,'Q',optsPlot);
        
end

function optsPlot = PlotDensityModeOpinion(MOD,optsPlot) %#ok

    % keep x axis labels as they are different for the two plots
    optsPlot.keepXLabels = true;
    
    ha = PlotMultipleFunctions(MOD,optsPlot,{'PlotSnapshotsDensityRadicals','PlotModeOpinion'});

    % add dots to the order parameter plots at the times corresponding to
    % the snapshots
    optsPlot.haDots = ha(2,:);
    AddTimeDots(MOD,'modeOpinion',optsPlot);
    
end

function optsPlot = PlotDensityMeanOpinion(MOD,optsPlot) %#ok

    % keep x axis labels as they are different for the two plots
    optsPlot.keepXLabels = true;
    
    ha = PlotMultipleFunctions(MOD,optsPlot,{'PlotSnapshotsDensityRadicals','PlotMeanOpinion'});
    
    % add dots to the order parameter plots at the times corresponding to
    % the snapshots
    optsPlot.haDots = ha(2,:);
    AddTimeDots(MOD,'meanOpinion',optsPlot);
    
end

function optsPlot = PlotDensityMeanModeOpinion(MOD,optsPlot) %#ok

    % keep x axis labels as they are different for the two plots
    optsPlot.keepXLabels = true;
    
    ha = PlotMultipleFunctions(MOD,optsPlot,{'PlotSnapshotsDensityRadicals','PlotMeanModeOpinion'});

    % add dots to the order parameter plots at the times corresponding to
    % the snapshots
    optsPlot.haDots = ha(2,:);
    AddTimeDots(MOD,{'meanOpinion','modeOpinion'},optsPlot);
    
end

function optsPlot = PlotDensityOrderParameterFinal(MOD,optsPlot) %#ok

    % get the order parameter limits over all computations, not just for
    % those in plotIDs and snapshotTimes
    optsPlot.useFull = true;
    optsPlot = GetOrderParameterColours(MOD,optsPlot);

    % and for density limits
    optsPlot = GetYLims(MOD,'rho_t',optsPlot);
    % force lower limit to 0
    optsPlot.yLim(1) = 0;
    optsPlot = rmfield(optsPlot,'useFull');

    % plot densities within grid with order parameter colour background
    optsPlot.subGrid = true;
    
    optsPlot.legendType = 'off';
    
    PlotFunctions(MOD,optsPlot,'PlotDensityOrderParameterFinal');
    
end

function optsPlot = PlotDensityOrderParameterFinalEqTime(MOD,optsPlot) %#ok
            
    optsPlot.colorbar = true;
 
    PlotMultipleFunctions(MOD,optsPlot,{'PlotDensityOrderParameterFinal','PlotEqTime'});
end

function optsPlot = PlotDensityOrderParameterMeanOpinionFinal(MOD,optsPlot) %#ok
            
    optsPlot.colorbar = true;
 
    PlotMultipleFunctions(MOD,optsPlot,{'PlotDensityOrderParameterFinal','PlotMeanOpinionFinal'});
end

function optsPlot = PlotDensityOrderParameterModeOpinionFinal(MOD,optsPlot) %#ok
            
    optsPlot.colorbar = true;
 
    PlotMultipleFunctions(MOD,optsPlot,{'PlotDensityOrderParameterFinal','PlotModeOpinionFinal'});
end

function optsPlot = PlotEqTime(MOD,optsPlot) %#ok

    optsPlot.colorbar = true;

    % get the eq time limits over all computations, not just for
    % those in plotIDs and snapshotTimes
    optsPlot.useFull = true;
    optsPlot = GetEqTimeColours(MOD,optsPlot);
    optsPlot = rmfield(optsPlot,'useFull');
    
    % set log scale if requested
    if(isfield(optsPlot,'logEq') && optsPlot.logEq)
        optsPlot.log = true;
        optsPlot.cLim = log10(optsPlot.cLim);
        optsPlot.tEqLim = log10(optsPlot.tEqLim);        
    end
    optsPlot.legendType = 'off';

    PlotFunctions(MOD,optsPlot,'PlotEqTime');
end

function optsPlot = PlotMass(MOD,optsPlot) %#ok
    
    % hard coded yLim around 1
    optsPlot.yLim = [0.9;1.1];
    
    if(isfield(optsPlot,'tLim'))
        optsPlot.xLim = optsPlot.tLim;
    end

    PlotFunctions(MOD,optsPlot,'PlotMass');
end

function optsPlot = PlotMeanOpinion(MOD,optsPlot) %#ok
    
    % set time limits if given
    if(isfield(optsPlot,'tLim'))
        optsPlot.xLim = optsPlot.tLim;
    end

    PlotFunctions(MOD,optsPlot,'PlotMeanOpinion');
end


function optsPlot = PlotMeanModeOpinion(MOD,optsPlot) %#ok
    
    % set time limits if given
    if(isfield(optsPlot,'tLim'))
        optsPlot.xLim = optsPlot.tLim;
    end

    PlotFunctions(MOD,optsPlot,'PlotMeanModeOpinion');
end

function optsPlot = PlotModeOpinion(MOD,optsPlot) %#ok
    
    % set time limits if given
    if(isfield(optsPlot,'tLim'))
        optsPlot.xLim = optsPlot.tLim;
    end

    PlotFunctions(MOD,optsPlot,'PlotModeOpinion');
end


function optsPlot = PlotTimeSpaceDensity(MOD,optsPlot) %#ok
    
    % set xy (top down) view and smoothed colours
    optsPlot.xy = true;
    optsPlot.interp = true;
    
    % individual color bars on each plot
    optsPlot.colorbar = false; 
    optsPlot.keepColorbars = true;
    
%     % determine z limits if not given
%     if(~isfield(optsPlot,'zLim'))
%         optsPlotTemp = optsPlot;
%         optsPlotTemp.useFull = false; % only set colours by those plotted
%         optsPlotTemp = GetYLimsSeparate(MOD,'rho_t',optsPlotTemp); % and separately over BCs
%         optsPlotTemp.yLim(1) = 0;
%         optsPlot.zLim = optsPlotTemp.yLim;
%     end
    
    optsPlot.useFull = false;

    % separate limits for each different snapshot
    if(isfield(optsPlot,'snapshotYLims'))
        optsPlot.plotIDs = optsPlot.plotID;
    end    
    
    % determine if the stochastic data exists
    if(isfield(MOD(1).Comps(optsPlot.plotID).stocResult,'dynamics'))
        % flag to do stochastic plots and keep x labels to stop resizing
        % screwing up the alignment
        optsPlot.stoc = true;
        optsPlot.keepXLabels = true;
    end
        
    optsPlot.legendType = 'off';
    
    PlotFunctions(MOD,optsPlot,'PlotTimeSpaceDensity');
end

%--------------------------------------------------------------------------
% Plot a list of functions
%--------------------------------------------------------------------------

function [ha,hf] = PlotMultipleFunctions(MOD,optsPlot,fns)
    % get the relevant plot options
    optsPlots = GetOptsPlots(MOD,optsPlot,fns);
    % do the plotting
    [ha,hf] = PlotFunctions(MOD,optsPlots,fns);
end

function optsPlots = GetOptsPlots(MOD,optsPlot,fns)

    % deal with single input
    if(~iscell(fns))
        fns = {fns};
    end

    nFns = length(fns);

    % don't do the plotting, just get the plot options
    optsPlot.suppress = true;
    
    % store in a structure and remove the plotting suppression
    for iFn = 1:nFns
        fni = fns{iFn};
        optsPlots(iFn).optsPlot = feval(str2func(fni),MOD,optsPlot); %#ok
        optsPlots(iFn).optsPlot = rmfield(optsPlots(iFn).optsPlot,'suppress'); %#ok
    end
        
end

%--------------------------------------------------------------------------
% Plot functions
%--------------------------------------------------------------------------

function [ha,hf] = PlotFunctions(MOD,optsPlots,fns)
    
    % deal with a single input
    if(length(optsPlots)==1)
        temp = optsPlots;
        clear optsPlots;
        optsPlots(1).optsPlot = temp;
    end
        
    % just return - don't do the actual plotting
    % used to call the plotting functions to set up optsPlot
    if(isfield(optsPlots(1).optsPlot,'suppress') && optsPlots(1).optsPlot.suppress)
        return;
    end
        
    % deal with a single input
    if(~iscell(fns))
        fns = {fns};
    end

    nFns = length(fns);
    nBCs = optsPlots(1).optsPlot.nBCs;
    
    % optsPlotBasic is sent to TightSubplots and FixTightSubplots
    optsPlotBasic.nRows = nFns;
    optsPlotBasic.nCols = nBCs;
    
    % if we're doing stochastic plots then we need extra axes in some cases
    stocFactor = 1;
    if(isfield(optsPlots(1).optsPlot,'stoc') && optsPlots(1).optsPlot.stoc)
        stocFactor = 2;
    end
        
    % determine rows and columns
    if(isfield(optsPlots(1).optsPlot,'nCols'))
        optsPlotBasic.nCols = optsPlots(1).optsPlot.nCols;
    else
        optsPlotBasic.nCols = optsPlots(1).optsPlot.nBCs;
    end
    
    if(isfield(optsPlots(1).optsPlot,'nRows'))
        optsPlotBasic.nRows = optsPlots(1).optsPlot.nRows;
    else
        optsPlotBasic.nRows = nFns * stocFactor ;
    end
       
    % set number of colors for snapshot plots
    nColors = 0;
    for iFn = 1:nFns
        if(isfield(optsPlots(iFn).optsPlot,'nColors'))
            nColors = max(nColors,optsPlots(iFn).optsPlot.nColors);
        end
    end
    if(nColors>0)
        optsPlotBasic.nColors = nColors;
    end
    
    % determine if we should keep labels and add space for a colorbar
    % used to determine axes placement in TightSubplot
    keepYLabels = false(nFns*stocFactor,nBCs);
    keepXLabels = false;
    colorbar = false;
    keepColorbars = false;
    legendType = cell(nFns,1);
    for iFn = 1:nFns
        
        if(isfield(optsPlots(iFn).optsPlot,'keepXLabels') && optsPlots(iFn).optsPlot.keepXLabels)
            keepXLabels = true;
        end
        
        if(isfield(optsPlots(iFn).optsPlot,'keepYLabels'))
            keepYLabels(iFn:iFn+stocFactor-1,:) = repmat(optsPlots(iFn).optsPlot.keepYLabels',stocFactor,1);
        end
    
        if(isfield(optsPlots(iFn).optsPlot,'colorbar') && optsPlots(iFn).optsPlot.colorbar)
            colorbar = true;
        end
        
        if(isfield(optsPlots(iFn).optsPlot,'keepColorbars') && optsPlots(iFn).optsPlot.keepColorbars)
            keepColorbars = true;
        end
        
        if(isfield(optsPlots(iFn).optsPlot,'legendType'))
            legendType{iFn} = optsPlots(iFn).optsPlot.legendType;
        end
        
        
    end

    optsPlotBasic.keepXLabels = keepXLabels;
    optsPlotBasic.keepYLabels = keepYLabels;
    optsPlotBasic.colorbar = colorbar;
    optsPlotBasic.keepColorbars = keepColorbars;
    optsPlotBasic.legendType = legendType;

    % line width and font size
    if(isfield(optsPlots(1).optsPlot,'linewidth'))
        optsPlotBasic.linewidth = optsPlots(1).optsPlot.linewidth;
    end
    if(isfield(optsPlots(1).optsPlot,'fontsize'))
        optsPlotBasic.fontsize = optsPlots(1).optsPlot.fontsize;
    end    
    
    % make the figure and subplots
    [ha,hacb,hf] = TightSubplot(optsPlotBasic); 
    
    nCols = optsPlotBasic.nCols;
            
    % fields to check whether they exist and, if so, assign relevant values
    % to pass to plotting functions
    checkFields = {'yLim','zLim','QLim','QFinalLim','EqLim'};
    
    for iFn = 1:nFns
        
        % get function and relevant plotting options
        fn = fns{iFn};
        optsPlotFull = optsPlots(iFn).optsPlot;
                        
        % plot for each of the BCs
        for iBC = 1:nCols
            optsPlot = optsPlotFull;
            
            % if we're doing stochastic plots separately then it uses two
            % rows, otherwise just one
            rowMask = (stocFactor*iFn)-(stocFactor - 1):stocFactor*iFn;
            optsPlot.ha = ha(rowMask,iBC);
            
            % allocate limits for individual plots
            for iField = 1:length(checkFields)
                field = checkFields{iField};                
                % allocate values across BCs if they are different
                if(isfield(optsPlot,field) && size(optsPlotFull.(field),2) == nBCs)
                    optsPlot.(field) = optsPlotFull.(field)(:,iBC);
                end
                
            end
            
            if(isfield(optsPlot,'multi') && optsPlot.multi)
            % direct MOD plot
                if(isfield(optsPlot(1),'subGrid') && optsPlot(1).subGrid)
                    % different axes convention for a subgrid plot
                    optsPlot = rmfield(optsPlot,'ha');
                    optsPlot.haOut = ha(iFn,iBC);
                    if(optsPlotBasic.colorbar)
                        optsPlot.hacb = hacb(iFn,1:stocFactor);
                    end
                end
                % call MOD plotting function
                MOD(iBC).(fn)(optsPlot);
            else
            % plot by computation            
                MOD(iBC).Comps(optsPlot.plotID).(fn)(optsPlot);
            end
        end
    end
       
    % add titles if there is more than one BC or asked for
    addTitle = (isfield(optsPlot,'addTitle') && optsPlot.addTitle);
    if(nBCs>1 || addTitle)
        optsPlotBasic.title = optsPlots(1).optsPlot.BCText;
    end
        
    % determine legend type for FixTightSubplots
    legendType = cell(nFns,stocFactor);
    for iFn = 1:nFns
        if(isfield(optsPlots(iFn).optsPlot,'legendType'))
            for iLeg = 1:stocFactor
                legendType{iFn,iLeg} = optsPlots(iFn).optsPlot.legendType;
            end
        end
    end
    optsPlotBasic.legendType = legendType;
    
    % determine annotation for FixTightSubplots
    if(isfield(optsPlots(1).optsPlot,'annotation'))
        optsPlotBasic.annotation = optsPlots(1).optsPlot.annotation;
    end
    
    % neaten up plots
    FixTightSubplots(ha,hacb,optsPlotBasic);
    
end


%--------------------------------------------------------------------------
% Add symbols to time plots corresponding to snapshots
%--------------------------------------------------------------------------

function AddTimeDots(MOD,vars,optsPlot)
    
    % deal with a single input
    if(~iscell(vars))
        vars = {vars};
    end
    
    nVars = length(vars);
    haDots = optsPlot.haDots;
    
    if(isfield(optsPlot,'linewidth'))
        markersize = 5*optsPlot.linewidth;
    else
        markersize = 10;
    end
        
    % loop over BC plots (a row in TightSubplots)
    for iBC = 1:optsPlot.nBCs
        
        ha = haDots(iBC);

        plotTimes = MOD(iBC).Comps(1).dynamicsResult.plotTimes;
        
        % default is every output time
        tPos = 1:length(plotTimes);
        % change to the snapshotTimes positions if they are given
        if(isfield(optsPlot,'snapshotTimes'))
            tPos = MOD(iBC).Comps(1).GetTPos(optsPlot.snapshotTimes,plotTimes);
        end

        % loop over variables
        for iVar = 1:nVars
            var = vars{iVar};

            % reset colour order to match snapshots
            ha.ColorOrderIndex = 1;
            % get values
            varValues = MOD(iBC).Comps(optsPlot.plotID).dynamicsResult.(var);
            % plot symbols for PDE
            for iT = tPos
                hp = plot(ha,plotTimes(iT),varValues(iT),'o','MarkerSize',markersize,'HandleVisibility','off');
                set(hp, 'markerfacecolor', get(hp, 'color'));
                set(hp, 'color', 'k');
            end
            
            % do the same for SDEs if the variables exist
            if(isfield(MOD(iBC).Comps(optsPlot.plotID).stocResult,'dynamics') ...
                    && isfield(MOD(iBC).Comps(optsPlot.plotID).stocResult.dynamics,var))
                
                ha.ColorOrderIndex = 1;
                varValues = MOD(iBC).Comps(optsPlot.plotID).stocResult.dynamics.(var);
                for iT = tPos
                    hp = plot(ha,plotTimes(iT),varValues(iT),'s','MarkerSize',markersize,'HandleVisibility','off');
                    set(hp, 'markerfacecolor', get(hp, 'color'));
                    set(hp, 'color', 'k');
                end    
                
            end
            
        end
        
        % turn off the legend for a single variable plot as it's given 
        % in the y axis label
        if(nVars == 1)
            legend(ha,'off');
        end
    end
        
end

%--------------------------------------------------------------------------
% Get limits across BC computations
%--------------------------------------------------------------------------

function optsPlot = GetYLims(MOD,vars,optsPlot)

    % deal with single input
    if(~iscell(vars))
        vars = {vars};
    end
    nVars = length(vars);

    % loop over BCs
    maxVar = NaN;
    minVar = NaN;
    for iBC = 1:optsPlot.nBCs
        for iVar = 1:nVars
            optsPlot = MOD(iBC).GetVarBounds(vars{iVar},optsPlot);
            maxVar = max(maxVar,optsPlot.yLim(2));
            minVar = min(minVar,optsPlot.yLim(1));
        end
    end
        
    optsPlot.yLim = [minVar;maxVar];
    
end

%--------------------------------------------------------------------------
% Get limits for individual computations
%--------------------------------------------------------------------------

function optsPlot = GetYLimsSeparate(MOD,vars,optsPlot)

    if(~iscell(vars))
        vars = {vars};
    end
    nVars = length(vars);
    nBCs = optsPlot.nBCs;
    
    yLim = zeros(2,nBCs);
    for iBC = 1:nBCs
        maxVar = NaN;
        minVar = NaN;
        for iVar = 1:nVars
            optsPlot = MOD(iBC).GetVarBounds(vars{iVar},optsPlot);
            maxVar = max(maxVar,optsPlot.yLim(2));
            minVar = min(minVar,optsPlot.yLim(1));
        end
        yLim(:,iBC) = [minVar;maxVar];
    end
        
    optsPlot.yLim = yLim;

end

%--------------------------------------------------------------------------
% Get limits for various properties
%--------------------------------------------------------------------------

function optsPlot = GetEqTimeColours(MOD,optsPlot)
    cMin = NaN;
    cMax = NaN;
    
    for iBC = 1:optsPlot.nBCs
        optsPlotTemp = MOD(iBC).GetEqTimeColours(optsPlot);
        cMin = min(cMin,optsPlotTemp.cLim(1));
        cMax = max(cMax,optsPlotTemp.cLim(2));
    end
    optsPlot.tEqLim = [cMin,cMax];
    
    optsPlot.cLim = optsPlot.tEqLim;
    optsPlot.colorbar = true;
end

function optsPlot = GetOrderParameterColours(MOD,optsPlot)
    
    cMin = NaN;
    cMax = NaN;

    if(~isfield(optsPlot,'QFinalLim'))
        for iBC = 1:optsPlot.nBCs
            optsPlotTemp = MOD(iBC).GetOrderParameterColours(optsPlot);
            cMin = min(cMin,optsPlotTemp.cLim(1));
            cMax = max(cMax,optsPlotTemp.cLim(2));
        end
        optsPlot.QFinalLim = [cMin,cMax];
    end
    optsPlot.cLim = optsPlot.QFinalLim;
                        
    optsPlot.colorbar = true;
end

function optsPlot = GetMeanOpinionColours(MOD,optsPlot)
        
    cMin = NaN;
    cMax = NaN;
    
    for iBC = 1:optsPlot.nBCs
        optsPlotTemp = MOD(iBC).GetMeanOpinionColours(optsPlot);
        cMin = min(cMin,optsPlotTemp.cLim(1));
        cMax = max(cMax,optsPlotTemp.cLim(2));
    end
    
    optsPlot.meanLim = [cMin,cMax];
    optsPlot.cLim = optsPlot.meanLim;
    optsPlot.meanRange = optsPlot.meanLim;
                        
    optsPlot.colorbar = true;
    
end

function optsPlot = GetModeOpinionColours(MOD,optsPlot)
    
    cMin = NaN;
    cMax = NaN;

    for iBC = 1:optsPlot.nBCs
        optsPlotTemp = MOD(iBC).GetModeOpinionColours(optsPlot);
        cMin = min(cMin,optsPlotTemp.cLim(1));
        cMax = max(cMax,optsPlotTemp.cLim(2));
    end
    optsPlot.modeLim = [cMin,cMax];
    optsPlot.cLim = optsPlot.modeLim;
    optsPlot.modeRange = optsPlot.modeLim;
                        
    optsPlot.colorbar = true;
end

%--------------------------------------------------------------------------
% Save plots as pdfs or pngs
%--------------------------------------------------------------------------

function SavePlot(fileName,hf,doPngs)

    if(~doPngs)
        pdfFile = [fileName '.pdf'];
        save2pdf(pdfFile,hf);
        fprintf(['File saved in ' pdfFile '\n']);
        close(gcf);
    else
        pngFile = [fileName '.png'];
        save2png(pngFile,hf);
        fprintf(['File saved in ' pngFile '\n']);
        close(gcf);
    end
    
end