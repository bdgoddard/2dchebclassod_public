 function [MOD,optsPlot] = OD_Generate_BC_Examples(Example,Params,MOD)
% OD_GENERATE_BC_EXAMPLES Generate opinion dynamics examples for three 
% different boundary conditions.
% 
% Outputs:
% MOD      - a MultiOpinionDynamics containing the results
% optsPlot - plotting options
%
% Inputs:
% Example - function outputting opts, optsPlot and doBCs
% Params  - parameters to be passed to Example
% MOD     - inputted output MOD to stop reloading, useful when large and
%           rerunning purely for plotting
 
    %----------------------------------------------------------------------
    % Set up default domains
    %----------------------------------------------------------------------

    % default on params
    if(nargin<2)
        Params = struct;
    end
  
    % default options for the different boundary conditions
    % (periodic, no-flux, even 2-periodic)
    Phys_Area(1) = struct('shape','FourierLine','N',200,'yMin',0,'yMax',1, ...
                          'even2periodic',false);
    Phys_Area(2) = struct('shape','SpectralLine','N',200,'yMin',0,'yMax',1, ...
                          'even2periodic',false);
    Phys_Area(3) = struct('shape','FourierLine','N',200,'yMin',-1,'yMax',1, ...
                          'even2periodic',true);
    
    %----------------------------------------------------------------------
    % Get information from the Example
    %----------------------------------------------------------------------
                                            
    % get options from the example we're running
    ExampleFn = str2func(Example);
    if(nargout(ExampleFn)==3)
        [opts,optsPlot,doBCs] = ExampleFn(Params);
    else
        [opts,optsPlot] = ExampleFn(Params);
        doBCs = [true,true,true];  % default to doing all three BCs
    end
    
    % addition to save directory, normally from values in Params
    if(isfield(opts,'nameAddition') && ~isempty(opts(1).nameAddition))
        name = [Example '_' opts.nameAddition];
    else
        name = Example;
    end
        
    % save directory, within dirData set by AddPaths
    saveDir = ['OpinionDynamicsPaper' filesep name];
    AddPaths(saveDir);

    % BC information
    optsPlot.nBCs = nnz(doBCs);
    BCTextFull = {'Periodic','No-Flux', 'Even 2-Periodic'};
    
    %----------------------------------------------------------------------
    % Run Example for each selected BC
    %----------------------------------------------------------------------
    
    BCText = {};
    BCCount = 0;
    for iBC = 1:3
        if(doBCs(iBC))
            % output computation information
            BCCount = BCCount + 1;
            fprintf('\n');
            cprintf('*black',['Starting ' BCTextFull{iBC} ' \n\n']);   
            
            if(nargin<3) % skip computation if MOD is an input
                % compute Example
                MOD(BCCount) = OD_Generate_Example(Example,Phys_Area(iBC),Params);
            end
            % output finished information
            fprintf('\n');
            cprintf('*black',['Finished ' BCTextFull{iBC} '\n']);
            BCText = [BCText, BCTextFull{iBC}]; %#ok
        end
    end

    %----------------------------------------------------------------------
    % Do plotting
    %----------------------------------------------------------------------

    % make output directory
    optsPlot.BCText = BCText;
    global dirData
    optsPlot.outputDir = [dirData filesep 'Output'];
    if(~exist(optsPlot.outputDir,'dir'))
        mkdir(optsPlot.outputDir);
    end
    optsPlot.name = name;
    if(isfield(optsPlot,'nameAddition'))
        optsPlot.name = [optsPlot.name '_' optsPlot.nameAddition];
    end
    
    % do plotting if we're not in headless
    if(usejava('desktop'))
        OD_Plot_BC_Examples(MOD,optsPlot);
    end
    
end