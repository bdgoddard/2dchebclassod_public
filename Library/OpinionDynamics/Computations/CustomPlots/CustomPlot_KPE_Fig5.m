function hf = CustomPlot_KPE_Fig5(MOD,optsPlot)

    plotTimes = MOD.Comps.dynamicsResult.plotTimes;

    nPlots = length(plotTimes);

    hf = figure('Position',[100,100,800,1200]);

    optsPlot.yLim = [0,10];

    OD = MOD.Comps;
    
    for iPlot = 1:nPlots
        optsPlot.linestyle = '-';
        optsPlot.ha = subplot(nPlots,1,iPlot);
        optsPlot.legend = ['t = ' num2str(plotTimes(iPlot))];
        OD.PlotDensity(OD.dynamicsResult.rho_t(:,iPlot),optsPlot);
        hold on
        optsPlot.linestyle = '--';
        optsPlot.legend = 'Intial';
        OD.PlotDensity(OD.dynamicsResult.rho_t(:,1),optsPlot);
    end
    
    for iPlot = 1:nPlots-1
        subplot(nPlots,1,iPlot);
        xticklabels('');
        xlabel('');
    end
    subplot(nPlots,1,nPlots);
    xlabel('Opinion');
    
    for iPlot = 1:nPlots
        subplot(nPlots,1,iPlot);
        toKeep = ceil(nPlots/2);
        if(iPlot~=toKeep)
            ylabel('');
        else
            ylabel('Density');
        end
    end

end