function hf = CustomPlot_KPE_Fig2(MOD,optsPlot)

    hf = figure('Position',[100,100,1200,800]);
    nComps = MOD.nComps;
    
    haRho = zeros(nComps,1);
    haQ   = zeros(nComps,1);
    
    nRows = nComps;
  
    for iComp = 1:nComps
        haRho(iComp) = subplot(nRows,2,2*(iComp-1)+1);
        haQ(iComp)   = subplot(nRows,2,2*iComp);
    end
    
    optsRho.ha = haRho;
    
    optsQ.ha = haQ;
    optsQ.xLim = [0,5000];
    optsQ.yLim = [0,1];
    optsQ.linecolor = 'r';
    
    MOD.PlotDensityRadicalsFinal(optsRho);
    MOD.PlotOrderParameter(optsQ);
    
    for iComp = 1:nComps-1
        xticklabels(haRho(iComp),'');
        xlabel(haRho(iComp),'');
        xticklabels(haQ(iComp),'');
        xlabel(haQ(iComp),'');
    end
    
    for iComp = 1:nComps
        toKeep = ceil(nComps/2);
        if(iComp~=toKeep)
           ylabel(haRho(iComp),'');
           ylabel(haQ(iComp),'');
        end
    end

end