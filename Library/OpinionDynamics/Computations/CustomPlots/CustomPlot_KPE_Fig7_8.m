function hf = CustomPlot_KPE_Fig7_8(MOD,optsPlot)

    optsPlot.yLim = [0,10];

    plotTimes = MOD.Comps(1).dynamicsResult.plotTimes;
    snapshotTimes = optsPlot.snapshotTimes;
    nTimes = length(snapshotTimes);
    nComps = MOD.nComps;
    
    hf(1) = figure('Position',[100,100,1200,800]);
    optsPlot.legend = '$\rho$';
    optsPlot.latex  = true;
    
    linecolors = {'r','b','k'};
    
    for iTime = 1:nTimes
        tPos = find(plotTimes == snapshotTimes(iTime));
        for iComp = 1:nComps
            optsPlot.ha = subplot(nTimes,nComps,(iTime-1)*nComps + iComp);
            optsPlot.linecolor = linecolors{iComp};
            MOD.Comps(iComp).PlotDensity(MOD.Comps(iComp).dynamicsResult.rho_t(:,tPos),optsPlot);
            xlabel('');
            ylabel('');
        end
    end
    subplot(3,3,4);
    ylabel('Density');
    subplot(3,3,8);
    xlabel('Opinion');
    

    hf(2) = figure('Position',[100,100,800,800]);
    linestyles = {'-','-.','--'};
    optsPlot.ha = axes;
    optsPlot.yLim = optsPlot.QLim;
    for iComp =1:nComps
        optsPlot.linestyle = linestyles{iComp};
        optsPlot.linecolor = linecolors{iComp};
        MOD.Comps(iComp).PlotOrderParameter(optsPlot);
        hold on
    end
    legend('S1','S2','S3')
    

end