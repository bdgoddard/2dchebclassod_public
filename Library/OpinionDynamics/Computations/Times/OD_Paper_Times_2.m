function [opts,optsPlot] = OD_Paper_Times_2(opts,optsPlot)
    % times
    tMax = 10^2;
    if(~isfield(optsPlot,'snapshotTimes'))
        optsPlot.snapshotTimes = [0;1;2;5;10;20;50;100];
    end
    opts.plotTimes = [0, logspace(0,log10(tMax),100)];
    opts.plotTimes = CombineTimes(opts.plotTimes,optsPlot.snapshotTimes);    
end
