function [opts,optsPlot] = OD_Paper_Times_5(opts,optsPlot)
    % times
    tMax = 10^5;
    optsPlot.snapshotTimes = [0;1;5;10;50;100;500;10^3;10^4;10^5];
    opts.plotTimes = [0, logspace(-1,log10(tMax),100)];
    opts.plotTimes = CombineTimes(opts.plotTimes,optsPlot.snapshotTimes);    
end
