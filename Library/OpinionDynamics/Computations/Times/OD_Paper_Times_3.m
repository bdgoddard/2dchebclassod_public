function [opts,optsPlot] = OD_Paper_Times_3(opts,optsPlot)
    % times
    tMax = 10^3;
    if(~isfield(optsPlot,'snapshotTimes'))
        optsPlot.snapshotTimes = [0;1;5;10;50;100;500;tMax];
    end

    opts.plotTimes = [0, logspace(0,log10(tMax),100)];
    opts.plotTimes = CombineTimes(opts.plotTimes,optsPlot.snapshotTimes);      
end
