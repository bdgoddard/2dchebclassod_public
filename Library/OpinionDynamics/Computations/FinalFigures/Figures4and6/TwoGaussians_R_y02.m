function [opts,optsPlot,doBCs] = TwoGaussians_R_y02(~)

    doBCs = [true;true;true];

    % parameter ranges
    y0_2s = 0.3:0.05:0.75;
    Rs = 0.05:0.05:0.3;

    opts.loopVars(1) = struct('field','optsPhys.R','value',Rs,'saveName','R');
    opts.loopVars(2) = struct('field','optsPhys.optsInitial.y0_2','value',y0_2s,'saveName','y02');

    % snapshots
    
    rs =    [0.3;0.3;0.3];
    y0_2s = [0.65;0.7;0.75];
    
    optsPlot.loopVarsPlot(1) = struct('field','optsPhys.R','value',rs);
    optsPlot.loopVarsPlot(2) = struct('field','optsPhys.optsInitial.y0_2','value',y0_2s);
    
    
    % run to equilibrium (or final time)
    opts.toEq = true;
     
    % compute mean and mode opinion
    opts.doMean = true;
    opts.doMode = true;
    
    % X and Y variables for order parameter plot
    optsPlot.varX = 'y02';
    optsPlot.varXLabel = '$y_{0,2}$';
    optsPlot.varY = 'R';
    optsPlot.varYLabel = '$R$';
    
    % time on a log scale
    optsPlot.tLog = true;

    % equilibrium on a log scale
    optsPlot.logEq = true;
    
    % times
    optsPlot.snapshotTimes = [0;1;2;10;100;500;10^3;10^4];
    [opts,optsPlot] = OD_Paper_Times_4(opts,optsPlot);

    
    opts.nameAddition = [];

    optsInitial  = struct('initialFn','OD_Initial_Gaussians', ...
                          'alpha_1',80,'y0_1',0.2,'M_1',1,'M_2',1, ...
                          'alpha_2',80);
    opts.optsPhys = struct('optsInitial',optsInitial,'sigma',0.03); 
   
    
    optsPlot.doPdfs = true;
    
    optsPlot.PlotDensityOrderParameter = false;
    optsPlot.PlotTimeSpaceDensity = false;
        
    optsPlot.PlotOrderParameterFinalEqTime = false;
    optsPlot.PlotDensityOrderParameterFinalEqTime = true;

    
    optsPlot.tLog = true;
    
    optsPlot.addSnapshotLabels = true;
    
    %optsPlot.keepYLabelsQ = [false,false,true];
    %optsPlot.QLim = [0, 0, 0; 1, 1, 2];
    optsPlot.QLim = [0;1];
    optsPlot.QFinalLim = [0;2];
    
    
end