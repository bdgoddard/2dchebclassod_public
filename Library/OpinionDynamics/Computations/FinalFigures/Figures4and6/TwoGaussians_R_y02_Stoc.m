function [opts,optsPlot,doBCs] = TwoGaussians_R_y02_Stoc(params)

    doBCs = [true;true;true];

    % parameter ranges
    y0_2s = 0.3:0.05:0.75;
    Rs = 0.05:0.05:0.3;

    opts.loopVars(1) = struct('field','optsPhys.R','value',Rs,'saveName','R');
    opts.loopVars(2) = struct('field','optsPhys.optsInitial.y0_2','value',y0_2s,'saveName','y02');

    % snapshots
    
    rs =    [0.3;0.3;0.3];
    y0_2s = [0.65;0.7;0.75];
    
    optsPlot.loopVarsPlot(1) = struct('field','optsPhys.R','value',rs);
    optsPlot.loopVarsPlot(2) = struct('field','optsPhys.optsInitial.y0_2','value',y0_2s);
    
    % only run these stochastic simulations
    opts.loopVarsStoc = optsPlot.loopVarsPlot;
    
    % run to equilibrium (or final time)
    opts.toEq = true;
     
    % compute mean and mode opinion
    opts.doMean = true;
    opts.doMode = true;
    
    % X and Y variables for order parameter plot
    optsPlot.varX = 'y02';
    optsPlot.varXLabel = '$y_{0,2}$';
    optsPlot.varY = 'R';
    optsPlot.varYLabel = '$R$';
    
    % time on a log scale
    optsPlot.tLog = false;

    % equilibrium on a log scale
    optsPlot.logEq = false;
    
    opts.nameAddition = [];

    optsInitial  = struct('initialFn','OD_Initial_Gaussians', ...
                          'alpha_1',80,'y0_1',0.2,'M_1',1,'M_2',1, ...
                          'alpha_2',80);
    opts.optsPhys = struct('optsInitial',optsInitial,'sigma',0.03); 
   
    
    nParticles = 10^4;
    if(isfield(params,'BC') && strcmp(params.BC,'Even2Periodic'))
        nParticles = nParticles/2;
    end
    nRuns = 10;
    nSamples = 1000;
    
    tMax = 10;
    dt = 0.01;
    nSteps = tMax/dt;
    opts.plotTimes = 0:tMax/100:tMax;
    opts.optsStoc = struct('nParticles',nParticles, ...
                      'nSteps',nSteps,'nRuns',nRuns, ...
                      'nSamples',nSamples,'thin',1,'burnin',10^2);
    
    optsPlot.doPdfs = true;
    
    optsPlot.snapshotTimes = [0;1;2;5;10];
    
    optsPlot.PlotDensityOrderParameter = true;
    optsPlot.PlotTimeSpaceDensity = true;
            
    optsPlot.addSnapshotLabels = true;
    
    optsPlot.keepYLabels = [true;true;true];
    optsPlot.snapshotYLims = true;

    
    %optsPlot.keepYLabelsQ = [false,false,true];
    %optsPlot.QLim = [0, 0, 0; 1, 1, 2];
    %optsPlot.QLim = [0;1];
    %optsPlot.QFinalLim = [0;2];
    
    optsPlot.legendType = 'last';
end