clear all
close all

params.y0R = 0.7;

% Figure 7 (top left) and Figure 8
cprintf('*g',['\n Uniform_GaussianRadicals_R_sigma, y0R = ' num2str(params.y0R) '\n']);
[MOD,optsPlot] = OD_Generate_BC_Examples('Uniform_GaussianRadicals_R_sigma',params);

% not as interesting
% cprintf('*g',['\n Uniform_GaussianRadicals_M_sigma, y0R = ' num2str(params.y0R) '\n']);
% [MOD,optsPlot] = OD_Generate_BC_Examples('Uniform_GaussianRadicals_M_sigma',params);
% close all

% Figure 7 (top right)
cprintf('*g',['\n Uniform_GaussianRadicals_M_R, y0R = ' num2str(params.y0R) ' phase space\n']);
[MOD,optsPlot] = OD_Generate_BC_Examples('Uniform_GaussianRadicals_M_R',params);

% Figure 9 (left)
params.snapshots = true; % do snapshots only for no-flux
cprintf('*g',['\n Uniform_GaussianRadicals_M_R, y0R = ' num2str(params.y0R) ' snapshots\n']);
[MOD,optsPlot] = OD_Generate_BC_Examples('Uniform_GaussianRadicals_M_R',params);

% Figure 7 (bottom left) and Figure 9 (right)
params = struct; % reset options
cprintf('*g',['\n Uniform_GaussianRadicals_y0_sigma \n']);
[MOD,optsPlot] = OD_Generate_BC_Examples('Uniform_GaussianRadicals_y0_sigma',params);

% Figure 7 (bottom right) and Figure 10
cprintf('*g',['\n Uniform_GaussianRadicals_y0_R \n']);
[MOD,optsPlot] = OD_Generate_BC_Examples('Uniform_GaussianRadicals_y0_R',params);
