 function [opts, optsPlot, doBCs] = Uniform_GaussianRadicals_R_sigma(params)
        
    doBCs = [true;true;true];
     
    % parameter ranges
    if(isfield(params,'coarse') && params.coarse)
        sigmas = 0.02:0.01:0.05;
        Rs = 0.05:0.05:0.35;
    else
        sigmas = 0.02:0.0025:0.05;
        Rs = 0.05:0.025:0.35;
    end

    opts.loopVars(1) = struct('field','optsPhys.R','value',Rs,'saveName','R');
    opts.loopVars(2) = struct('field','optsPhys.sigma','value',sigmas,'saveName','sigma');

    
    sigmas = [0.02;0.03];
    rs = 0.1*ones(size(sigmas));
    
    optsPlot.loopVarsPlot(1) = struct('field','optsPhys.R','value',rs);
    optsPlot.loopVarsPlot(2) = struct('field','optsPhys.sigma','value',sigmas);
    
    
    % run to equilibrium (or final time)
    opts.toEq = true;
     
    % X and Y variables for order parameter plot
    optsPlot.varX = 'sigma';
    optsPlot.varXLabel = '$\sigma$';
    optsPlot.varY = 'R';
    optsPlot.varYLabel = '$R$';
    
    % time on a log scale
    optsPlot.tLog = true;

    [opts,optsPlot] = OD_Paper_Times_4(opts,optsPlot);
    
    
    % equilibrium on a log scale
    optsPlot.logEq = true;

 
    if(~isfield(params,'y0R'))
        y0R = 0.7;
    else
        y0R = params.y0R;
    end
    if(~isfield(params,'sigmaR'))
        sigmaR = 0.025;
    else
        sigmaR = params.sigmaR;
    end
    if(~isfield(params,'MR'))
        MR = 0.1;
    else
        MR = params.MR;
    end

    opts.nameAddition = ['y0R_' num2str(y0R) '_sigmaR_' num2str(sigmaR) 'MR_' num2str(MR)];
    
    optsInitial  = struct('initialFn','OD_Initial_Uniform');
    optsRadicals = struct('radicalsFn','OD_Radicals_Gaussian','y0',y0R,'sigma',sigmaR,'M',MR);
    
    opts.optsPhys = struct('optsInitial',optsInitial,'optsRadicals',optsRadicals);
    
    opts.toEq = true;
     
    optsPlot.PlotDensityOrderParameterFinalEqTime = false;
    
    optsPlot.legPos = 'northwest';
    optsPlot.legendType = 'last';
    optsPlot.loopVarsLegend = 'last';
    optsPlot.addSnapshotLabels = true;
    
    optsPlot.QLim = [0;1];

    
    optsPlot.PlotDensityOrderParameter = true;
    optsPlot.PlotTimeSpaceDensity = true;
    optsPlot.tLog = true;
        
    optsPlot.doPdfs = true;
    
    
end                 

