 function [opts, optsPlot, doBCs] = Uniform_GaussianRadicals_y0_sigma(params)
        
    doBCs = [true;true;true];
     
    % parameter ranges
    if(isfield(params,'coarse') && params.coarse)
        sigmas = 0.02:0.01:0.05;
        y0s = 0.5:0.05:0.9;
    else
        sigmas = 0.02:0.005:0.05;
        y0s = 0.5:0.05:0.9;
    end


    opts.loopVars(1) = struct('field','optsPhys.optsRadicals.y0','value',y0s,'saveName','y0');
    opts.loopVars(2) = struct('field','optsPhys.sigma','value',sigmas,'saveName','sigma');

    
    sigmas = [0.02;0.025];
    y0s = [0.7; 0.85];
    
    optsPlot.loopVarsPlot(1) = struct('field','optsPhys.optsRadicals.y0','value',y0s);
    optsPlot.loopVarsPlot(2) = struct('field','optsPhys.sigma','value',sigmas);
    
    
    % run to equilibrium (or final time)
    opts.toEq = true;
     
    % X and Y variables for order parameter plot
    optsPlot.varX = 'y0';
    optsPlot.varXLabel = '$y_0$';
    optsPlot.varY = 'sigma';
    optsPlot.varYLabel = '$\sigma$';
    
    % time on a log scale
    optsPlot.tLog = true;

    [opts,optsPlot] = OD_Paper_Times_4(opts,optsPlot);
    
    
    % equilibrium on a log scale
    optsPlot.logEq = true;

    
    if(~isfield(params,'R'))
        R = 0.1;
    else
        R = params.R;
    end 
    if(~isfield(params,'MR'))
        MR = 0.1;
    else
        MR = params.MR;
    end 
    if(~isfield(params,'sigmaR'))
        sigmaR = 0.025;
    else
        sigmaR = params.sigmaR;
    end

    opts.nameAddition = ['R_' num2str(R) '_MR_' num2str(MR) '_sigmaR_' num2str(sigmaR)];
    
    optsInitial  = struct('initialFn','OD_Initial_Uniform');
    optsRadicals = struct('radicalsFn','OD_Radicals_Gaussian','M',MR,'sigma',sigmaR);
    
    opts.optsPhys = struct('R',R,'optsInitial',optsInitial,'optsRadicals',optsRadicals);
    
    opts.toEq = true;
     
    optsPlot.PlotOrderParameterFinalEqTime = false;
    optsPlot.PlotDensityOrderParameterFinalEqTime = false;
    
    optsPlot.legPos = 'northwest';
    optsPlot.legendType = 'off';
    optsPlot.addSnapshotLabels = true;
    
    optsPlot.QLim = [0;1];
    
    optsPlot.PlotDensityOrderParameter = true;
    optsPlot.PlotTimeSpaceDensity = true;
    optsPlot.tLog = true;
        
    optsPlot.doPdfs = true;
    
    
end                 

