 function [opts, optsPlot, doBCs] = Uniform_GaussianRadicals_y0_R(params)
        
    doBCs = [true;true;true];
     
    % parameter ranges
    if(isfield(params,'coarse') && params.coarse)
        Rs = 0.05:0.05:0.3;
        y0s = 0.5:0.05:0.8;
    else
        Rs = 0.05:0.025:0.3;
        y0s = 0.5:0.025:0.8;
    end


    opts.loopVars(1) = struct('field','optsPhys.optsRadicals.y0','value',y0s,'saveName','y0');
    opts.loopVars(2) = struct('field','optsPhys.R','value',Rs,'saveName','R');

    
%     rs = [0.1;0.225;0.3];
%     y0s = 0.7*ones(size(rs));

    rs = [0.1;0.3];
    y0s = 0.7*ones(size(rs));

    optsPlot.loopVarsPlot(1) = struct('field','optsPhys.optsRadicals.y0','value',y0s);
    optsPlot.loopVarsPlot(2) = struct('field','optsPhys.R','value',rs);
    
    
    % run to equilibrium (or final time)
    opts.toEq = true;
     
    % X and Y variables for order parameter plot
    optsPlot.varX = 'y0';
    optsPlot.varXLabel = '$y_0$';
    optsPlot.varY = 'R';
    optsPlot.varYLabel = '$R$';
    
    % time on a log scale
    optsPlot.tLog = true;

    [opts,optsPlot] = OD_Paper_Times_4(opts,optsPlot);
    
    
    % equilibrium on a log scale
    optsPlot.logEq = true;

    
    if(~isfield(params,'sigma'))
        sigma = 0.02;
    else
        sigma = params.sigma;
    end

    if(~isfield(params,'MR'))
        MR = 0.1;
    else
        MR = params.MR;
    end 
    if(~isfield(params,'sigmaR'))
        sigmaR = 0.025;
    else
        sigmaR = params.sigmaR;
    end

    opts.nameAddition = ['sigma_' num2str(sigma) '_MR_' num2str(MR) '_sigmaR_' num2str(sigmaR)];
    
    optsInitial  = struct('initialFn','OD_Initial_Uniform');
    optsRadicals = struct('radicalsFn','OD_Radicals_Gaussian','M',MR,'sigma',sigmaR);
    
    opts.optsPhys = struct('sigma',sigma,'optsInitial',optsInitial,'optsRadicals',optsRadicals);
    
    opts.toEq = true;
     
    optsPlot.PlotOrderParameterFinalEqTime = false;
    optsPlot.PlotDensityOrderParameterFinalEqTime = true;
    
    optsPlot.legPos = 'northwest';
    optsPlot.legendType = 'first';
    optsPlot.loopVarsLegend = 'last';
    optsPlot.addSnapshotLabels = true;
    
    optsPlot.QLim = [0;1];
    
    optsPlot.PlotDensityOrderParameter = true;
    optsPlot.PlotTimeSpaceDensity = true;
    optsPlot.tLog = true;
        
    optsPlot.doPdfs = true;
    
    
end                 

