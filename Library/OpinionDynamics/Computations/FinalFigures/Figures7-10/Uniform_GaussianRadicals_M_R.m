 function [opts, optsPlot, doBCs] = Uniform_GaussianRadicals_M_R(params)
        
    if(isfield(params,'snapshots') && params.snapshots)
        snapshots = true;
        doBCs = [false;true;false];
    else
        snapshots = false;
        doBCs = [true;true;true];
    end
     
    % parameter ranges
    if(isfield(params,'coarse') && params.coarse)
        Rs = 0.05:0.05:0.35;
        Ms = 0.05:0.05:0.2;
    else
        Rs = 0.05:0.025:0.35;
        Ms = 0.05:0.0125:0.2;
    end


    opts.loopVars(1) = struct('field','optsPhys.optsRadicals.M','value',Ms,'saveName','M');
    opts.loopVars(2) = struct('field','optsPhys.R','value',Rs,'saveName','R');

    
    rs = [0.275;0.275];
    ms = [0.0625;0.075];
    
    optsPlot.loopVarsPlot(1) = struct('field','optsPhys.optsRadicals.M','value',ms);
    optsPlot.loopVarsPlot(2) = struct('field','optsPhys.R','value',rs);
    
    
    % run to equilibrium (or final time)
    opts.toEq = true;
     
    % X and Y variables for order parameter plot
    optsPlot.varX = 'M';
    optsPlot.varXLabel = '$M$';
    optsPlot.varY = 'R';
    optsPlot.varYLabel = '$R$';
    
    % time on a log scale
    optsPlot.tLog = true;

    optsPlot.snapshotTimes = [1;2;5;20;100;500;3000;10^4];
    [opts,optsPlot] = OD_Paper_Times_4(opts,optsPlot);
    
    
    % equilibrium on a log scale
    optsPlot.logEq = true;


    if(~isfield(params,'sigma'))
        sigma = 0.02;
    else
        sigma = params.sigma;
    end
 
    if(~isfield(params,'y0R'))
        y0R = 0.7;
    else
        y0R = params.y0R;
    end
    if(~isfield(params,'sigmaR'))
        sigmaR = 0.025;
    else
        sigmaR = params.sigmaR;
    end

    opts.nameAddition = ['sigma_' num2str(sigma) '_y0R_' num2str(y0R) '_sigmaR_' num2str(sigmaR)];
    
    optsInitial  = struct('initialFn','OD_Initial_Uniform');
    optsRadicals = struct('radicalsFn','OD_Radicals_Gaussian','y0',y0R,'sigma',sigmaR);
    
    opts.optsPhys = struct('sigma',sigma,'optsInitial',optsInitial,'optsRadicals',optsRadicals);
    
    opts.toEq = true;

    if(snapshots)
        optsPlot.PlotDensityOrderParameter = true;
        optsPlot.PlotTimeSpaceDensity = true;
    else
        optsPlot.PlotDensityOrderParameterFinalEqTime = true;
    end

    optsPlot.legPos = 'northwest';
    optsPlot.legendType = 'first';
    optsPlot.loopVarsLegend = 'first';
    optsPlot.addSnapshotLabels = true;
    optsPlot.addTitle = true;

    optsPlot.QLim = [0;1];

    
    optsPlot.PlotMass = false;
    optsPlot.tLog = true;
        
    optsPlot.doPdfs = true;
    
    
end                 

