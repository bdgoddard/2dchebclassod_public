 function [opts,optsPlot,doBCs] = KPE19_Fig5(~)
    
    doBCs = [false,false,true];
 
    opts.nameAddition = [];
    
    opts.plotTimes = [0;10;20;40;80];
    
    optsInitial  = struct('initialFn','OD_Initial_Uniform');
    optsRadicals = struct('radicalsFn','OD_Radicals_Triangle','S',0.1,'A',0.7,'M',0.1);
    
    optsPhys = struct('R',0.1, 'sigma', 0.01, ...
                      'optsInitial',optsInitial,'optsRadicals',optsRadicals);
            
    opts.optsPhys = optsPhys;
       
    optsPlot.Custom = {'CustomPlot_KPE_Fig5'};
    
    optsPlot.doPdfs = true;
end                 

