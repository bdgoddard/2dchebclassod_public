 function [opts,optsPlot,doBCs] = KPE19_Fig2(~)
        
    doBCs = [false,false,true];
    
    opts.nameAddition = [];
    
    opts.plotTimes = [0:10^2:10^4];
    
    optsInitial  = struct('initialFn','OD_Initial_Uniform');
    optsRadicals = struct('radicalsFn','OD_Radicals_Triangle','S',0.1,'A',0.7,'M',0.1);
    
    optsPhys = struct('R',0.1,'optsInitial',optsInitial,'optsRadicals',optsRadicals);
    
    opts.optsPhys = optsPhys;
    
    sigmas = [0.01;0.02;0.03;0.04;0.05];
   
    opts.loopVars = struct('field','optsPhys.sigma','value',sigmas,'saveName','sigma');
    
    optsPlot.Custom = {'CustomPlot_KPE_Fig2'};
    
    optsPlot.doPdfs = true;
    
end                 

