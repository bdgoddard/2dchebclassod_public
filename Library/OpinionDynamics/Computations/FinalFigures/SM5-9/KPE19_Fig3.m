 function [opts, optsPlot, doBCs] = KPE19_Fig3(params)
        
    doBCs = [false,false,true];
 
    opts.nameAddition = [];
    
    opts.plotTimes = [0:10:10^3];
    
    optsInitial  = struct('initialFn','OD_Initial_Uniform');
    optsRadicals = struct('radicalsFn','OD_Radicals_Triangle','S',0.1,'A',0.7,'M',0.1);
    
    optsPhys = struct('R',0.1,'optsInitial',optsInitial,'optsRadicals',optsRadicals);
    
    opts.optsPhys = optsPhys;

    if(nargin==0)
        params = struct;
    end
    
    if(isfield(params,'quick') && params.quick)
        sigmas = 0.01:0.01:0.15;
        Ms = 0.01:0.05:1;
    else
        sigmas = 0.01:0.005:0.15;
        Ms = 0.01:0.02:1;
    end

    opts.loopVars(1) = struct('field','optsPhys.optsRadicals.M','value',Ms,'saveName','M');
    opts.loopVars(2) = struct('field','optsPhys.sigma','value',sigmas,'saveName','sigma');

    optsPlot.varX = 'M';
    optsPlot.varXLabel = '$M$';
    optsPlot.varY = 'sigma';
    optsPlot.varYLabel = '$\sigma$';

    
    opts.toEq = true;
    
    optsPlot.cLim = [0.2,1];
    optsPlot.PlotOrderParameterFinal = true;
    
    optsPlot.doPdfs = true;
    
end                 

