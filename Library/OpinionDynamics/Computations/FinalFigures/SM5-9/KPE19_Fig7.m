function [opts,optsPlot,doBCs] = KPE19_Fig7(~)
        
    doBCs = [false,false,true];
    
    opts.nameAddition = [];

    optsInitial  = struct('initialFn','OD_Initial_Uniform');
    optsRadicals = struct('radicalsFn','OD_Radicals_Triangle','S',0.1);
    
    optsPhys = struct('R',0.1, 'sigma',0.01, 'optsInitial',optsInitial);
    
    MA = [ 0.05, 0.85; ...
           0.15, 0.85; ...
           0.15, 0.7];
                      
    nComps = size(MA,1);
    
    for iComp = 1:nComps
        optsRadicals.M = MA(iComp,1);
        optsRadicals.A = MA(iComp,2);
        optsPhys.optsRadicals = optsRadicals;
        opts(iComp).optsPhys = optsPhys;
        opts(iComp).plotTimes = [0:1:100];
    end

    optsPlot.QLim = [0.15;0.3];
    optsPlot.snapshotTimes = [10;20;80];
    
    optsPlot.Custom = {'CustomPlot_KPE_Fig7_8'};  
    
    optsPlot.doPdfs = true;
    
end                 

