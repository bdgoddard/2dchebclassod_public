 function [opts,optsPlot,doBCs] = Uniform_R_sigma_Small(~)
     
   % parameter ranges
    %sigmas = 0.02:0.0025:0.045;
    sigmas = 0.02:0.0025:0.05;
    Rs = 0.05:0.0125:0.2;

    opts.loopVars(1) = struct('field','optsPhys.R','value',Rs,'saveName','R');
    opts.loopVars(2) = struct('field','optsPhys.sigma','value',sigmas,'saveName','sigma');

    ss = [0.0225;0.025;0.0375;0.04;0.045];
    rs = 0.1125*ones(size(ss));
    
    optsPlot.loopVarsPlot(1) = struct('field','optsPhys.R','value',rs);
    optsPlot.loopVarsPlot(2) = struct('field','optsPhys.sigma','value',ss);
    
    optsPlot.loopVarsLegend = 'last';
    
    optsPlot.addSnapshotLabels = true;
    
    % run to equilibrium (or final time)
    opts.toEq = true;
    opts.eqTol = 5*10^-4;
    
    % X and Y variables for order parameter plot
    optsPlot.varX = 'sigma';
    optsPlot.varXLabel = '$\sigma$';
    optsPlot.varY = 'R';
    optsPlot.varYLabel = '$R$';
    
    % time on a log scale
    optsPlot.tLog = true;

    % equilibrium on a log scale
    optsPlot.logEq = true;
 
    opts.nameAddition = [];
    
    % initial condition
    optsInitial  = struct('initialFn','OD_Initial_Uniform');
    opts.optsPhys = struct('optsInitial',optsInitial); 

    [opts,optsPlot] = OD_Paper_Times_4(opts,optsPlot);
    
    doBCs = [false;true;false];
    
    optsPlot.PlotDensityOrderParameterFinalEqTime = true;
    optsPlot.PlotDensityOrderParameter = true;
    optsPlot.PlotTimeSpaceDensity = true;
    
    optsPlot.doPdfs = true;
            
end                 

 