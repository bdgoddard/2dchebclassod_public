 function [opts,optsPlot,doBCs] = Uniform_R_sigma(~)
     
    sigmas = 0.05:0.025:0.25;
    Rs = 0.05:0.05:0.5;

    opts.loopVars(1) = struct('field','optsPhys.R','value',Rs,'saveName','R');
    opts.loopVars(2) = struct('field','optsPhys.sigma','value',sigmas,'saveName','sigma');
    
    % run to equilibrium (or final time)
    opts.toEq = true;
     
    % X and Y variables for order parameter plot
    optsPlot.varX = 'sigma';
    optsPlot.varXLabel = '$\sigma$';
    optsPlot.varY = 'R';
    optsPlot.varYLabel = '$R$';
    
    % time on a log scale
    optsPlot.tLog = true;

    % equilibrium on a log scale
    optsPlot.logEq = true;
    
    
    % times
    [opts,optsPlot] = OD_Paper_Times_4(opts,optsPlot);
    
    
    % which boundary conditions to consider
    doBCs = [true;true;true]; 
 
    opts.nameAddition = [];
    
    % initial condition
    optsInitial  = struct('initialFn','OD_Initial_Uniform');
    opts.optsPhys = struct('optsInitial',optsInitial); 

        
    optsPlot.PlotDensityOrderParameterFinal = true;
    %optsPlot.PlotOrderParameterFinalEqTime = true;
    
    optsPlot.doPdfs = true;
            
end                 

 