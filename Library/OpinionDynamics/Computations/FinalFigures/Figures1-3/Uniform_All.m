clear all

close all

params = struct;

% Figure 1 (left)
cprintf('*g',['\n Uniform_R_sigma \n']);
[MOD,optsPlot] = OD_Generate_BC_Examples('Uniform_R_sigma',params);

% Figure 1 (middle) and Figure 2
cprintf('*g',['\n Uniform_R_sigma_Zoom \n']);
[MOD_Zoom,optsPlot_Zoom] = OD_Generate_BC_Examples('Uniform_R_sigma_Zoom',params);

% Figure 1 right and Figure 3
cprintf('*g',['\n Uniform_R_sigma_Small \n']);
[MOD_Small,optsPlot_Small] = OD_Generate_BC_Examples('Uniform_R_sigma_Small',params);

close all