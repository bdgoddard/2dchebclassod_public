 function [opts,optsPlot,doBCs] = Uniform_R_sigma_Zoom(~)
     
    sigmas = 0.05:0.0025:0.075;
    Rs = 0.12:0.005:0.22;    
    
    opts.loopVars(1) = struct('field','optsPhys.R','value',Rs,'saveName','R');
    opts.loopVars(2) = struct('field','optsPhys.sigma','value',sigmas,'saveName','sigma');
    
    % run to equilibrium (or final time)
    opts.toEq = true;
     
    % X and Y variables for order parameter plot
    optsPlot.varX = 'sigma';
    optsPlot.varXLabel = '$\sigma$';
    optsPlot.varY = 'R';
    optsPlot.varYLabel = '$R$';
    
    % time on a log scale
    optsPlot.tLog = true;

    % equilibrium on a log scale
    optsPlot.logEq = true;
    
    
    % times
    [opts,optsPlot] = OD_Paper_Times_4(opts,optsPlot);
        
    opts.nameAddition = [];
    
    % initial condition
    optsInitial  = struct('initialFn','OD_Initial_Uniform');
    opts.optsPhys = struct('optsInitial',optsInitial); 
            
    % which boundary conditions to consider - only do no-flux
    doBCs = [false;true;false]; 
    
    % snapshots
    sigma0 = 0.05;
    sigmas1 = sigma0*ones(5,1);
    rs1 = [0.12;0.13;0.14;0.16;0.21];
    
    r0 = 0.13;
    rs2 = r0*ones(2,1);
    sigmas2 = [0.055; 0.0575];
    
    r0 = 0.155;
    rs3 = r0*ones(3,1);
    sigmas3 = [0.0675;0.07;0.0725];
    
    sigmas = [sigmas1;sigmas2;sigmas3];
    rs = [rs1;rs2;rs3];    
    
    optsPlot.loopVarsPlot(1) = struct('field','optsPhys.R','value',rs);
    optsPlot.loopVarsPlot(2) = struct('field','optsPhys.sigma','value',sigmas);
    optsPlot.varLabels.optsPhys.R     = '$R$';
    optsPlot.varLabels.optsPhys.sigma = '$\sigma$';    
    
    optsPlot.loopVarsLegend = 'first';
    
    % plotting
    optsPlot.PlotDensityOrderParameterFinalEqTime = true;
    optsPlot.PlotDensityOrderParameter = false;
    optsPlot.PlotTimeSpaceDensity = false;
    
    optsPlot.doPdfs = true;
    
    optsPlot.addSnapshotLabels = true;
    
        
end                 

 