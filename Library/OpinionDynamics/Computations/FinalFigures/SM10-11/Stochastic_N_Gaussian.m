 function [opts,optsPlot,doBCs] = Stochastic_N_Gaussian(params)

    doBCs = [true;true;true];
 
    nParticlesAll = [10^2;5*10^2;10^3;10^4];
    %nRunsAll = 10^5./nParticlesAll; % same number of total particles
    if(isfield(params,'BC') && strcmp(params.BC,'Even2Periodic'))
        nParticlesAll = nParticlesAll/2;
    end
    nRunsAll = 10^5./nParticlesAll; % same number of independent runs if R=0
    
    nComps = length(nParticlesAll);

    optsInitial  = struct('initialFn','OD_Initial_Gaussian','alpha',20,'y0',0.1);      
    optsPhys = struct('R',0.2,'sigma',0.02,'optsInitial',optsInitial); 
    
    for iComp = 1:nComps
    
        opts(iComp).optsStoc = struct('nParticles',nParticlesAll(iComp), ...
                              'nSteps',10^3,'nRuns',nRunsAll(iComp), ...
                              'nSamples',nRunsAll(iComp),'thin',1,'burnin',10^2);
        opts(iComp).optsPhys = optsPhys;
        
        opts(iComp).nameAddition = [];
        opts(iComp).toEq = true;
        opts(iComp).plotTimes = 0:0.1:10;
    end

    
    optsPlot.snapshotTimes = [0;1;2;5;10];
    
    
    % time on a log scale
    optsPlot.tLog = false;

    % equilibrium on a log scale
    optsPlot.logEq = false;
        
    optsPlot.PlotDensityOrderParameterFinalEqTime = false;
    %optsPlot.PlotOrderParameterFinalEqTime = true;
    
    optsPlot.PlotDensityOrderParameter = true;
    optsPlot.PlotTimeSpaceDensity = false;
    
    optsPlot.doPdfs = true;
            
end                 

 