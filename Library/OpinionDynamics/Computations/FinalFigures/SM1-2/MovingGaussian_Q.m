clear all
close all

Phys_Area(1) = struct('shape','FourierLine','N',200,'yMin',0,'yMax',1, ...
                      'even2periodic',false);
Phys_Area(2) = struct('shape','SpectralLine','N',200,'yMin',0,'yMax',1, ...
                      'even2periodic',false);
Phys_Area(3) = struct('shape','FourierLine','N',200,'yMin',-1,'yMax',1, ...
                      'even2periodic',true);

Plot_Area = struct('N',400,'yMin',0,'yMax',1);

BCs = {'Periodic','NoFlux','Even2Periodic'};

titles = {'Periodic','No Flux', 'Even 2-Periodic'};

saveDir = ['OpinionDynamicsPaper' filesep 'MovingGaussianQ'];

AddPaths(saveDir);

Rs = [0.05;0.1;0.25];
nRs = length(Rs);
linestyles = {':','--','-'};

for iBC = 1:3

    kernelDirGlobal = ['..' filesep '2DChebClassData' filesep 'OpinionDynamicsPaper' ...
                           filesep 'Kernels' filesep BCs{iBC}];


    optsNum = struct('PhysArea',Phys_Area(iBC), ...
                 'PlotArea',Plot_Area, ...                  
                 'kernelDirGlobal',kernelDirGlobal, ...
                 'kernelN',50);

    opts.optsNum = optsNum;
    
    optsInitial  = struct('initialFn','OD_Initial_Gaussian','alpha',400,'y0',0,'periodic',true);
    opts.optsPhys = struct('optsInitial',optsInitial,'sigma',0.1);
             
    for iR = 1:nRs
        Rs(iR);
        opts.optsPhys.R = Rs(iR);
        MOD(iBC,iR).OD = OpinionDynamics(opts);
    end

end

y0s = 0:1/200:1;
ny0s = length(y0s);

%plotIDs = 1:40:ny0s;
plotIDs = [1;11;51;101;151;191;201]';

nPlots = length(plotIDs);

% Periodic

Q = zeros(ny0s,3,nRs);

optsPlot.title = titles;
optsPlot.nRows = 2;
optsPlot.nCols = 3;
optsPlot.nColors = nPlots-1;
optsPlot.keepXLabels = true;
[ha,hacb,hf1] = TightSubplot(optsPlot);

optsPlot.plain = true;

for iBC = 1:3
    
    for iR = 1:nRs
    
        IDC = MOD(iBC,iR).OD.IDC;
        y = MOD(iBC,iR).OD.IDC.Pts.y;

        optsPlot.ha = ha(1,iBC);    

        for iy0 = 1:ny0s
            y0 = y0s(iy0);

            MOD(iBC,iR).OD.optsPhys.optsInitial.y0 = y0;

            MOD(iBC,iR).OD.GetInitialCondition;
            rho = MOD(iBC,iR).OD.rhoI;

            % plot
            if((iR==1) && any(iy0==plotIDs))
                MOD(iBC,iR).OD.PlotDensity(rho,optsPlot);
                hold on
            end

            % compute Q
            if(iBC<3)
                MOD(iBC,iR).OD.dynamicsResult.rho_t = rho;
            else
                MOD(iBC,iR).OD.dynamicsResult.rho_t = MOD(iBC,iR).OD.cut(rho);
            end
            MOD(iBC,iR).OD.ComputeOrderParameter;
            Q(iy0,iBC,iR) = MOD(iBC,iR).OD.dynamicsResult.Q;

            
        end

    end
    
    ylim(ha(1,iBC),[0,12]);
    ylabel(ha(1,iBC),'Density')
    xlabel(ha(1,iBC),'Opinion')
    
end

for iBC = 1:3
    hold(ha(2,iBC),'on');
    for iR = 1:nRs
        plot(ha(2,iBC),y0s,Q(:,iBC,iR),'linewidth',1.5,'linestyle',linestyles{iR}, ...
            'color','k','DisplayName',['$R$ = ' num2str(Rs(iR))]);
        ha(2,iBC).ColorOrderIndex=1;
        drawnow
        %set(ha(2,iBC), 'ColorOrder', circshift(get(ha(2,iBC), 'ColorOrder'), 1))
        for iPlot = plotIDs
            hp = plot(ha(2,iBC),y0s(iPlot),Q(iPlot,iBC,iR),'o','MarkerSize',7,'HandleVisibility','off');
            set(hp, 'markerfacecolor', get(hp, 'color'), 'color', 'k');
            drawnow
        end
    end
    ylim(ha(2,iBC),[0.4,2]);
    leg = legend(ha(2,iBC),'Location','north');
    set(leg,'Interpreter','latex');
    ylabel(ha(2,iBC),'Order Parameter')
    xlabel(ha(2,iBC),'$y_0$','Interpreter','latex')
    box(ha(2,iBC),'on')
end

FixTightSubplots(ha,hacb,optsPlot);

%set(ha,'TickLabelInterpreter','latex')

% Non-Periodic

Q = zeros(ny0s,3,nRs);

optsPlot.nRows = 2;
optsPlot.nCols = 2;
optsPlot.nColors = nPlots;
optsPlot.keepXLabels = true;
[ha,hacb,hf2] = TightSubplot(optsPlot);

optsPlot.plain = true;

for iBC = 2:3
    
    for iR = 1:nRs
    
        IDC = MOD(iBC,iR).OD.IDC;
        y = MOD(iBC,iR).OD.IDC.Pts.y;

        optsPlot.ha = ha(1,iBC-1);    

        for iy0 = 1:ny0s
            y0 = y0s(iy0);

            MOD(iBC,iR).OD.optsPhys.optsInitial.y0 = y0;
            MOD(iBC,iR).OD.optsPhys.optsInitial.periodic = false;            

            MOD(iBC,iR).OD.GetInitialCondition;
            rho = MOD(iBC,iR).OD.rhoI;

            % plot
            if((iR==1) && any(iy0==plotIDs))
                MOD(iBC,iR).OD.PlotDensity(rho,optsPlot);
                hold on
            end

            % compute Q
            if(iBC<3)
                MOD(iBC,iR).OD.dynamicsResult.rho_t = rho;
            else
                MOD(iBC,iR).OD.dynamicsResult.rho_t = MOD(iBC,iR).OD.cut(rho);
            end
            MOD(iBC,iR).OD.ComputeOrderParameter;
            Q(iy0,iBC,iR) = MOD(iBC,iR).OD.dynamicsResult.Q;

        end

    end
    
    ylim(ha(1,iBC-1),[0,24]);
    ylabel(ha(1,iBC-1),'Density')
    xlabel(ha(1,iBC-1),'Opinion')
end

optsPlot.title = optsPlot.title(2:3);

for iBC = 2:3
    
    hold(ha(2,iBC-1),'on');
    for iR = 1:nRs
        plot(ha(2,iBC-1),y0s,Q(:,iBC,iR),'linewidth',1.5,'linestyle',linestyles{iR}, ...
            'color','k','DisplayName',['$R$ = ' num2str(Rs(iR))]);
        ha(2,iBC-1).ColorOrderIndex=1;
        drawnow
        for iPlot = plotIDs
            hp = plot(ha(2,iBC-1),y0s(iPlot),Q(iPlot,iBC,iR),'o','MarkerSize',7,'HandleVisibility','off');
            set(hp, 'markerfacecolor', get(hp, 'color'), 'color', 'k');
            drawnow
        end
    end
    ylim(ha(2,iBC-1),[0.4,2]);
    leg = legend(ha(2,iBC-1),'Location','north');
    set(leg,'Interpreter','latex');
    ylabel(ha(2,iBC-1),'Order Parameter')
    xlabel(ha(2,iBC-1),'$y_0$','Interpreter','latex')
    box(ha(2,iBC-1),'on')
end

FixTightSubplots(ha,hacb,optsPlot);


global dirData

pdfDir = [dirData filesep 'Output'];

if(~exist(pdfDir,'dir'))
    mkdir(pdfDir);
end
       
save2pdf([pdfDir filesep 'MovingGaussianQ_1.pdf'],hf1);
save2pdf([pdfDir filesep 'MovingGaussianQ_2.pdf'],hf2);