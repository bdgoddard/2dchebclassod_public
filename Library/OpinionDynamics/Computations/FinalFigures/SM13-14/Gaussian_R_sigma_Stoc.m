 function [opts,optsPlot,doBCs] = Gaussian_R_sigma_Stoc(params)
 
    sigmas = [0.07;0.11;0.13;0.15];
    Rs = 0.23;    

    opts.loopVars(1) = struct('field','optsPhys.R','value',Rs,'saveName','R');
    opts.loopVars(2) = struct('field','optsPhys.sigma','value',sigmas,'saveName','sigma');
    
    % run to equilibrium (or final time)
    opts.toEq = true;
 
    % X and Y variables for order parameter plot
    optsPlot.varX = 'sigma';
    optsPlot.varXLabel = '$\sigma$';
    optsPlot.varY = 'R';
    optsPlot.varYLabel = '$R$';

    % which boundary conditions to consider
    doBCs = [true;true;true]; 
 
     % initial condition
    if(~isfield(params,'y0'))
        y0 = 0.3;
    else
        y0 = params.y0;
    end
    opts.nameAddition = ['y0_' num2str(y0)];
    
    optsInitial  = struct('initialFn','OD_Initial_Gaussian','alpha',20,'y0',y0);
    opts.optsPhys = struct('optsInitial',optsInitial); 

    
    nParticles = 10^4;
    nRuns = 10;
    if(isfield(params,'BC') && strcmp(params.BC,'Even2Periodic'))
        nParticles = nParticles/2;
    end
    nSamples = 1000;

    tMax = 50;
    dt = 0.01;
    nSteps = tMax/dt;
    opts.plotTimes = 0:tMax/100:tMax;
    opts.optsStoc = struct('nParticles',nParticles, ...
                      'nSteps',nSteps,'nRuns',nRuns, ...
                      'nSamples',nSamples,'thin',1,'burnin',10^2);

    
    if(isfield(params,'eqTol'))
        opts.eqTol =  params.eqTol;
    end
    
    %optsPlot.snapshotTimes = 0:tMax/5:tMax;
    
    optsPlot.snapshotTimes = [0;1;2;5;10;20;50];
    
    % plotting settings
    optsPlot.doPdfs = true;
    
    % which loopVars to plot
    %sigmas = [0.07;0.11;0.13;0.15];
    %Rs = 0.23*ones(size(sigmas));
     
    sigmas = [0.07;0.11;0.13;0.15];
    Rs = 0.23*ones(size(sigmas));
    
    optsPlot.loopVarsPlot(1) = struct('field','optsPhys.sigma','value',sigmas);
    optsPlot.loopVarsPlot(2) = struct('field','optsPhys.R','value',Rs);
    optsPlot.varLabels.optsPhys.sigma = '$\sigma$';
    optsPlot.varLabels.optsPhys.R     = '$R$';


    optsPlot.PlotOrderParameterFinalEqTime = false;
    optsPlot.PlotDensityOrderParameterFinalEqTime = false;
    
    optsPlot.PlotDensityOrderParameter = true;
    optsPlot.PlotTimeSpaceDensity = false;
    
    optsPlot.tLog = false;
    optsPlot.logEq = false;
    optsPlot.tLim = [0;50];
    
    optsPlot.legendType = 'last';
    
    
    optsPlot.keepYLabels = [true;true;true];
    optsPlot.snapshotYLims = true;
    
    
    optsPlot.addSnapshotLabels = true;    
    
    
 end                 

 