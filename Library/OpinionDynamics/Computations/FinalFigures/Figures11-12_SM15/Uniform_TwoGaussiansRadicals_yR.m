 function [opts, optsPlot, doBCs] = Uniform_TwoGaussiansRadicals_yR(Params)
        
    doBCs = [true;true;true];    
    
    % run to equilibrium (or final time)
    opts.toEq = true;
         
    % time on a log scale
    optsPlot.tLog = true;

    [opts,optsPlot] = OD_Paper_Times_2(opts,optsPlot);
    
    
    % equilibrium on a log scale
    optsPlot.logEq = true;

    y0R_1 = Params.y0R_1;
    y0R_2 = Params.y0R_2;
 
    if(~isfield(Params,'sigmaR_1'))
        sigmaR_1 = 0.025;
    else
        sigmaR_1 = Params.sigmaR_1;
    end
    if(~isfield(Params,'sigmaR_2'))
        sigmaR_2 = 0.025;
    else
        sigmaR_2 = Params.sigmaR_2;
    end    
    
    if(~isfield(Params,'lambda'))
        lambda = 0.5;
    else
        lambda = Params.lambda;
    end

    if(~isfield(Params,'M'))
        M = 0.1;
    else
        M = Params.M;
    end
    
    
    opts.nameAddition = ['y0R1_' num2str(y0R_1) '_sigmaR1_' num2str(sigmaR_1) ...
                         '_y0R2_' num2str(y0R_2) '_sigmaR2_' num2str(sigmaR_2) ...
                         '_lambda_' num2str(lambda) '_M_' num2str(M) ];
    
    optsInitial  = struct('initialFn','OD_Initial_Uniform');
    optsRadicals = struct('radicalsFn','OD_Radicals_TwoGaussians', ...
                          'y0_1',y0R_1,'sigma_1',sigmaR_1, ...
                          'y0_2',y0R_2,'sigma_2',sigmaR_2, ...
                          'lambda',lambda,'M',M);
    
    opts.optsPhys = struct('R',0.3,'sigma',0.03,'optsInitial',optsInitial,'optsRadicals',optsRadicals);
    
    opts.toEq = true;
     
    optsPlot.PlotOrderParameterFinalEqTime = false;
    optsPlot.PlotDensityOrderParameterFinalEqTime = false;
    optsPlot.legPos = 'northwest';
    optsPlot.legendType = 'right';
    
    optsPlot.keepYLabelsQ = [false,false,true];
    optsPlot.QLim = [0;1];

    
    optsPlot.PlotDensityOrderParameter = true;
    optsPlot.PlotTimeSpaceDensity = true;
    optsPlot.tLog = true;
        
    optsPlot.doPdfs = true;
    
    
end                 

