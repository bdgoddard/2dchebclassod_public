 function [opts, optsPlot, doBCs] = Uniform_TwoGaussiansRadicals_R_sigma(params)
        
    if(isfield(params,'doBCs'))
        doBCs = params.doBCs;
    else
        doBCs = [true;true;true];
    end
     
    % parameter ranges
    if(isfield(params,'coarse') && params.coarse)
        sigmas = 0.02:0.01:0.05;
        Rs = 0.05:0.05:0.35;
    else
        sigmas = 0.02:0.005:0.05;
        Rs = 0.05:0.025:0.35;
    end

    opts.loopVars(1) = struct('field','optsPhys.R','value',Rs,'saveName','R');
    opts.loopVars(2) = struct('field','optsPhys.sigma','value',sigmas,'saveName','sigma');

    sigmas = [0.035;0.035;0.025];
    rs  = [0.25;0.3;0.1];

    optsPlot.loopVarsPlot(1) = struct('field','optsPhys.R','value',rs);
    optsPlot.loopVarsPlot(2) = struct('field','optsPhys.sigma','value',sigmas);
    
    
    % run to equilibrium (or final time)
    opts.toEq = true;
     
    % X and Y variables for order parameter plot
    optsPlot.varX = 'sigma';
    optsPlot.varXLabel = '$\sigma$';
    optsPlot.varY = 'R';
    optsPlot.varYLabel = '$R$';
    
    % time on a log scale
    optsPlot.tLog = true;

    optsPlot.snapshotTimes = [0;1;5;10;50;200;10^3;10^4];
    [opts,optsPlot] = OD_Paper_Times_4(opts,optsPlot);
    
    
    % equilibrium on a log scale
    optsPlot.logEq = true;

 
    if(~isfield(params,'y0R_1'))
        y0R_1 = 0.2;
    else
        y0R_1 = params.y0R_1;
    end
    if(~isfield(params,'y0R_2'))
        y0R_2 = 0.8;
    else
        y0R_2 = params.y0R_2;
    end

    if(~isfield(params,'sigmaR_1'))
        sigmaR_1 = 0.025;
    else
        sigmaR_1 = params.sigmaR_1;
    end
    if(~isfield(params,'sigmaR_2'))
        sigmaR_2 = 0.025;
    else
        sigmaR_2 = params.sigmaR_2;
    end    
    
    if(~isfield(params,'lambda'))
        lambda = 0.5;
    else
        lambda = params.lambda;
    end

    if(~isfield(params,'M'))
        M = 0.1;
    else
        M = params.M;
    end
    
    
    opts.nameAddition = ['y0R1_' num2str(y0R_1) '_sigmaR1_' num2str(sigmaR_1) ...
                         '_y0R2_' num2str(y0R_2) '_sigmaR2_' num2str(sigmaR_2) ...
                         '_lambda_' num2str(lambda) '_M_' num2str(M) ];
    
    optsInitial  = struct('initialFn','OD_Initial_Uniform');
    optsRadicals = struct('radicalsFn','OD_Radicals_TwoGaussians', ...
                          'y0_1',y0R_1,'sigma_1',sigmaR_1, ...
                          'y0_2',y0R_2,'sigma_2',sigmaR_2, ...
                          'lambda',lambda,'M',M);
    
    opts.optsPhys = struct('optsInitial',optsInitial,'optsRadicals',optsRadicals);
    
    opts.toEq = true;
    
    optsPlot.addSnapshotLabels = true;
    optsPlot.legPos = 'northwest';
    optsPlot.QLim = [0;1];
    

    if(~isfield(params,'doBCs')) % suppress for single BCs
        optsPlot.PlotDensityOrderParameterFinalEqTime = true;
        optsPlot.PlotDensityOrderParameter = false;
        optsPlot.PlotTimeSpaceDensity = false;
        
        optsPlot.legendType = 'right';
    else

        optsPlot.PlotDensityOrderParameter = true;
        optsPlot.PlotTimeSpaceDensity = true;
        optsPlot.tLog = true;
        
        if(params.doBCs(1))
            optsPlot.nameAddition = 'Periodic';
        elseif(params.doBCs(2))
            optsPlot.nameAddition = 'NoFlux';
        end
        
        optsPlot.legPos = 'north';
        optsPlot.loopVarsLegend = 'last';

        %optsPlot.legendType = 'off';
        
        optsPlot.addTitle = true;
    end
        
    %optsPlot.legendType = 'first';
    
    optsPlot.doPdfs = true;
    
    
end                 

