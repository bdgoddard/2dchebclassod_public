clear all
close all

% Figures 11 and 12
lambdas = [0.5;0.499];
nComps = length(lambdas);

params = struct;

for iComp = 1:nComps
    
    if(isfield(params,'doBCs'))
        params = rmfield(params,'doBCs');
    end
    
    lambda = lambdas(iComp);
    params.lambda = lambda;
    params.coarse = false;
    
    % full set of BCs with parameter regime plots
    cprintf('*g',['\n Uniform_TwoGaussiansRadicals_R_sigma, lambda = ' num2str(lambda) ' full \n']);
    [MOD,optsPlot] = OD_Generate_BC_Examples('Uniform_TwoGaussiansRadicals_R_sigma',params);
    
    % individual BCs with snapshots
    params.doBCs = [true;false;false]; % periodic
    cprintf('*g',['\n Uniform_TwoGaussiansRadicals_R_sigma, lambda = ' num2str(lambda) ' periodic \n']);
    [MOD,optsPlot] = OD_Generate_BC_Examples('Uniform_TwoGaussiansRadicals_R_sigma',params);

    % individual BCs with snapshots
    params.doBCs = [false;true;false]; % no flux
    cprintf('*g',['\n Uniform_TwoGaussiansRadicals_R_sigma, lambda = ' num2str(lambda) ' no flux \n']);
    [MOD,optsPlot] = OD_Generate_BC_Examples('Uniform_TwoGaussiansRadicals_R_sigma',params);
        
    close all
    
end

y0R_1s = [0.245;0.255];
y0R_2s = 1-y0R_1s;
nComps = length(y0R_1s);
params = struct;

for iComp = 1:nComps
    params.y0R_1 = y0R_1s(iComp);
    params.y0R_2 = y0R_2s(iComp);
    cprintf('*g',['\n Uniform_TwoGaussiansRadicals_yR, y0R_1 = ' num2str(params.y0R_1) ...
                  ', y0R_2 = ' num2str(params.y0R_2) '\n']);
    [MOD,optsPlot] = OD_Generate_BC_Examples('Uniform_TwoGaussiansRadicals_yR',params);

    close all

end
