 function [opts,optsPlot,doBCs] = Uniform_R_sigma_Stoc(~)
     
   % parameter ranges
    sigmas = [0.0225;0.025;0.0375;0.04;0.0425;0.045];
    Rs = 0.1125;

    opts.loopVars(1) = struct('field','optsPhys.R','value',Rs,'saveName','R');
    opts.loopVars(2) = struct('field','optsPhys.sigma','value',sigmas,'saveName','sigma');
    
    optsPlot.addSnapshotLabels = true;
    
    % run to equilibrium (or final time)
    opts.toEq = true;
    %opts.eqTol = 5*10^-4;
    
    sigmas = [0.0225;0.025;0.0375;0.04;0.045];
    Rs = 0.1125*ones(size(sigmas));
    
    optsPlot.loopVarsPlot(1) = struct('field','optsPhys.sigma','value',sigmas);
    optsPlot.loopVarsPlot(2) = struct('field','optsPhys.R','value',Rs);
    optsPlot.varLabels.optsPhys.sigma = '$\sigma$';
    optsPlot.varLabels.optsPhys.R     = '$R$';
        
    % X and Y variables for order parameter plot
    optsPlot.varX = 'sigma';
    optsPlot.varXLabel = '$\sigma$';
    optsPlot.varY = 'R';
    optsPlot.varYLabel = '$R$';
    
    % time on a log scale
    optsPlot.tLog = false;

    % equilibrium on a log scale
    optsPlot.logEq = false;
 
    opts.nameAddition = [];
    
    % initial condition
    optsInitial  = struct('initialFn','OD_Initial_Uniform');
    opts.optsPhys = struct('optsInitial',optsInitial); 

    nParticles = 10^4;
    nRuns = 10;
    nSamples = 1000;

    tMax = 50;
    dt = 0.01;
    nSteps = tMax/dt;
    opts.plotTimes = 0:tMax/100:tMax;
    opts.optsStoc = struct('nParticles',nParticles, ...
                      'nSteps',nSteps,'nRuns',nRuns, ...
                      'nSamples',nSamples,'thin',1,'burnin',10^2);
    
    
    optsPlot.snapshotTimes = [0;5;10;20;50];
    
    doBCs = [false;true;false];
    
    optsPlot.PlotDensityOrderParameterFinalEqTime = false;
    optsPlot.PlotOrderParameterFinalEqTime = false;
    
    optsPlot.PlotDensityOrderParameter = true;
    optsPlot.PlotTimeSpaceDensity = false;

    optsPlot.snapshotYLims = true;

    optsPlot.tLim = [0;50];
    
    optsPlot.legendType = 'last';
    optsPlot.legPos = 'southeast';
    optsPlot.loopVarsLegend = 'last';
    
    
%     optsPlot.QFinalLim = [0;1];
%     optsPlot.QLim = [0;1];
    
    optsPlot.doPdfs = true;
            
end                 

 