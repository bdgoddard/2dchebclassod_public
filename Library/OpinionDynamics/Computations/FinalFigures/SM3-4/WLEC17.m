 function [opts,optsPlot,doBCs] = WLEC17(~)
    
    doBCs = [true,false,false];
    
    opts.nameAddition = [];
 
    optsInitial  = struct('initialFn','OD_Initial_Gaussian','alpha',20,'y0',0.5);
    optsPhys = struct('R',0.2','optsInitial',optsInitial);

    plotTimes = {0:0.2:1,0:0.2:1,0:0.2:1,0:4:20};
    sigmas    = {0.1,0.15,0.2,0.25};

    nComps = length(plotTimes);
    for iComp = 1:nComps
        opts(iComp).plotTimes = plotTimes{iComp}*(2*pi);  % rescale to get rid of erroneous
        optsPhys.sigma = sigmas{iComp}/sqrt(2*pi);    % factor of 2*pi
        opts(iComp).optsPhys = optsPhys;
    end    
    
    optsPlot.PlotSnapshotsDensityRadicals = true;
    
    optsPlot.legendType = 'right';
    
    optsPlot.separate = true;
    optsPlot.yLim = [0,14];
    optsPlot.wide = true;
    
    optsPlot.doPdfs = true;
end                 

