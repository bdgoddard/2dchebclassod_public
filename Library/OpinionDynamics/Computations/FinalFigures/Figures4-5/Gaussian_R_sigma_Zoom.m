 function [opts,optsPlot,doBCs] = Gaussian_R_sigma_Zoom(Params)
 
    sigmas = 0.07:0.01:0.15;
    Rs = 0.15:0.02:0.25;    

    opts.loopVars(1) = struct('field','optsPhys.R','value',Rs,'saveName','R');
    opts.loopVars(2) = struct('field','optsPhys.sigma','value',sigmas,'saveName','sigma');
    
    % run to equilibrium (or final time)
    opts.toEq = true;
 
    % X and Y variables for order parameter plot
    optsPlot.varX = 'sigma';
    optsPlot.varXLabel = '$\sigma$';
    optsPlot.varY = 'R';
    optsPlot.varYLabel = '$R$';

    % which boundary conditions to consider
    doBCs = [true;true;true]; 
 
     % initial condition
    if(~isfield(Params,'y0'))
        y0 = 0.3;
    else
        y0 = Params.y0;
    end
    opts.nameAddition = ['y0_' num2str(y0)];
    
    optsInitial  = struct('initialFn','OD_Initial_Gaussian','alpha',20,'y0',y0);
    opts.optsPhys = struct('optsInitial',optsInitial); 

    [opts,optsPlot] = OD_Paper_Times_4(opts,optsPlot);
    
    if(isfield(Params,'eqTol'))
        opts.eqTol =  Params.eqTol;
    end
    
    % plotting settings
    optsPlot.doPdfs = true;
    
    % which loopVars to plot
    sigmas = [0.07;0.11;0.13;0.15];
    Rs = 0.23*ones(size(sigmas));
        
    optsPlot.loopVarsPlot(1) = struct('field','optsPhys.sigma','value',sigmas);
    optsPlot.loopVarsPlot(2) = struct('field','optsPhys.R','value',Rs);
    optsPlot.varLabels.optsPhys.sigma = '$\sigma$';
    optsPlot.varLabels.optsPhys.R     = '$R$';


    optsPlot.PlotDensityOrderParameterFinalEqTime = true;
    optsPlot.PlotDensityOrderParameter = true;
    optsPlot.PlotTimeSpaceDensity = true;
    
    optsPlot.tLog = true;
    optsPlot.logEq = true;

    optsPlot.legendType = 'last';
    
    optsPlot.keepYLabelsQ = [false,false,true];
    optsPlot.QLim = [0, 0, 0; 1, 1, 2];
    optsPlot.QFinalLim = [0;2];
    
    optsPlot.addSnapshotLabels = true;    
    
 end                 

 