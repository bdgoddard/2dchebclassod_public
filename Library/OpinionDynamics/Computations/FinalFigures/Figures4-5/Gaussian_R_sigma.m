 function [opts,optsPlot,doBCs] = Gaussian_R_sigma(Params)
     
    %parameter ranges
    sigmas = 0.05:0.025:0.25;
    Rs = 0.05:0.05:0.5;  
    
    opts.loopVars(1) = struct('field','optsPhys.R','value',Rs,'saveName','R');
    opts.loopVars(2) = struct('field','optsPhys.sigma','value',sigmas,'saveName','sigma');
    
    % run to equilibrium (or final time)
    opts.toEq = true;
    opts.eqTol = 10^-3;
    
    % X and Y variables for order parameter plot
    optsPlot.varX = 'sigma';
    optsPlot.varXLabel = '$\sigma$';
    optsPlot.varY = 'R';
    optsPlot.varYLabel = '$R$';
    
    % time on a log scale
    optsPlot.tLog = true;

    % equilibrium on a log scale
    optsPlot.logEq = true;

    % which boundary conditions to consider
    doBCs = [true;true;true];
    %doBCs = [false;true;false];
    
       
    % initial condition
    if(~isfield(Params,'y0'))
        y0 = 0.3;
    else
        y0 = Params.y0;
    end
    opts.nameAddition = ['y0_' num2str(y0)];
    
    optsInitial  = struct('initialFn','OD_Initial_Gaussian','alpha',20,'y0',y0);
    opts.optsPhys = struct('optsInitial',optsInitial); 
       
    [opts,optsPlot] = OD_Paper_Times_4(opts,optsPlot);
    
    optsPlot.PlotDensityOrderParameterFinalEqTime = true;
    
    optsPlot.PlotDensityOrderParameter = false;
    optsPlot.PlotTimeSpaceDensity = false;
    
    optsPlot.doPdfs = true;
            
end                 

 