clear all
close all

% Figure 4 (top row)
cprintf('*g',['\n Gaussian_R_sigma_y0s \n']);
Gaussian_R_sigma_y0s;

% Figure 4 (bottom left) and Figure 5
cprintf('*g',['\n Gaussian_R_sigma_Zoom \n']);
params.y0 = 0.3;
[MOD,optsPlot] = OD_Generate_BC_Examples('Gaussian_R_sigma_Zoom',params);