y0s = [0.2;0.3];
nComps = length(y0s);

for iComp = 1:nComps
    y0 = y0s(iComp);
    cprintf('*g',['\n Gaussian_R_sigma, y0 = ' num2str(y0) '\n']);
    params.y0 = y0;
    [MOD_Zoom,optsPlot_Zoom] = OD_Generate_BC_Examples('Gaussian_R_sigma',params);

    %     % small sigma doesn't do a lot here
%     [MOD_Small,optsPlot_Small] = OD_Generate_BC_Examples('OD_Paper_Gaussian_R_sigma_Small',params);
    close all
end
