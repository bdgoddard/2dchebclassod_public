% Wang figure 2
% nParticles=100;
% tMax = 16*10^3;
% nSteps = 10^5; 
% RSigma = [0.1, 0.01];

RSigma = [0.05, 0.01;
          0.05, 0.02;
          0.05, 0.03;
          0.2, 0.07;
          0.2, 0.09;
          0.2, 0.11];

nComps = size(RSigma,1);

for iComp = 1:nComps
    opts(iComp).nParticles = 300;
    opts(iComp).tMax = 500;
    opts(iComp).L = 1;
    opts(iComp).nSteps = 10^3;
    opts(iComp).IC = 'delta';
    opts(iComp).x0 = 0.5;
    opts(iComp).R = RSigma(iComp,1);
    opts(iComp).sigma = RSigma(iComp,2);
end

data = doOD_SDE(opts);

figure('Position',[100,100,1200,800])

optsPlot.xLim = [200,500];
optsPlot.plotSkip = 3;

for iPlot = 1:nComps
    optsPlot.ha = subplot(2,3,iPlot);
    if(iPlot>3)
        optsPlot.xLim = [200,400];
    end

    Plot_OD_SDE_x(data.SDE(iPlot),optsPlot);    
end

%Plot_OD_SDE_Q(data.SDE(1));