function MOD = Test_OD_Wang_Rescaled()

    AddPaths();

    Phys_Area = struct('shape','FourierLine','N',100,'yMin',0,'yMax',1);
 
    Plot_Area = struct('N',400,'yMin',0,'yMax',1);
    
    optsNum = struct('PhysArea',Phys_Area, ...
                     'PlotArea',Plot_Area, ...
                     'dataSubDir',mfilename);
                 
    optsInitial  = struct('initialFn','HKInitial_Gaussian','alpha',20,'y0',0.5);
    optsKernel   = struct('alpha',1);
    
    optsPhys = struct('R',0.2, ...
                      'optsInitial',optsInitial, ....
                      'optsKernel',optsKernel);

    plotTimes = {0:0.2:1,0:0.2:1,0:0.2:1,0:4:20};
    sigmas    = {0.1,0.15,0.2,0.25};

    nComps = length(plotTimes);
    
    opts = struct;
    
    for iComp = 1:nComps
        optsNum.plotTimes = plotTimes{iComp}*(2*pi);  % rescale to get rid of erroneous
        optsPhys.sigma = sigmas{iComp}/sqrt(2*pi);    % factor of 2*pi
        opts.OD(iComp).optsNum = optsNum;
        opts.OD(iComp).optsPhys = optsPhys;
    end    
        
    DataDir = ['OpinionDynamics' filesep mfilename filesep 'Multi'];    
    MOD = DataStorage(DataDir,@DoMultiOpinionDynamics,opts,[],[],[],false);

    
    optsPlot.yLim = [0;14];
    MOD.PlotSnapshotsDensityRadicals(optsPlot);
        
          
end