 function MOD = Test_OD_Even2Periodic_Fig3()
    
    AddPaths();
 
    Phys_Area = struct('shape','FourierLine','N',200,'yMin',-1,'yMax',1,...
                        'even2periodic',true);
 
    Plot_Area = struct('N',400,'yMin',0,'yMax',1);
    
    plotTimes = [0:10^2:10^3];
    
    optsNum = struct('PhysArea',Phys_Area, ...
                     'PlotArea',Plot_Area, ...                  
                     'plotTimes',plotTimes, ...
                     'dataSubDir',mfilename );
                 
    optsRadicals = struct('radicalsFn','HKRadicals_Triangle','S',0.1,'A',0.7);
    optsInitial  = struct('initialFn','HKInitial_Uniform');
    
    optsPhys = struct('R',0.1, ...
                      'optsInitial',optsInitial,'optsRadicals',optsRadicals);
    
%     sigmas = 0.01:0.05:0.16;
%     Ms = 0:0.2:1;

    sigmas = 0.01:0.01:0.16;
    Ms = 0:0.1:1;

%     sigmas = 0.01:0.005:0.15;
%     Ms = 0:0.02:1;
    
    opts.loopVars(1) = struct('field','optsPhys.optsRadicals.M','value',Ms,'saveName','M');
    opts.loopVars(2) = struct('field','optsPhys.sigma','value',sigmas,'saveName','sigma');
        
    opts.optsPhys = optsPhys;
    opts.optsNum = optsNum;
    
    DataDir = ['OpinionDynamics' filesep mfilename filesep 'Multi'];    
    MOD = DataStorage(DataDir,@DoMultiOpinionDynamics,opts,[],[],[],false);

    optsQ.cLim = [0.2,1];

    MOD.PlotOrderParameterFinal(optsQ);
    
end                 

