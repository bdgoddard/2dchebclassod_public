 function MOD = Test_OD_Even2Periodic_Fig7()
    
    AddPaths();
 
    Phys_Area = struct('shape','FourierLine','N',200,'yMin',-1,'yMax',1,...
                        'even2periodic',true);
 
    Plot_Area = struct('N',400,'yMin',0,'yMax',1);
    
    plotTimes = [0:1:100];
    
    optsNum = struct('PhysArea',Phys_Area, ...
                     'PlotArea',Plot_Area, ...                  
                     'plotTimes',plotTimes, ...
                     'dataSubDir',mfilename );
                 
    optsRadicals = struct('radicalsFn','HKRadicals_Triangle','S',0.1);
    optsInitial  = struct('initialFn','HKInitial_Uniform');
    
    optsPhys = struct('R',0.1, 'sigma',0.01, 'optsInitial',optsInitial);
    
    MA = [ 0.05, 0.85; ...
           0.15, 0.85; ...
           0.15, 0.7];
                      
    nComps = size(MA,1);
    
    opts = struct;
    for iComp = 1:nComps
        optsRadicals.M = MA(iComp,1);
        optsRadicals.A = MA(iComp,2);
        optsPhys.optsRadicals = optsRadicals;
        opts.OD(iComp).optsNum = optsNum;
        opts.OD(iComp).optsPhys = optsPhys;
    end
       
    DataDir = ['OpinionDynamics' filesep mfilename filesep 'Multi'];    
    MOD = DataStorage(DataDir,@DoMultiOpinionDynamics,opts,[],[],[],false);
    
    optsPlot.yLim = [0,10];
    
    snapshotTimes = [10;20;80];
    nTimes = length(snapshotTimes);
    nComps = length(opts.OD);
    
    figure('Position',[100,100,1200,800]);
    optsPlot.legend = '$\rho$';
    optsPlot.latex  = true;
    for iTime = 1:nTimes
        tPos = find(plotTimes == snapshotTimes(iTime));
        for iComp = 1:nComps
            optsPlot.ha = subplot(nTimes,nComps,(iTime-1)*nComps + iComp);
            MOD.Comps(iComp).PlotDensity(MOD.Comps(iComp).dynamicsResult.rho_t(:,tPos),optsPlot);
        end
    end

    figure('Position',[100,100,800,800]);
    linestyles = {'-','-.','--'};
    optsPlot.ha = axes;
    optsPlot.yLim = [0.15,0.3];
    for iComp =1:nComps
        optsPlot.linestyle = linestyles{iComp};
        MOD.Comps(iComp).PlotOrderParameter(optsPlot);
        hold on
    end
    legend('S1','S2','S3')
    
    
    
end                 

