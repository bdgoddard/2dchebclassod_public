 function OD = Test_OD_Even2Periodic_Fig5()
    
    AddPaths();
 
    Phys_Area = struct('shape','FourierLine','N',200,'yMin',-1,'yMax',1,...
                        'even2periodic',true);
 
    Plot_Area = struct('N',400,'yMin',0,'yMax',1);
    
    plotTimes = [0;10;20;40;80];
    
    optsNum = struct('PhysArea',Phys_Area, ...
                     'PlotArea',Plot_Area, ...                  
                     'plotTimes',plotTimes, ...
                     'dataSubDir',mfilename );
                 
    optsRadicals = struct('radicalsFn','HKRadicals_Triangle','S',0.1,'A',0.7,'M',0.1);
    optsInitial  = struct('initialFn','HKInitial_Uniform');
    
    optsPhys = struct('R',0.1, 'sigma', 0.01, ...
                      'optsInitial',optsInitial,'optsRadicals',optsRadicals);
            
    opts.optsPhys = optsPhys;
    opts.optsNum = optsNum;
    
   
    OD = DoOpinionDynamics(opts);
    
    nPlots = length(plotTimes);
    
    optsPlot.yLim = [0,10];
    
    figure('Position',[100,100,800,1200]);
    for iPlot = 1:nPlots
        optsPlot.ha = subplot(nPlots,1,iPlot);
        optsPlot.linestyle = '-';
        optsPlot.legend = ['t = ' num2str(plotTimes(iPlot))];
        OD.PlotDensity(OD.dynamicsResult.rho_t(:,iPlot),optsPlot);
        hold on
        optsPlot.linestyle = '--';
        optsPlot.legend = 'Intial';
        OD.PlotDensity(OD.dynamicsResult.rho_t(:,1),optsPlot);
    end

    
end                 

