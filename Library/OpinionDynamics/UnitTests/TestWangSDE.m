
%optsPhys
opts.R = 0.2;
opts.sigma = 0.1/sqrt(2*pi);

opts.nParticles = 300;
opts.L = 1; % optsPhys
opts.nSteps = 10^3;
opts.nRuns = 50;
%opts.tMax = 20*2*pi;
opts.tMax = 1*2*pi;

opts.nSaves = 100;

optsIC.nSamples = 10^2; 
optsIC.nParticles = opts.nParticles;

optsIC.thin = 1; 
optsIC.burnin = 10^2; 

% optsPhys
optsIC.rho = 'HKInitial_Gaussian'; 
optsIC.params.alpha = 20; 
optsIC.params.y0 = 0.5;

% optsNum
optsIC.yMin = 0;
optsIC.yMax = opts.L;

opts.optsIC = optsIC;

xIC = ODE_SDE_IC(optsIC);


[x,t] = OD_SDE_Runs(opts,xIC);

data.x = x;
data.t = t;
data.opts = opts;

