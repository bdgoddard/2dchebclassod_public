function TestPlotSampleInitialCondition(x,opts)

yMin = opts.yMin;
yMax = opts.yMax;
nSamples = opts.nSamples;

dy = (yMax-yMin)/30;

edges = [yMin: dy : yMax];

[N,edges] = histcounts(x,edges);

N = N/nSamples/dy;

nBins = length(edges)-1;
mids = edges(1:nBins) + dy/2;

figure

scatter(mids,N);

params = opts.params;
rho = str2func(opts.rho);

dy_plot = (yMax-yMin)/100;

y_plot = [yMin : dy_plot : yMax]';
params.Int = dy_plot*ones(1,length(y_plot));

rho_plot = rho(y_plot,params);

hold on
plot(y_plot,rho_plot)

end