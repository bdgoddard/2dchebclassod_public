function [x,opts] = TestSampleInitialCondition()

yMin = 0;
yMax = 1;

opts.nSamples = 10^4; 
opts.thin = 1; 
opts.burnin = 10^2; 

opts.yMin = yMin; 
opts.yMax = yMax; 

opts.rho = 'HKInitial_Gaussian'; 
opts.params.alpha = 20; 
opts.params.y0 = 0.5;

x=SampleInitialCondition(opts);

TestPlotSampleInitialCondition(x,opts);

end