function aBox = Test_BoxDiscConvolution(opts)
   
    AddPaths();
    global dirData;
        
    %----------------------------------------------------------------------
    % Initialisation
    %----------------------------------------------------------------------

    PhysArea = struct('shape','Box','N',[30;30], ...
                        'y1Min',0,'y1Max',1,'y2Min',0,'y2Max',1);
    
    
    PlotArea = struct('N',[50,50],'y1Min',0,'y1Max',1,'y2Min',0,'y2Max',1);
      
    shapeClass = str2func(PhysArea.shape);
    aBox = shapeClass(PhysArea);
    
    [Pts,Diff,Int,Ind,~] = aBox.ComputeAll(PlotArea); 

    R = 0.2;

    shapeParams.R = R;
    shapeParams.N = [10;10];
    shapeParams.NT = [10;10];

    y1 = Pts.y1_kv;
    y2 = Pts.y2_kv;
    
    params.alpha1 = 1;
    params.alpha2 = 1;
    params.shapeParams = shapeParams;
    params.PhysArea = PhysArea;

    %----------------------------------------------------------------------
    % Construct edge lines, which are easy
    %----------------------------------------------------------------------
    
    x = linspace(0,1,100)';
    
    % Bottom
    [exactConvB1,exactConvB2] = exactConvolutionB(x);
    
    % Top, Left and Right in terms of Bottom
    exactConvT1 = -flip(exactConvB1);
    exactConvT2 = -flip(exactConvB2);

    exactConvL1 = exactConvB2;
    exactConvL2 = exactConvB1;

    exactConvR1 = exactConvT2;
    exactConvR2 = exactConvT1;
        
    % Middle
    [exactConvM1,exactConvM2] = exactConvolutionM(x);

    %----------------------------------------------------------------------
    % Construct bottom left quadrant
    %----------------------------------------------------------------------
    
    y1Plot = aBox.Interp.pts1;
    y2Plot = aBox.Interp.pts2;
    
    BLMask = (y1Plot<=0.5 & y2Plot<=0.5);    
    TLMask = (y1Plot<=0.5 & y2Plot>=0.5);
    BRMask = (y1Plot>=0.5 & y2Plot<=0.5);
    TRMask = (y1Plot>=0.5 & y2Plot>=0.5);
    
    X1BL = y1Plot(BLMask);
    X2BL = y2Plot(BLMask);
    [exactConvBL1,exactConvBL2] = exactConvolutionBL(X1BL,X2BL);

    X1TL = y1Plot(TLMask);
    X2TL = y2Plot(TLMask);
    [exactConvTL1,exactConvTL2] = exactConvolutionBL(X1TL,1-X2TL);
    exactConvTL2 = -exactConvTL2;
    
    X1BR = y1Plot(BRMask);
    X2BR = y2Plot(BRMask);
    [exactConvBR1,exactConvBR2] = exactConvolutionBL(1-X1BR,X2BR);
    exactConvBR1 = -exactConvBR1;

    X1TR = y1Plot(TRMask);
    X2TR = y2Plot(TRMask);
    [exactConvTR1,exactConvTR2] = exactConvolutionBL(1-X1TR,1-X2TR);
    exactConvTR1 = -exactConvTR1;
    exactConvTR2 = -exactConvTR2;
    
    
    %----------------------------------------------------------------------
    % Construct numerical approximation everywhere
    %----------------------------------------------------------------------

    dataDir = [dirData filesep 'HKBox'];
    if(~exist(dataDir,'dir'))
        mkdir(dataDir);
    end
    
    func = @convolutionWrapper;
    recompute = false;
    data = DataStorage(dataDir,func,params,[],recompute);
    
    Conv = data.Conv;
    
    Conv1 = Conv(:,:,1);
    Conv2 = Conv(:,:,2);

    %----------------------------------------------------------------------
    % Compare
    %----------------------------------------------------------------------
    
    figure('Position',[100,100,1200,800]);
    rho0 = ones(size(y1));
    numConv1 = Conv1*rho0;
    numConv2 = Conv2*rho0;
    
    numConv1Plot = aBox.Interp.InterPol*numConv1;
    numConv2Plot = aBox.Interp.InterPol*numConv2;
    
    errs1 = [abs(numConv1Plot(BLMask) - exactConvBL1); ...
             abs(numConv1Plot(BRMask) - exactConvBR1); ...
             abs(numConv1Plot(TLMask) - exactConvTL1); ...
             abs(numConv1Plot(TRMask) - exactConvTR1)];
    err1 = max(errs1);
    errs2 = [abs(numConv2Plot(BLMask) - exactConvBL2); ...
             abs(numConv2Plot(BRMask) - exactConvBR2); ...
             abs(numConv2Plot(TLMask) - exactConvTL2); ...
             abs(numConv2Plot(TRMask) - exactConvTR2)];
    err2 = max(errs2);
    
    disp(['Maximum L-infinity error in Conv1 = ' num2str(err1)]);
    disp(['Maximum L-infinity error in Conv2 = ' num2str(err2)]);
    
    
    subplot(1,2,1)
    aBox.plot(numConv1);    
    hold on
    plot3(zeros(size(x)),x,exactConvL1,'Color','r','LineWidth',3);
    plot3(ones(size(x)),x,exactConvR1,'Color','r','LineWidth',3);
    plot3(x,zeros(size(x)),exactConvB1,'Color','r','LineWidth',3);
    plot3(x,ones(size(x)),exactConvT1,'Color','r','LineWidth',3);

    plot3(0.5*ones(size(x)),x,exactConvM1,'Color','r','LineWidth',3);
    plot3(x,0.5*ones(size(x)),exactConvM2,'Color','r','LineWidth',3);
    scatter3(X1BL,X2BL,exactConvBL1,'ro');
    scatter3(X1TL,X2TL,exactConvTL1,'ro');
    scatter3(X1BR,X2BR,exactConvBR1,'ro');
    scatter3(X1TR,X2TR,exactConvTR1,'ro');
    
    subplot(1,2,2)
    aBox.plot(numConv2);
    hold on
    plot3(zeros(size(x)),x,exactConvL2,'Color','r','LineWidth',3);
    plot3(ones(size(x)),x,exactConvR2,'Color','r','LineWidth',3);
    plot3(x,zeros(size(x)),exactConvB2,'Color','r','LineWidth',3);
    plot3(x,ones(size(x)),exactConvT2,'Color','r','LineWidth',3);
    
    plot3(0.5*ones(size(x)),x,exactConvM2,'Color','r','LineWidth',3);
    plot3(x,0.5*ones(size(x)),exactConvM1,'Color','r','LineWidth',3);
    
    scatter3(X1BL,X2BL,exactConvBL2,'ro');
    scatter3(X1TL,X2TL,exactConvTL2,'ro');
    scatter3(X1BR,X2BR,exactConvBR2,'ro');
    scatter3(X1TR,X2TR,exactConvTR2,'ro');
    
   
    function data = convolutionWrapper(params,otherInputs)

        shapeParams = params.shapeParams;
        
        data.Conv = aBox.ComputeConvolutionMatrix_Pointwise(@kernel,shapeParams);
        
        function K = kernel(y1,y2)
            alpha1 = params.alpha1;
            alpha2 = params.alpha2;
            K1 = alpha1*y1;
            K2 = alpha2*y2;
            K = [K1, K2];
        end
        
    end

    function [conv1,conv2] = exactConvolutionBL(X1,X2)
        
        maskL = (X1<=R & X2>=R);
        maskB = (X2<=R & X1>=R);
        maskC = ( (X1.^2 + X2.^2)<=R^2 );
        maskW = (X1<=R & X2<=R & ~maskC);


        X1L = X1(maskL);
        X2B = X2(maskB);
        X1C = X1(maskC);
        X2C = X2(maskC);
        X1W = X1(maskW);
        X2W = X2(maskW);


        aL = X1L/R;
        aB = X2B/R;

        aLC = X1C/R;
        aBC = X2C/R;

        aLW = X1W/R;
        aBW = X2W/R;


        segment1 = zeros(size(X1));
        segment2 = zeros(size(X1));

        triangleL1 = zeros(size(X1));
        triangleL2 = zeros(size(X1));

        triangleB1 = zeros(size(X1));
        triangleB2 = zeros(size(X1));

        rectangle1 = zeros(size(X1));
        rectangle2 = zeros(size(X1));


        segment1(maskL) = -2*sqrt(1-aL.^2)*R^3/3;
        segment2(maskL) = 0;

        segment1(maskB) = 0;
        segment2(maskB) = -2*sqrt(1-aB.^2)*R^3/3;

        segment1(maskC) = R^3/3 * ( - sqrt(1-aLC.^2) - aBC );
        segment2(maskC) = R^3/3 * ( - aLC - sqrt(1-aBC.^2) );

        segment1(maskW) = -2*R^3/3 * sqrt(1-aLW.^2);
        segment2(maskW) = -2*R^3/3 * sqrt(1-aBW.^2);


        triangleL1(maskL) = 2*X1L.^3/3 .* sqrt(1-aL.^2)./aL;
        triangleL2(maskL) = 0;

        triangleB1(maskB) = 0;
        triangleB2(maskB) = 2*X2B.^3/3 .* sqrt(1-aB.^2)./aB;

        triangleL1(maskC) = X1C.^3/3 .* sqrt(1-aLC.^2)./aLC;
        triangleL2(maskC) = (-1/2) * X1C.^3/3 .* (1-aLC.^2)./aLC.^2;

        triangleB1(maskC) = (-1/2) * X2C.^3/3 .* (1-aBC.^2)./aBC.^2;
        triangleB2(maskC) = X2C.^3/3 .* sqrt(1-aBC.^2)./aBC;

        triangleL1(maskW) = 2*X1W.^3/3 .* sqrt(1-aLW.^2)./aLW;
        triangleL2(maskW) = 0;

        triangleB1(maskW) = 0;
        triangleB2(maskW) = 2*X2W.^3/3 .* sqrt(1-aBW.^2)./aBW;


        triangleL1(X1==0) = 0;
        triangleL2(X1==0) = 0;
        triangleB1(X2==0) = 0;
        triangleB2(X2==0) = 0;

        rectangle1(maskC) = X1C.^2.*X2C/2;
        rectangle2(maskC) = X2C.^2.*X1C/2;

        conv1 = segment1 + triangleL1 + triangleB1 + rectangle1;
        conv2 = segment2 + triangleL2 + triangleB2 + rectangle2;
        
    end


    function [conv1,conv2] = exactConvolutionB(x)
        
        xMaskL = x<R;
        xMaskR = x>1-R;
        xL = x(xMaskL);
        xR = x(xMaskR);

        alphaL = xL/R;
        alphaR = (1-xR)/R;

        % Bottom
        segmentB1 = zeros(size(x));
        segmentB2 = -2*ones(size(x))*R^3/3;

        triangleB1 = zeros(size(x));
        triangleB2 = zeros(size(x));

        segmentB1(xMaskL) = -sqrt(1-alphaL.^2)*R^3/3;
        segmentB2(xMaskL) = -(1+alphaL)*R^3/3;

        segmentB1(xMaskR) = sqrt(1-alphaR.^2)*R^3/3;
        segmentB2(xMaskR) = -(1+alphaR)*R^3/3;

        triangleB1(xMaskL) = xL.^3/3 .* sqrt(1-alphaL.^2)./alphaL;
        triangleB2(xMaskL) = -xL.^3/3 .* (1-alphaL.^2)./alphaL.^2/2;

        triangleB1(xMaskR) = -(1-xR).^3/3 .* sqrt(1-alphaR.^2)./alphaR;
        triangleB2(xMaskR) = -(1-xR).^3/3 .* (1-alphaR.^2)./alphaR.^2/2;

        triangleB1(isnan(triangleB1)) = 0;
        triangleB2(isnan(triangleB2)) = 0;

        conv1 = segmentB1 + triangleB1;
        conv2 = segmentB2 + triangleB2;
    end

    function [conv1,conv2] = exactConvolutionM(x)
        
        xMaskL = x<R;
        xMaskR = x>1-R;
        xL = x(xMaskL);
        xR = x(xMaskR);

        alphaL = xL/R;
        alphaR = (1-xR)/R;

        segmentM1 = zeros(size(x));
        segmentM2 = zeros(size(x));
        triangleM1 = zeros(size(x));
        triangleM2 = zeros(size(x));    

        segmentM2(xMaskL) = -2*sqrt(1-alphaL.^2)*R^3/3;
        triangleM2(xMaskL) = 2*sqrt(1-alphaL.^2)./alphaL.*xL.^3/3;

        segmentM2(xMaskR) = 2*sqrt(1-alphaR.^2)*R^3/3;
        triangleM2(xMaskR) = -2*sqrt(1-alphaR.^2)./alphaR.*(1-xR).^3/3;

        triangleM2(isnan(triangleM2)) = 0;

        conv1 = segmentM1 + triangleM1;
        conv2 = segmentM2 + triangleM2;
    end

% To check convolution areas
%     for i = 1:length(y1)
%         shapeParams.Origin = [y1(i);y2(i)];
% %         shapeParams.Origin = [0.25;1];
% 
%         disc = Disc(shapeParams);
%         
%         area = Intersect(aBox,disc);
%         hold off
%         scatter(area.pts.y1_kv,area.pts.y2_kv);
%         hold on
%         disc.PlotGrid;
%         scatter(y1(i),y2(i),'r');
%         
%         axis('equal');
%         xlim([0,1])
%         ylim([0,1])
%         
%         pause(0.1)
%         
%     end
        
%     return



end