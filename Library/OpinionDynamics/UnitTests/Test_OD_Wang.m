function MOD = Test_OD_Wang()

    AddPaths();

    Phys_Area = struct('shape','FourierLine','N',100,'yMin',0,'yMax',1);
 
    Plot_Area = struct('N',400,'yMin',0,'yMax',1);
    
    optsNum = struct('PhysArea',Phys_Area, ...
                     'PlotArea',Plot_Area, ...
                     'dataSubDir',mfilename);
                 
    optsInitial  = struct('initialFn','HKInitial_Gaussian','alpha',20,'y0',0.5);
    optsKernel   = struct('alpha',2*pi);
    
    optsPhys = struct('R',0.2, ...
                      'optsInitial',optsInitial, ....
                      'optsKernel',optsKernel);

    plotTimes = {0:0.2:1,0:0.2:1,0:0.2:1,0:4:20};
    sigmas    = {0.1,0.15,0.2,0.25};

    nComps = length(plotTimes);
    
    opts = struct;
    
    for iComp = 1:nComps
        optsNum.plotTimes = plotTimes{iComp};
        optsPhys.sigma = sigmas{iComp};
        opts.OD(iComp).optsNum = optsNum;
        opts.OD(iComp).optsPhys = optsPhys;
    end    
        
    DataDir = ['OpinionDynamics' filesep mfilename filesep 'Multi'];    
    MOD = DataStorage(DataDir,@DoMultiOpinionDynamics,opts,[],[],[],false);

    
%     MOD = MultiOpinionDynamics(opts);
%     MOD.ComputeDynamics;
    optsPlot.yLim = [0;14];
    MOD.PlotSnapshotsDensityRadicals(optsPlot);
                  
end