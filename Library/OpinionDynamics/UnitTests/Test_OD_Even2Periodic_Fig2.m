 function MOD = Test_OD_Even2Periodic_Fig2()
    
    AddPaths();
 
    Phys_Area = struct('shape','FourierLine','N',200,'yMin',-1,'yMax',1,...
                        'even2periodic',true);

%     Phys_Area = struct('shape','FourierLine','N',100,'yMin',-1,'yMax',1,...
%                         'even2periodic',true);
                    
                    
    Plot_Area = struct('N',400,'yMin',0,'yMax',1);
    
    plotTimes = [0:10^2:10^4];
    
    optsNum = struct('PhysArea',Phys_Area, ...
                     'PlotArea',Plot_Area, ...                  
                     'plotTimes',plotTimes, ...
                     'dataSubDir',mfilename);
                 
    optsRadicals = struct('radicalsFn','OD_Radicals_Triangle','S',0.1,'A',0.7,'M',0.1);
    optsInitial  = struct('initialFn','OD_Initial_Uniform');
    
    optsPhys = struct('R',0.1,'optsInitial',optsInitial,'optsRadicals',optsRadicals);
    
    opts.optsPhys = optsPhys;
    opts.optsNum  = optsNum;
    
    sigmas = [0.01;0.02;0.03;0.04;0.05];
   
    opts.loopVars = struct('field','optsPhys.sigma','value',sigmas,'saveName','sigma');
    
    DataDir = ['OpinionDynamics' filesep mfilename filesep 'Multi'];    
    MOD = DataStorage(DataDir,@DoMultiOpinionDynamics,opts,[],[],[],false);

    figure('Position',[100,100,1200,800])
    nComps = length(sigmas);
    
    haRho = zeros(nComps,1);
    haQ   = zeros(nComps,1);
    
    nRows = nComps;
  
    for iComp = 1:nComps
        haRho(iComp) = subplot(nRows,2,2*(iComp-1)+1);
        haQ(iComp)   = subplot(nRows,2,2*iComp);
    end
    
    optsRho.ha = haRho;
    
    optsQ.ha = haQ;
    optsQ.xLim = [0,5000];
    optsQ.yLim = [0,1];
    optsQ.linecolor = 'r';
    
    MOD.PlotDensityRadicalsFinal(optsRho);
    MOD.PlotOrderParameter(optsQ);
        
end                 

