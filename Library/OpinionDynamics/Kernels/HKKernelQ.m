function K = HKKernelQ(r,params)

    if(nargin>1 && isfield(params,'alpha'))
        alpha = params.alpha;
    else
        alpha = 1;
    end
    
    K = alpha*ones(size(r));

end