function K = HKKernel(r,params)

    if(nargin>1 && isfield(params,'alpha'))
        alpha = params.alpha;
    else
        alpha = 1;
    end
    
    K = alpha*r;

end