function [xSave,tSave] = OD_SDE(opts,xIC)

nParticles = opts.nParticles;
tMax = opts.tMax;
nSteps = opts.nSteps;
L = opts.L;
R = opts.R;
sigma = opts.sigma;

if(isfield(opts,'nSaves'))
    nSaves = opts.nSaves;
else
    nSaves = opts.nSteps;
end

saveStep = ceil(nSteps/nSaves);


h = tMax/nSteps;
t = 0:h:opts.tMax;

% precompute noise
W=randn(nParticles, nSteps+1);

% preallocate positions
xSave = zeros(nParticles, nSaves+1);
tSave = zeros(nSaves+1,1);

xSave(:,1) = xIC;
tSave(1) = 0;

x = xIC;      


% order parameter
% Q = zeros(nSteps+1,1);
% 
% [~,nRij] = F_SDE(x(:,1),R);
% 
% Q(1) = (nParticles^-2)*sum(nRij);

iSave = 1;

for iStep = 1:nSteps

    iStep

    %[F,nRij] = F_SDE(x(:,iStep),R);

    %x(:,iStep+1) = x(:,iStep) - h*F + sigma*sqrt(h).*W(:,iStep);

    %x(:,iStep+1) = mod(x(:,iStep+1),L);    

    [F,nRij] = F_SDE(x,R);

    x = x - h*F + sigma*sqrt(h).*W(:,iStep);

    x = mod(x,L);    
    
    if(mod(iStep,saveStep)==0 || iStep == nSteps)
        iSave = iSave + 1;
        xSave(:,iSave) = x;
        tSave(iSave) = t(iStep+1);
    end
    
    
%     Q(iStep+1) = (nParticles^-2)*sum(nRij);

end
    
% data.t = t;
% data.x = x;
% data.Q = Q;
% data.opts = opts;



%--------------------------------------------------------------------------

function [F,nRij] = F_SDE(x,R)
%F_SDE Calculates F for SDE solver

    RijFull = getRijSigned1DPeriodic(x,L);
    Rij = RijFull;
    
    Rij(abs(Rij)>R) = 0; %Works out which opinions (i,j) aren't within R
                         %and sets all these Rij to be zero
    sumRij = sum(Rij,2); %For row i, sum over x_i - x_j
    F = (sumRij/nParticles); % compute force
    
    % swap order to remove RijFull
    if(nargout > 1)
        nRij = abs(RijFull)<=R;
        nRij = sum(nRij,2); %Finds the number of individuals who are
                            %within R of each individual in a row vector
    end
    
end

end

