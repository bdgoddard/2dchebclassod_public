function Plot_OD_SDE_Q(data,optsPlot)

if(nargin==1)
    optsPlot = struct;
end

t = data.t;
Q = data.Q;

if(isfield(optsPlot,'ha'))
    ha = optsPlot.ha;
else
    figure('Position',[200,200,800,800])
    ha = axes;
end

plot(ha,t,Q,'b');
ylim([0,1]);
%     set(gca,'xscale','log')

if(isfield(optsPlot,'xLim'))
    xlim(ha,optsPlot.xLim);
end
if(isfield(optsPlot,'yLim'))
    ylim(ha,optsPlot.yLim);
end
    
xlabel('Time');
ylabel('Order Parameter');

end