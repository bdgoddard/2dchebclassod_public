function [F,nRij] = F_SDE_Periodic(x,R,L,nParticles)
%F_SDE Calculates F for SDE solver

    RijFull = getRijSigned1DPeriodic(x,L);
    Rij = RijFull;

    Rij(abs(Rij)>R) = 0; %Works out which opinions (i,j) aren't within R
                         %and sets all these Rij to be zero
    sumRij = sum(Rij,2); %For row i, sum over x_i - x_j
    F = (sumRij/nParticles); % compute force

    % swap order to remove RijFull
    if(nargout > 1)
        nRij = abs(RijFull)<=R;
        nRij = sum(nRij,2); %Finds the number of individuals who are
                            %within R of each individual in a row vector
    end

end
