function xIC = ODE_SDE_IC(optsIC)

% initial condition
if(strcmp(optsIC.rho,'uniform'))
    xIC = rand(nParticles,nSamples);
elseif(strcmp(optsIC.rho,'delta'))
    xIC = x0(ones(nParticles,nSamples));
else
    xIC = SampleInitialCondition(optsIC); %nParticles*nSamples
end    


end