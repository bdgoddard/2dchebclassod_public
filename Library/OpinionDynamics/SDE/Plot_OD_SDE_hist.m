function Plot_OD_SDE_hist(data,optsPlot)

if(nargin==1)
    optsPlot = struct;
end

t = data.t;
x = data.x;
opts = data.opts;

nParticles = opts.nParticles;
nRuns = opts.nRuns;

setEdges = false;
tPos = length(t);

if(isfield(optsPlot,'ha'))
    ha = optsPlot.ha;
else
    figure('Position',[200,200,800,800])
    ha = axes;
end

if(isfield(optsPlot,'xLim'))
    xLim = optsPlot.xLim;
    if(isfield(optsPlot,'nBins'))
        nBins = optsPlot.nBins;
    else
        nBins = round(nParticles/10);
    end
    dx = (xLim(2)-xLim(1))/nBins;
    edges = xLim(1):dx:xLim(2);
    setEdges = true;
end

if(isfield(optsPlot,'tPlot'))
    %tPos = find(t==optsPlot.tPlot);
    [~,tPos] = min(abs(t-optsPlot.tPlot));
end

xHist = x(:,tPos,:);
xHist = xHist(:);

if(setEdges)
    [bincounts,edges] = histcounts(xHist,edges);
else
    [bincounts,edges] = histcounts(xHist);
    dx = edges(2)-edges(1);
end

bincounts = bincounts/nParticles/nRuns/dx;
%sum(bincounts*dx);
scatter(edges(1:end-1)+dx/2,bincounts)
xlim(ha,[0,1]);

if(isfield(optsPlot,'xLim'))
    xlim(ha,optsPlot.xLim);
end
if(isfield(optsPlot,'yLim'))
    ylim(ha,optsPlot.yLim);
end
    
xlabel('Opinion');
ylabel('Density');


end