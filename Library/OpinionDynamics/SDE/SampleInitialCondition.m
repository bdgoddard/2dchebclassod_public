function x=SampleInitialCondition(opts,~)

if(isfield(opts,'params'))
    params = opts.params;
else
    params = struct;
end

% and options for sampling
nSamples=opts.nSamples;
nParticles = opts.nParticles;
nSamplesTotal = nSamples*nParticles;
thin=opts.thin;
burnin=opts.burnin;
   
yMin = opts.yMin;
yMax = opts.yMax;
initialGuess = (yMin + yMax)/2;

rho = str2func(opts.rho);

tic
% myslicesample is just slicesample with a progress bar
x = myslicesample(initialGuess,nSamplesTotal,@logpdf,thin,burnin);  
x = reshape(x,nParticles,nSamples);
toc


%--------------------------------------------------------------------------
% logpdf function generated from V
%--------------------------------------------------------------------------
function logp = logpdf(x)
    % only sample in the interval
    if(any(x<yMin) || any(x>yMax))
        logp = -inf;
    else
        logp= log(rho(x,params)); % inside the interval sample rho
    end
    

end % logpdf

end % end samplepdf