function data = doOD_SDE(opts)

    nComps = length(opts);
        
    for iComp = 1:nComps
        data.SDE(iComp) = OD_SDE(opts(iComp));
    end

end