function Plot_OD_SDE_x(data,optsPlot)

if(nargin==1)
    optsPlot = struct;
end

t = data.t;
x = data.x;
opts = data.opts;

nParticles = opts.nParticles;

plotSkip = 1;

if(isfield(optsPlot,'plotSkip'))
    plotSkip = optsPlot.plotSkip;
end
if(isfield(optsPlot,'ha'))
    ha = optsPlot.ha;
else
    figure('Position',[200,200,800,800])
    ha = axes;
end

for i = 1:plotSkip:nParticles
    plot(ha,t,x(i,:),'b.');
    hold on
end
ylim([0,1]);

if(isfield(optsPlot,'xLim'))
    xlim(ha,optsPlot.xLim);
end
if(isfield(optsPlot,'yLim'))
    ylim(ha,optsPlot.yLim);
end
    
xlabel('Time');
ylabel('Opinion');


end