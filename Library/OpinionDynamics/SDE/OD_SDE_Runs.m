function [x,t] = OD_SDE_Runs(opts,xIC)

    nRuns = opts.nRuns;
    if(isfield(opts,'nSaves'))
        nSaves = opts.nSaves;
    else
        nSaves = opts.nSteps;
    end
    nParticles = opts.nParticles;

    nSamples = size(xIC,2);
    ICStep = nSamples/nRuns;
    ICMask = (0:nRuns-1)*ICStep + 1;
    
    xICRuns = xIC(:,ICMask);
        
    x = zeros(nParticles,nSaves+1,nRuns);
    
    for iRun = 1:nRuns
        [xTemp,t] = OD_SDE(opts,xICRuns(:,iRun));
        x(:,:,iRun) = xTemp;
    end
    
end