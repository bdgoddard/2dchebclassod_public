classdef MultiOpinionDynamics < handle
    
    properties (Access = public)                    
        
        OD
        
        optsPhys
        optsNum
        
        optsStoc = []
        stocComps = []
        
        loopVars
        compVars
        
        nOpts
        nVars
        nComps
              
        DataStorageDir = 'OpinionDynamics'
        dirNames
        compNames = []
        outputDir
        pdfDir

        Comps
        
    end
    
    methods (Access = public)          
        
        %------------------------------------------------------------------
        % Constructor
        %------------------------------------------------------------------

        function this = MultiOpinionDynamics(opts)      
                                               
            if(isfield(opts,'loopVars'))
                this.optsPhys = opts.optsPhys;
                this.optsNum  = opts.optsNum;
                if(isfield(opts,'optsStoc'))
                    this.optsStoc  = opts.optsStoc;
                end
                this.loopVars = opts.loopVars;
            else
                this.loopVars = [];    
                this.OD = opts.OD;
                this.nComps = length(this.OD);
                if(isfield(opts,'optsNum'))
                    this.optsNum = opts.optsNum;
                end
            end
                        
            Preprocess(this);
            
            if(isfield(opts,'stocComps'))
                this.stocComps = opts.stocComps;
            elseif(isfield(opts,'loopVarsStoc'))
                optsTemp.loopVarsPlot = opts.loopVarsStoc;
                this.stocComps = GetToPlot(this,optsTemp);
            else
                for iComp = 1:this.nComps
                    if(isfield(this.OD(iComp),'optsStoc'))
                        this.stocComps = [this.stocComps, iComp];
                    end
                end
            end            

        end
        
        %------------------------------------------------------------------
        % Preprocessor
        %------------------------------------------------------------------
        
        function Preprocess(this)
                        
            global dirData
                                    
            if(isfield(this.optsNum,'dataDir'))
                this.DataStorageDir = this.optsNum.dataDir;
                this.optsNum = rmfield(this.optsNum,'dataDir');
            end         
                        
            if(isfield(this.optsNum,'dataSubDir'))
                if(~isempty(this.DataStorageDir))
                    this.DataStorageDir = [this.DataStorageDir filesep this.optsNum.dataSubDir];
                else
                    this.DataStorageDir = [this.optsNum.dataSubDir];
                end
                this.optsNum = rmfield(this.optsNum,'dataSubDir');
            end
            
            this.outputDir = [dirData filesep this.DataStorageDir filesep 'Output'];
            this.pdfDir    = [this.outputDir filesep 'Pdfs' filesep];
            
            this.nVars = length(this.loopVars);
                        
            if(this.nVars>0)
                PreprocessLoopVars(this);
                this.nComps = prod(this.nOpts);
                
                PreprocessCompVars(this);
                PreprocessOpts(this);
            end
            
            PreprocessComputations(this)
            
        end
        
        function PreprocessLoopVars(this)
            
            for iVar = 1:this.nVars
                value = this.loopVars(iVar).value;
                if( ischar(value) )
                    value = {value};
                elseif( isnumeric(value) )
                    value = value(:);
                    value = num2cell(value);
                end
                this.loopVars(iVar).value = value;
                this.nOpts(iVar) = length(value);
            end         

        end
        
        function PreprocessCompVars(this)
            
            vars = ones(this.nVars,1);
            vars(1) = 0;
            
            this.compVars = zeros(this.nVars,this.nComps);
            
            for iComp = 1:this.nComps

                iVar = 1;

                while iVar<=this.nVars
                    if(vars(iVar) == this.nOpts(iVar))
                        vars(iVar) = 1;
                        iVar = iVar+1;
                    else
                        vars(iVar) = vars(iVar)+1;
                        break
                    end
                end

                this.compVars(:,iComp) = vars;
               
            end

        end
        
        function PreprocessOpts(this)

            for iComp = 1:this.nComps
                opts = struct;
                opts.optsPhys = this.optsPhys;
                opts.optsNum  = this.optsNum;
                if(~isempty(this.optsStoc))
                    opts.optsStoc  = this.optsStoc;
                end
                dirName = [];
                
                for iVar = 1:this.nVars
                    field = split(this.loopVars(iVar).field,'.');
                    value = this.loopVars(iVar).value{this.compVars(iVar,iComp)};
                    if(iscell(value))
                        value = value{1};
                    end
                    opts  = setfield(opts,field{:},value);
                    if(isnumeric(value))
                        if(length(value)==1)
                            value = num2str(value);
                        else
                            this.loopVars(iVar).saveName = [];
                        end
                    end
                    saveName = this.loopVars(iVar).saveName;
                    if(~isempty(saveName))
                        if(~isempty(dirName))
                            dirName = [dirName '_']; %#ok
                        end
                        dirName = [dirName saveName '=' value]; %#ok
                    end
                end

                if(iComp==1)
                    this.OD = opts;
                    this.dirNames{1} = dirName;
                else
                    this.OD(iComp) = opts;
                    this.dirNames{iComp} = dirName;
                end
                
                if(~isempty(this.dirNames{iComp}))
                    this.compNames{iComp} = strrep(this.dirNames{iComp},'_',', ');
                    this.compNames{iComp} = [' (' this.compNames{iComp} ')'];
                else
                    this.compNames{iComp} = [];
                end

            end
        
        end
                        
        function PreprocessComputations(this)
            
            if(length(this.compNames)<this.nComps)
                for iComp = 1:this.nComps
                    this.compNames{iComp} = [];
                end
            end
            
            for iComp = 1:this.nComps
                
                cprintf('*blue',['Preprocessing computation ' num2str(iComp) '/' num2str(this.nComps) ...
                                 this.compNames{iComp} '\n']);
                
                opts = this.OD(iComp);

                opts.optsNum.dataDir = this.DataStorageDir;
                if(~isempty(this.dirNames))
                    opts.optsNum.dynDir = this.dirNames{iComp};
                    opts.optsNum.configDir = this.DataStorageDir;
                    opts.optsNum.configSubDir = this.dirNames{iComp};
                end
                
                if(iComp == 1)
                    this.Comps = OpinionDynamics(opts);
                else
                    this.Comps(iComp) = OpinionDynamics(opts);
                end
                fprintf(1,'\n');
            end             
            
        end
        
        %------------------------------------------------------------------
        % Computations
        %------------------------------------------------------------------
        
        function ComputeDynamics(this)
            for iComp = 1:this.nComps
                cprintf('*blue',['Computing dynamics ' num2str(iComp) '/' num2str(this.nComps) ...
                                 this.compNames{iComp} '\n']);
                this.Comps(iComp).ComputeDynamics();
                fprintf(1,'\n');
            end 
        end
        
        function ComputeOrderParameter(this)
            
            if(isempty(this.Comps(1).dynamicsResult))
                this.ComputeDynamics;
            end
            
            for iComp = 1:this.nComps
                cprintf('*blue',['Computing order parameter ' num2str(iComp) '/' num2str(this.nComps) ...
                                 this.compNames{iComp} '\n']);
                this.Comps(iComp).ComputeOrderParameter();
                fprintf(1,'\n');
            end 
        end            

        function ComputeMass(this)
            
            if(isempty(this.Comps(1).dynamicsResult))
                this.ComputeDynamics;
            end

            for iComp = 1:this.nComps
                cprintf('*blue',['Computing mass ' num2str(iComp) '/' num2str(this.nComps) ...
                        this.compNames{iComp} '\n']);
                this.Comps(iComp).ComputeMass();
                fprintf(1,'\n');
            end 
        end            

        function ComputeMeanOpinion(this)
            
            if(isempty(this.Comps(1).dynamicsResult))
                this.ComputeDynamics;
            end
            
            for iComp = 1:this.nComps
                cprintf('*blue',['Computing mean opinion ' num2str(iComp) '/' num2str(this.nComps) ...
                                 this.compNames{iComp} '\n']);
                this.Comps(iComp).ComputeMeanOpinion();
                fprintf(1,'\n');
            end 
        end            

        function ComputeModeOpinion(this)
            
            if(isempty(this.Comps(1).dynamicsResult))
                this.ComputeDynamics;
            end
            
            for iComp = 1:this.nComps
                cprintf('*blue',['Computing mode opinion ' num2str(iComp) '/' num2str(this.nComps) ...
                                 this.compNames{iComp} '\n']);
                this.Comps(iComp).ComputeModeOpinion();
                fprintf(1,'\n');
            end 
        end            
     
        
        function ComputeStocDynamics(this)
                                    
            nStocComps = length(this.stocComps);
            compCount = 0;
            
            for iComp = this.stocComps
                compCount = compCount + 1;
                cprintf('*blue',['Computing stochastic dynamics ' num2str(compCount) '/' num2str(nStocComps) ...
                                 this.compNames{iComp} '\n']);
                this.Comps(iComp).ComputeStocDynamics();
                fprintf(1,'\n');
            end 
        end
        
        function ComputeStocOrderParameter(this)
            
            if(isempty(this.Comps(1).dynamicsResult))
                this.ComputeStocDynamics;
            end
            
            nStocComps = length(this.stocComps);
            compCount = 0;
            
            for iComp = this.stocComps
                compCount = compCount + 1;
                cprintf('*blue',['Computing stochastic order parameter ' num2str(compCount) '/' num2str(nStocComps) ...
                                 this.compNames{iComp} '\n']);
                this.Comps(iComp).ComputeStocOrderParameter();
                fprintf(1,'\n');
            end 
        end            
        
        
        %------------------------------------------------------------------
        % Plotting
        %------------------------------------------------------------------

        function PlotDensityRadicalsFinal(this,opts)
            
            if(nargin<2)
                opts = struct;
            end
            
            opts = MakeGrid(this,opts);
            
            opts.latex = true;
                        
            optsComp = opts;
            for iComp = 1:this.nComps
                optsComp.ha = opts.ha(opts.rows(iComp),opts.cols(iComp));
                this.Comps(iComp).PlotDensityRadicalsFinal(optsComp);
            end
                       
            FixTightSubplots(opts.ha,opts.hacb,opts);
        end

        
        function hStruct = PlotMovie(this,varName,opts)
            
            if(~this.CheckPlotTimes)
                error('PlotTimes must match for each computation');
            else
                plotTimes = this.Comps(1).dynamicsResult.t;
            end
            
            if(nargin < 3)
                opts = struct;
            end

            if(~iscell(varName))
                varName = {varName};
            end

            opts.name = 'Movie';
            
            varStruct = struct;
            
            for iComp = 1:this.nComps
                if(iComp==1)
                    [varStruct.var,opts] = this.Comps(iComp).ExtractVars(varName,opts);
                else
                    [varStruct(iComp).var,opts] = this.Comps(iComp).ExtractVars(varName,opts);
                end
            end
                        
            opts = GetVarBounds(this,varName,opts);
            
            doPdfs = (isfield(opts,'save') && opts.save);
                                    
            nT = length(plotTimes);

            if(doPdfs)
                [nd,pdfFileNames] = setupPdfMovie(plotTimes,this.pdfDir);
            end
                        
            opts.varName = varName;
            opts.nVars = length(varName);
            opts.xLabel = 'Opinion';     
            
            opts.Position = [100 100 1200 800];
            
            [hStruct,opts] = SetUpPlots(this,opts);
            
            opts.external = true;
            
            hf = hStruct.hf;
            opts.ha = hStruct.ha;
            timeDependent = opts.timeDependent;
                        
            optsT = opts;
            props = {'linestyle','linecolor','linewidth','legend'};
            nProps = length(props);
                        
            for iT = 1:nT
                hold off

                if(isfield(opts,'title'))
                    optsT.title = opts.title;
                else
                    optsT.title = ['t = ' num2str(plotTimes(iT))];
                end

                for iComp = 1:this.nComps
                
                    for iVar = 1:opts.nVars
                        if(timeDependent(iVar))
                            varT = varStruct(iComp).var{iVar}(:,iT);
                        else
                            varT = varStruct(iComp).var{iVar};
                        end

                        for iProp = 1:nProps
                            prop = props{iProp};
                            optsT.(prop) = opts.(prop){iVar};
                        end

                        this.Comps(iComp).PlotDensity(varT,optsT);
                        hold on
                    end

                end
                
                ylim([min(minVal),max(maxVal)]);
                xlabel(opts.xLabel);    
                
                if(doPdfs)
                    pdfFileNames = savePdfFrame(this.pdfDir,pdfFileNames,iT,nd,hf);
                end
                                
                if(~doPdfs)
                    pause(0.05);
                end

                drawnow
                
            end
            
            if(doPdfs)
               makePdfMovie(this.pdfDir,pdfFileNames);
            end
            
        end

        function PlotSnapshots(this,varName,opts)
            
            if(nargin<3)
                opts = struct;
            end
            
            opts = MakeGrid(this,opts);
            
            optsComp = opts;
            for iComp = 1:this.nComps
                optsComp.ha = opts.ha(opts.rows(iComp),opts.cols(iComp));
                this.Comps(iComp).PlotSnapshots(varName,optsComp);
            end
            
            FixTightSubplots(opts.ha,opts.hacb,opts);
                                    
        end
        
        function PlotSnapshotsDensityRadicals(this,opts)
            
            if(nargin<2)
                opts = struct;
            end
            
            opts = MakeGrid(this,opts);
                        
            optsComp = opts;
            for iComp = 1:this.nComps
                optsComp.ha = opts.ha(opts.rows(iComp),opts.cols(iComp));
                this.Comps(iComp).PlotSnapshotsDensityRadicals(optsComp);
            end
            
            FixTightSubplots(opts.ha,opts.hacb,opts);
            
        end
        
        function PlotDynamicValue(this,varName,opts)

            if(nargin<2)
                opts = struct;
            end
            
            opts = MakeGrid(this,opts);
            
            optsComp = opts;
            for iComp = 1:this.nComps
                optsComp.ha = opts.ha(opts.rows(iComp),opts.cols(iComp));
                this.Comps(iComp).PlotDynamicValue(varName,optsComp);
            end
            
            FixTightSubplots(opts.ha,opts.hacb,opts);
            
        end
        
        function PlotOrderParameter(this,opts)
                        
            if(~isfield(this.Comps(1).dynamicsResult,'Q'))
                this.ComputeOrderParameter;
            end
            
            if(nargin<2 || ~isfield(opts,'yLabel'))
                opts.yLabel = 'Order Parameter';
            end
            
            if(nargin<2 || ~isfield(opts,'QLim'))
                opts.yLim = [0;1];
            end
            
            PlotDynamicValue(this,'Q',opts);            
        end
        
        function PlotMass(this,opts)
            if(~isfield(this.Comps(1).dynamicsResult,'mass'))
                this.ComputeMass;
            end
            
            if(nargin<2 || ~isfield(opts,'yLabel'))
                opts.yLabel = 'Mass';
            end
            PlotDynamicValue(this,'mass',opts);            
        end
        
        
        function PlotTimeSpace(this,varName,opts)
                        
            if(nargin<3)
                opts = struct;
            end
            
            opts = MakeGrid(this,opts);
            
            optsComp = opts;
            for iComp = 1:this.nComps
                optsComp.ha = opts.ha(opts.rows(iComp),opts.cols(iComp));
                this.Comps(iComp).PlotTimeSpace(varName,optsComp);
            end
            
            FixTightSubplots(opts.ha,opts.hacb,opts);
            
            if(this.nVars == 2 && isfield(opts,'xy') && opts.xy)
                LabelGrid(this,opts);                
            end
                        
        end
                
        function PlotGridFinal(this,varName,opts)
            
            if(nargin<3)
                opts = struct;
            end

            if(isfield(opts,'loopVarsPlot'))
                [plotIDs,annotations] = GetToPlot(this,opts);
                nLabels = length(plotIDs);
            else
                plotIDs = NaN;
                annotations = [];
                nLabels = 0;
            end
            
            gridText = strings(nLabels,1);
            gridTextX = zeros(nLabels,1);
            gridTextY = zeros(nLabels,1);
            
            opts = GetVarIDs(this,opts);

            varXID = opts.varXID;
            varYID = opts.varYID;
            
            if(~this.CheckPlotTimesFinal)
                error('Final PlotTimes must match for each computation');
            end

            doSDE = ~isempty(this.optsStoc);
            if(doSDE)
                gridVarStoc = zeros(this.nOpts(varXID),this.nOpts(varYID));
            end            
            
            gridVar = zeros(this.nOpts(varXID),this.nOpts(varYID));
            
            plotPos = 1;
            
            for iComp = 1:this.nComps
                compVarX = this.compVars(varXID,iComp);
                compVarY = this.compVars(varYID,iComp);
                tempVar = this.Comps(iComp).dynamicsResult.(varName);
                gridVar(compVarX,compVarY) = tempVar(end);
                if(doSDE)
                    if(ismember(iComp,this.stocComps) && isfield(this.Comps(iComp).stocResult.dynamics,varName))
                        tempVarStoc = this.Comps(iComp).stocResult.dynamics.(varName);
                        gridVarStoc(compVarX,compVarY) = tempVarStoc(end);
                    else
                        gridVarStoc(compVarX,compVarY) = NaN;
                    end
                end
                if(ismember(iComp,plotIDs))
                    gridText(plotPos) = annotations(iComp);
                    gridTextX(plotPos) = compVarX;
                    gridTextY(plotPos) = compVarY;
                    plotPos = plotPos + 1;
                end
            end
            
            opts.nVars = 1;
            optsOld = opts;

            if(isfield(opts,'log') && opts.log)
                gridVar = log10(gridVar);
                if(doSDE)
                    gridVarStoc = log10(gridVarStoc);
                end
            end
            
            [hStruct(1),opts] = SetUpPlots(this,optsOld);
            
            % deal with NaNs - plot them in the background colour
            imAlpha=ones(size(gridVar));
            imAlpha(isnan(gridVar)) = 0;
            imagesc([1,this.nOpts(varXID)],[1,this.nOpts(varYID)],gridVar.','AlphaData',imAlpha.')
            %set(gca,'color',0*[1 1 1]);
            
            if(isfield(opts,'addSnapshotLabels') && opts.addSnapshotLabels)
                %text(gridTextX, gridTextY, gridText, 'HorizontalAlignment', 'Center','FontSize',16)
                if(isfield(opts,'fontsize'))
                    fontsize = 0.75*opts.fontsize;
                else
                    fontsize = 16;
                end

                text(gridTextX, gridTextY, gridText, ...
                     'HorizontalAlignment', 'Center','FontSize',fontsize, ...
                     'Color','r')
            end
            
            
            if(doSDE)
                [hStruct(2),opts] = SetUpPlots(this,opts);
                imagesc([1,this.nOpts(varXID)],[1,this.nOpts(varYID)],gridVarStoc.')
            end
                        
            for iPlot = 1:length(hStruct)
                ha = hStruct(iPlot).ha;
                                
                set(ha,'YDir','normal')
                
                if(isfield(opts,'cRange'))
                    if(opts.cRange(2)>opts.cRange(1))
                        caxis(ha,opts.cRange);
                    end
                end
                
                if(isfield(opts,'cMap'))
                    colormap(ha,opts.cMap);
                end
                
                hc = colorbar(ha);
                
                if(isfield(opts,'cLim'))
                    if(opts.cLim(2)>opts.cLim(1))
                        hc.Limits = opts.cLim;
                    end
                end
                
                opts.ha = ha;
                opts = GetVarLabels(this,opts);
                                
                FixPlots(this,hStruct(iPlot),opts);
                axis(gca,'square');
            end
            
        end
        
        function PlotOrderParameterFinal(this,optsPlot)
            
            if(nargin<2)
                optsPlot = struct;
            end
            
            if(~isfield(this.Comps(1).dynamicsResult,'Q'))
                ComputeOrderParameter(this);
            end
            
            doSDE = ~isempty(this.optsStoc);
            if(doSDE && ~ isfield(this.Comps(1).stocResult.dynamics,'Q'))
                ComputeStocOrderParameter(this);
            end
            
            optsPlot = this.GetOrderParameterColours(optsPlot);
            
            this.PlotGridFinal('Q',optsPlot);
              
        end
        
        function PlotEqTime(this,optsPlot)
            
            if(nargin<2)
                optsPlot = struct;
            end
            
            if(~isfield(this.Comps(1).dynamicsResult,'tEq'))
                error('tEq is not set - enable optsNum.toEq');
            end
                        
            optsPlot = GetEqTimeColours(this,optsPlot);
            
            this.PlotGridFinal('tEq',optsPlot);
                          
        end
        
        function PlotMeanOpinionFinal(this,optsPlot)
            
            if(nargin<2)
                optsPlot = struct;
            end
            
            if(~isfield(this.Comps(1).dynamicsResult,'meanOpinion'))
                ComputeMeanOpinion(this);
            end
            
            optsPlot = GetMeanOpinionColours(this,optsPlot);
            
            this.PlotGridFinal('meanOpinion',optsPlot);
                          
        end        

        function PlotModeOpinionFinal(this,optsPlot)
            
            if(nargin<2)
                optsPlot = struct;
            end
            
            if(~isfield(this.Comps(1).dynamicsResult,'modeOpinion'))
                ComputeMeanOpinion(this);
            end
                        
            optsPlot = GetModeOpinionColours(this,optsPlot);
            
            this.PlotGridFinal('modeOpinion',optsPlot);
                          
        end       
        
        function PlotDensityOrderParameterFinal(this,optsPlot)
            
            if(nargin<2)
                optsPlot = struct;
            end

            optsPlot = this.GetOrderParameterColours(optsPlot);

            if(~isfield(optsPlot,'yLim'))
                [minRho,maxRho] = this.GetVarBounds('rho_t',optsPlot);
                optsPlot.yLim = [minRho,maxRho];
            end
            
            optsPlot.setColor = 1;
            if(~isfield(optsPlot,'linecolor'))
                optsPlot.linecolor = {[0.5;0.5;0.5],'r'};
            end
            if(~isfield(optsPlot,'linestyle'))
                optsPlot.linestyle = {'-','-'};
            end
                        
            PlotSubGrid(this,'PlotDensityRadicalsFinal','Q',optsPlot)
            
        end

        
        %------------------------------------------------------------------
        % Determine which plots to do out of loopvars
        %------------------------------------------------------------------

        function opts = GetVarIDs(this,opts)
            
            if(~isfield(opts,'varX'))
                if(this.nVars~=2)
                    error('Plotting only available for two variables');
                else
                    varXID = 1;
                    varYID = 2;
                end
            else
                varX = opts.varX;
                varY = opts.varY;
            
                setX = false;
                setY = false;
                for iVar = 1:this.nVars
                    if(strcmp(this.loopVars(iVar).saveName,varX))
                        varXID = iVar;
                        setX = true;
                    end
                    if(strcmp(this.loopVars(iVar).saveName,varY))
                        varYID = iVar;
                        setY = true;
                    end
                end

                if(~setX)
                    error(['Unknown X variable: ' varX]);
                elseif(~setY)
                    error(['Unknown Y variable: ' varY]);
                end
                
            end
            
            opts.varXID = varXID;
            opts.varYID = varYID;
            
        end
        
        %------------------------------------------------------------------
        % Get labels for loopvar plotting
        %------------------------------------------------------------------

        function opts = GetVarLabels(this,opts)
            
                ha = opts.ha;
                
                varXID = opts.varXID;
                varYID = opts.varYID;
                
                nTicksX = this.nOpts(varXID);
                nTicksY = this.nOpts(varYID);
                
                %maxTicks = 8;
                maxTicks = 4;
                
                skipX = ceil(nTicksX/maxTicks);
                maskX = 1:skipX:nTicksX;
                xTickLabels = this.loopVars(varXID).value;
                xticks(ha,maskX);
                xticklabels(ha,xTickLabels(maskX));

                skipY = ceil(nTicksY/maxTicks);
                maskY = 1:skipY:nTicksY;
                yTickLabels = this.loopVars(varYID).value;
                yticks(ha,maskY);
                yticklabels(ha,yTickLabels(maskY));
                                
                if(isfield(opts,'varLabels'))
                    varLabels = opts.varLabels;
                else
                    varLabels = [];
                end
                if(~isfield(opts,'varXLabel'))
                    if(~isempty(varLabels))
                        field = split(this.loopVars(varXID).field,'.');
                        opts.xLabel = getfield(varLabels,field{:});
                    else
                        opts.xLabel = this.loopVars(varXID).saveName;
                    end
                else
                    opts.xLabel = opts.varXLabel;
                end
                if(~isfield(opts,'varYLabel'))
                    if(~isempty(varLabels))
                        field = split(this.loopVars(varYID).field,'.');
                        opts.yLabel = getfield(varLabels,field{:});
                    else
                        opts.yLabel = this.loopVars(varYID).saveName;
                    end
                else
                    opts.yLabel = opts.varYLabel;
                end
        end
                
        %------------------------------------------------------------------
        % Plot in a subgrid
        %------------------------------------------------------------------

        function PlotSubGrid(this,fns,cVar,opts)
                                    
            if(nargin<4)
                opts = struct;
            end
            
            if(isfield(opts,'loopVarsPlot'))
                [plotIDs,annotations] = GetToPlot(this,opts);
                nLabels = length(plotIDs);
            else
                plotIDs = NaN;
                annotations = [];
                nLabels = 0;
            end
            
            % stop plotting a subset of computations
            if(isfield(opts,'loopVarsPlot'))
                opts = rmfield(opts,'loopVarsPlot');
            end
            if(isfield(opts,'snapshotTimes'))
                opts = rmfield(opts,'snapshotTimes');
            end

            
            opts = GetVarIDs(this,opts);
            varXID = opts.varXID;
            varYID = opts.varYID;
            
            nVarX = this.nOpts(varXID);
            nVarY = this.nOpts(varYID);
            
            if(isfield(opts,'ha') && isfield(opts,'haOut'))
                ha = opts.ha;
                haOut = opts.haOut;
            else
                opts.nRows = 1;
                opts.nCols = 1;
                opts.nSubRows = nVarY;
                opts.nSubCols = nVarX;
                [ha,haOut,hacb,~] = AxesInAxes(opts);
            end
                                    
            gridText = strings(nLabels,1);
            gridTextX = zeros(nLabels,1);
            gridTextY = zeros(nLabels,1);            
            plotPos = 1;
            
            uistack(haOut,'bottom')
            if(isempty(cVar))
                cVals = ones(nVarX,nVarY);
            else
                cVals = zeros(this.nOpts(varXID),this.nOpts(varYID));
                for iComp = 1:this.nComps
                    compVarX = this.compVars(varXID,iComp);
                    compVarY = this.compVars(varYID,iComp);
                    tempVal = this.Comps(iComp).dynamicsResult.(cVar);
                    cVals(compVarX,compVarY) = tempVal(end);

                    if(ismember(iComp,plotIDs))
                        gridText(plotPos) = annotations(iComp);
                        gridTextX(plotPos) = compVarX;
                        gridTextY(plotPos) = compVarY;
                        plotPos = plotPos + 1;
                    end
                    
                end

            end
            
            imagesc(haOut,[1,this.nOpts(varXID)],[1,this.nOpts(varYID)],cVals.')
            set(haOut,'YDir','normal')
            
            if(isfield(opts,'addSnapshotLabels') && opts.addSnapshotLabels)
                if(isfield(opts,'fontsize'))
                    fontsize = 0.75*opts.fontsize;
                else
                    fontsize = 16;
                end
                
                text(haOut,gridTextX, gridTextY, gridText, ...
                     'HorizontalAlignment', 'Center','FontSize',fontsize, ...
                     'Color','r')
            end            
                        
            if(~isempty(hacb))

                if(isfield(opts,'cRange'))
                    caxis(haOut,opts.cRange);
                    caxis(hacb,opts.cRange);
                end
                
                if(isfield(opts,'cMap'))
                    colormap(haOut,opts.cMap);
                    colormap(hacb,opts.cMap);
                end
                
                hc = colorbar(haOut);
                hccb = colorbar(hacb);
                
                if(isfield(opts,'cLim'))
                    hc.Limits = opts.cLim;
                    hccb.Limits = opts.cLim;
                end
                
                colorbar(haOut,'off');

            end
            
            opts.ha = haOut;
            hStruct.ha = haOut;
            
            opts = GetVarLabels(this,opts);
                        
            FixPlots(this,hStruct,opts);            
            
            opts.plain = true; % suppress titles, axes labels, legend
            
            if(~iscell(fns))
                fns = {fns};
            end
            nfns = length(fns);
            for iComp = 1:this.nComps
                compVarX = this.compVars(varXID,iComp);
                compVarY = this.compVars(varYID,iComp);
                opts.ha = ha(compVarY,compVarX);
                for ifn = 1:nfns
                    fn = fns{ifn};
                    this.Comps(iComp).(fn)(opts);
                end    
            end
            
            set(ha,'xlabel',[]);
            set(ha,'ylabel',[]);
            set(ha,'xticklabels',[]);
            set(ha,'yticklabels',[]);
            set(ha,'color','none');            
            
        end
                
        %------------------------------------------------------------------
        % Get bounds on a variable over all or some of the computations
        %------------------------------------------------------------------
                
        function opts = GetVarBounds(this,varName,opts)
                        
            if(nargin<3)
                opts = struct;
            end
            
            if(~iscell(varName))
                varName = {varName};
            end
            
            minVal = nan(length(varName),1);
            maxVal = nan(length(varName),1);
            
            if(isfield(opts,'useFull') && opts.useFull)
                useFull = true;
            else
                useFull = false;
            end
                        
            if(isfield(opts,'plotIDs'))
                plotIDs = opts.plotIDs;
            elseif(isfield(opts,'loopVarsPlot') && ~useFull)
                plotIDs = GetToPlot(this,opts);
            else
                plotIDs = 1:this.nComps;
            end
                                                 
            for iComp = plotIDs
                ODi = this.Comps(iComp);
                [plotTimes,nT] = ODi.GetPlotTimes;
                if(isfield(opts,'final') && opts.final)
                    tPos = nT;
                elseif(isfield(opts,'snapshotTimes') && ~useFull)
                    tPos = ODi.GetTPos(opts.snapshotTimes,plotTimes);
                else
                    tPos = 1:nT;
                end
                                
                for iVar = 1:length(varName)
                    varVal = ODi.dynamicsResult.(varName{iVar});
                    if(size(varVal,2) == nT)
                        varVal = varVal(:,tPos);
                    end 
                    maxVal(iVar) = max(maxVal(iVar), max(varVal(:)));
                    minVal(iVar) = min(minVal(iVar), min(varVal(:)));
                    
                    if(isfield(ODi.stocResult,'dynamics') ...
                            && isfield(ODi.stocResult.dynamics,varName{iVar}))
                        varVal = ODi.stocResult.dynamics.(varName{iVar});
                        if(size(varVal,2) == nT)
                            varVal = varVal(:,tPos);
                        end 
                        maxVal(iVar) = max(maxVal(iVar), max(varVal(:)));
                        minVal(iVar) = min(minVal(iVar), min(varVal(:)));
                    end
                    
                end
            end
            
            if(~isfield(opts,'tight') || ~opts.tight)
                minVal = 0.9*minVal;
                maxVal = 1.1*maxVal;
            end
            
            opts.yLim = [minVal,maxVal];
            
        end
        
        %------------------------------------------------------------------
        % Get colours
        %------------------------------------------------------------------
        
        function optsPlot = GetOrderParameterColours(this,optsPlot)
                        
            if(~isfield(optsPlot,'QFinalLim'))
                optsTemp = optsPlot;
                optsTemp.tight = true;
                optsTemp = this.GetVarBounds('Q',optsTemp);
                yLim = optsTemp.yLim;
                yMin = 0;
                yMax = max(1,yLim(2));
                optsPlot.cLim = [yMin,yMax];
            else
                optsPlot.cLim = optsPlot.QFinalLim;
            end
            if(~isfield(optsPlot,'QRange'))
                optsPlot.cRange = [0,2];
            else
                optsPlot.cRange = optsPlot.QRange;
            end
            if(~isfield(optsPlot,'QMap'))
                optsPlot.cMap = [parula(256);flip(copper(255))];
            else
                optsPlot.cMap = optsPlot.QMap;
            end                        
            
        end

        function optsPlot = GetEqTimeColours(this,optsPlot)
            
            
            if(~isfield(optsPlot,'tEqLim'))

                optsTemp = optsPlot;
                optsTemp.tight = true;
                optsTemp = this.GetVarBounds('tEq',optsTemp);
                optsPlot.cLim = optsTemp.yLim;

            else
                optsPlot.cLim = optsPlot.tEqLim;
            end
                        
            if(~isfield(optsPlot,'cRange'))
                optsPlot.cRange = optsPlot.cLim;
            end
                        
            if(~isfield(optsPlot,'tEQMap'))
                optsPlot.cMap = parula(256);
            else
                optsPlot.cMap = optsPlot.tEqMap;
            end                        
            
        end

        function optsPlot = GetMeanOpinionColours(this,optsPlot)

            if(~isfield(this.Comps(1).dynamicsResult,'meanOpinion'))
                ComputeMeanOpinion(this);
            end
                        
            if(~isfield(optsPlot,'meanLim'))

                optsTemp = optsPlot;
                optsTemp.tight = true;
                optsTemp.final = true;
                optsTemp = this.GetVarBounds('meanOpinion',optsTemp);
                optsPlot.cLim = optsTemp.yLim;

            else
                optsPlot.cLim = optsPlot.meanLim;
            end
                        
            if(isfield(optsPlot,'logEq') && optsPlot.logEq && (optsPlot.cLim(1)==0))
                optsPlot.cLim(1) = this.Comps(1).dynamicsResult.plotTimes(2);
            end
            
            if(~isfield(optsPlot,'meanRange'))
                optsPlot.cRange = [0,1];
            else
                optsPlot.cRange = optsPlot.meanRange;
            end
            
            if(~isfield(optsPlot,'meanMap'))
                optsPlot.cMap = parula(256);
            else
                optsPlot.cMap = optsPlot.meanMap;
            end                        
            
        end        

        function optsPlot = GetModeOpinionColours(this,optsPlot)

            if(~isfield(this.Comps(1).dynamicsResult,'modeOpinion'))
                ComputeModeOpinion(this);
            end
            
            if(~isfield(optsPlot,'modeLim'))

                optsTemp = optsPlot;
                optsTemp.tight = true;
                optsTemp.final = true;
                optsTemp = this.GetVarBounds('modeOpinion',optsTemp);
                optsPlot.cLim = optsTemp.yLim;

            else
                optsPlot.cLim = optsPlot.modeLim;
            end         
            
            if(isfield(optsPlot,'logEq') && optsPlot.logEq && (optsPlot.cLim(1)==0))
                optsPlot.cLim(1) = this.Comps(1).dynamicsResult.plotTimes(2);
            end
            
            if(~isfield(optsPlot,'modeRange'))
                optsPlot.modeRange = [0,1];
            else
                optsPlot.cRange = optsPlot.modeRange;
            end
            
            if(~isfield(optsPlot,'modeMap'))
                optsPlot.cMap = parula(256);
            else
                optsPlot.cMap = optsPlot.modeMap;
            end                        
            
        end    
        
        %------------------------------------------------------------------
        % Determine which computations to do plotting for
        %------------------------------------------------------------------        
        
        function [plotIDs,annotations] = GetToPlot(this,opts)
            if(isfield(opts,'loopVarsPlot'))
                loopVarsPlot = opts.loopVarsPlot;
                nFields = length(loopVarsPlot);
                nToPlot = length(loopVarsPlot(1).value);
                plotIDs = zeros(1,nToPlot);

                tempStruct = struct;
                for iComp = 1:this.nComps
                    for iField = 1:nFields
                        field = split(loopVarsPlot(iField).field,'.');
                        value = getfield(this.Comps(iComp),field{:});
                        if(isnumeric(value))
                            value = round(value,10); % fudge eps inaccuracies
                        end
                        tempStruct = setfield( tempStruct, field{:}, value );
                    end
                    compStruct(iComp) = tempStruct; %#ok
                end

                for iToPlot = 1:nToPlot
                    for iField = 1:nFields
                        field = split(loopVarsPlot(iField).field,'.');
                        value = loopVarsPlot(iField).value(iToPlot);
                        if(isnumeric(value))
                            value = round(value,10); % fudge eps inaccuracies
                        end
                        tempStruct = setfield( tempStruct, field{:}, value );
                    end
                    toPlotStruct(iToPlot) = tempStruct; %#ok
                end       

                alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                annotations = strings(this.nComps,1);
                for iToPlot = 1:nToPlot
                    for iComp = 1:this.nComps
                        if(isequal(toPlotStruct(iToPlot),compStruct(iComp)))
                            plotIDs(iToPlot)=iComp;
                            annotations(iComp) = alphabet(iToPlot);
                            break;
                        end
                    end
                    if(plotIDs(iToPlot)==0)
                        msg = 'Values [';
                        for iField = 1:nFields
                            if(iField>1)
                                msg = [msg ', ']; %#ok
                            end
                            msg = [msg num2str(loopVarsPlot(iField).value(iToPlot))]; %#ok
                        end
                        msg = [msg '] do not exist']; %#ok
                        error(msg);
                    end
                end
            else
                plotIDs = 1:this.nComps;
                annotations = cell(this.nComps,1);
            end
        end        
        
        %------------------------------------------------------------------
        % Check plot times are consistent over computations
        %------------------------------------------------------------------

        function match = CheckPlotTimes(this)
            plotTimeTest = this.Comps(1).dynamicsResult.t;
            match = true;
            for iComp = 2:this.nComps
                if(~isequal(plotTimeTest,this.Comps(iComp).dynamicsResult.t))
                    match = false;
                    break
                end
            end                
        end

        function match = CheckPlotTimesFinal(this)
            plotTimeTest = this.Comps(1).dynamicsResult.t(end);
            match = true;
            for iComp = 2:this.nComps
                if(~isequal(plotTimeTest,this.Comps(iComp).dynamicsResult.t(end)))
                    match = false;
                    break
                end
            end                
        end
                
        %------------------------------------------------------------------
        % Make nice plots and neaten them up
        %------------------------------------------------------------------
                         
        function [hStruct,opts] = SetUpPlots(~,opts)
            
            props = {'linestyle','linecolor','linewidth'};
            defaults.linestyle = {'-'};
            defaults.linecolor = {'r'};
            defaults.linewidth = {1.5};
            
            if(~isfield(opts,'setColor'))
                if(isfield(opts,'linecolor'))
                    opts.setColor = true;
                else
                    opts.setColor = false;
                end
            end
            
            if(isfield(opts,'ha'))
                ha = opts.ha;
                axes(ha);
                hStruct.hf = gcf;
                hStruct.ha = ha;
            else
                if(isfield(opts,'Position'))
                    position = opts.Position;
                else
                    position = [100 100 800 800];
                end
                hStruct.hf = figure('Color','white','Position',position);
                hStruct.ha = axes;
            end

            legTemp = get(gca,'legend');
            if(~isempty(legTemp))
                opts.legString = legTemp.String;
            else
                opts.legString = {};
            end
                        
            nProps = length(props);
            for iProp = 1:nProps
                prop = props{iProp};
                if(isfield(opts,prop))
                    if(~iscell(opts.(prop)))
                        opts.(prop) = {opts.(prop)};
                    end
                    if(length(opts.(prop)) == 1)
                        opts.(prop) = repmat(opts.(prop),1,opts.nVars);
                    end
                else
                    temp(1:opts.nVars) = defaults.(prop);
                    opts.(prop) = temp;
                end
            end            

        end
        
        function FixPlots(~,hStruct,opts)

            ha = hStruct.ha;
            
            nRows = size(ha,1);
            nCols = size(ha,2);
            
            if(isfield(opts,'fontsize'))
                fontsize = opts.fontsize;
            else
                fontsize = 20;
            end
            if(isfield(opts,'linewidth'))
                if(iscell(opts.linewidth))
                    linewidth = opts.linewidth{1};
                else
                    linewidth = opts.linewidth;
                end
            else
                linewidth = 1.5;
            end
                        
            for iRow = 1:nRows
                for iCol = 1:nCols
                    hai = ha(iRow,iCol);
                    xlabel(hai,opts.xLabel,'interpreter','latex');
                    ylabel(hai,opts.yLabel,'interpreter','latex');
                    set(hai,'fontsize',fontsize);
                    set(hai,'linewidth',linewidth);
                end
            end
            drawnow;

        end        
        
        function opts = MakeGrid(this,opts)
            
            if(nargin<2)
                opts = struct;
            end
                        
            if(~isfield(opts,'ha'))
                
                if(this.nVars == 1)
                    nRows = this.nComps;
                    nCols = 1;
                elseif(this.nVars == 2)
                    nCols = this.nOpts(1);
                    nRows = this.nOpts(2);
                else
                    nCols = ceil(sqrt(this.nComps));
                    nRows = ceil(this.nComps/nCols);
                end

                if(~isfield(opts,'nRows'))
                    opts.nRows = nRows;
                end
                if(~isfield(opts,'nCols'))
                    opts.nCols = nCols;
                end

                [opts.ha,opts.hacb] = TightSubplot(opts);

                ids = 1:this.nComps;
                opts.rows = floor((ids+1)/nCols);
                opts.cols = ids - (opts.rows-1)*nCols;
                
            else
            
                ids = 1:this.nComps;
                opts.rows = ids;
                opts.cols = ones(size(ids));
                opts.hacb = [];
                
            end 
        end
        
        function LabelGrid(this,opts)
            ha = opts.ha;
            for iCol = 1:opts.nCols
                %subplot(opts.nRows,opts.nCols,this.nComps-opts.nCols + iCol);
                titleString = this.AddValue2String([this.loopVars(1).saveName ' = ' ],this.loopVars(1).values{iCol});
                xlabel(ha(opts.nRows,iCol),titleString);
            end

            for iRow = 1:opts.nRows
                %subplot(opts.nRows,opts.nCols,opts.nCols*(iRow-1) + 1);
                titleString = this.AddValue2String([this.loopVars(2).saveName ' = ' ],this.loopVars(2).values{iRow});
                ylabel(ha(iRow,1),titleString);
            end
        end
       
        %------------------------------------------------------------------
        % String auxiliary function
        %------------------------------------------------------------------

        function string = AddValue2String(~,string,value)
            if(isnumeric(value))
                string = [string num2str(value)];
            else
                string = [string value];
            end
        end
        
        
    end

end