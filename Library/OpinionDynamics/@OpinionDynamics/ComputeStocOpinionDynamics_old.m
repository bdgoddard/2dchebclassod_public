function ComputeStocOpinionDynamics_old(this) 

    %----------------------------------------------------------------------
    % Initialisation
    %----------------------------------------------------------------------
    
      
    opts.nParticles = this.optsStoc.nParticles;
    opts.nSteps     = this.optsStoc.nSteps;
    opts.nRuns      = this.optsStoc.nRuns;
    
    opts.plotTimes  = this.optsNum.plotTimes;
    opts.sigma      = this.optsPhys.sigma;

    opts.optsInitial = this.optsPhys.optsInitial;
    
    % function to compute interparticle forces
    % along with any options/parameters needed
    opts.F_SDE_Fn = this.optsStoc.F_SDE_Fn;
    opts.optsF_SDE = this.optsStoc.optsF_SDE;
    
    % default is no radicals
    if(isfield(this.optsPhys,'optsRadicals'))
        opts.optsRadicals = this.optsPhys.optsRadicals;
    else
        opts.optsRadicals = [];
    end
    
    % determine the boundary conditions we're using
    % applyBC is the function that applies the BC
    % (see bottom of file)
    shape = this.optsNum.PhysArea.shape;
    if(strcmp(shape,'FourierLine'))
        if(this.even2periodic)
            applyBC = @Even2PeriodicBC;
            yMin = opts.optsF_SDE.yMin;
            yMax = opts.optsF_SDE.yMax;
        else
            applyBC = @PeriodicBC;
            L = opts.optsF_SDE.L;
        end
    elseif(strcmp(shape,'SpectralLine'))
        applyBC = @NoFluxBC;
        yMin = opts.optsF_SDE.yMin;
        yMax = opts.optsF_SDE.yMax;
    end
    

    
    %---------------------------------------------------------------------%
    % Solve the SDE                                                       %
    %---------------------------------------------------------------------%
    
    % prevent asking if we want to load in DataStorage
    optsOther.SkipLoadQuestion = true;
    % run dynamics and store
    [this.stocResult.dynamics,~,paramsDS] = DataStorage(this.stocDir,...
                            @ComputeODDynamics,opts,optsOther,[],[],false);
    this.FilenameStocDyn = paramsDS.Filename;
    
    function data = ComputeODDynamics(opts,~)
        
        % get parameters
        nParticles = opts.nParticles;
        nSteps = opts.nSteps;
        nRuns = opts.nRuns;
        
        plotTimes = opts.plotTimes;
        tMin = plotTimes(1);
        tMax = plotTimes(end);
        
        % function to evaluate interactions
        F_SDE_Fn = str2func(opts.F_SDE_Fn);
        % parameters for interactions, including R
        optsF_SDE = opts.optsF_SDE;
        
        sigma = opts.sigma;
        
        % timestepping
        h = (tMax-tMin)/nSteps;
        t = tMin:h:tMax;
                
        % find closest timestep to plotTimes
        nSaves = length(plotTimes);
        savePos = zeros(nSaves,1);
        tSave = zeros(nSaves,1);
        for iSave = 1:nSaves
            [~,tempPos] = min(abs(t-plotTimes(iSave)));
            savePos(iSave) = tempPos;
            tSave(iSave) = t(savePos(iSave));
        end
        
        % preallocate positions
        xSave = zeros(nParticles,nSaves,nRuns);
        
        % initial condition and radicals
        xIC = this.stocResult.IC.x;
        xR  = this.stocResult.Radicals.x;
        
        % determine which initial conditions from the samples
        % to use (taken uniformly over the samples)
        nSamples = size(xIC,2);
        ICMask = floor(nSamples/nRuns)*(0:nRuns-1) + 1;
        
        % mask so we don't move the radicals
        mask = 1:nParticles;
                
        % parallel pool
        poolobj = gcp('nocreate'); % If no pool, do not create new one.
        if isempty(poolobj)
            poolInfo = parcluster('local');
            maxWorkers = poolInfo.NumWorkers;
            maxWorkers = min(maxWorkers,10); % limit to 10 workers for niceness
            parpool(maxWorkers);
        end

        % data queue to get information from parallel pool
        dataQueue = parallel.pool.DataQueue;
        afterEach(dataQueue, @printProgress);
        % parameters for parfor updating and estimated finish time
        % (see printProgress)
        nDone = 0;
        lineLength = 0;
        tStart = clock;
        updateCount = 0;
        updateThreshold = nRuns/100; % how often to update (i.e. 100 updates)
        
        parfor iRun = 1:nRuns
            
            % precompute noise for all timesteps
            W=randn(nParticles, nSteps+1);

            % preallocate output positions
            xSaveRun = zeros(nParticles,nSaves);
            
            % add radicals if they are included
            if(~isempty(xR))
                x = [ xIC(:,ICMask(iRun)) ; xR(:,ICMask(iRun))]; %#ok
            else
                x = xIC(:,ICMask(iRun));
            end
            
            % initial condition at time 0
            xSaveRun(:,1) = x(mask);
            % increment which save we're on
            iSave = 2;
            
            for iStep = 2:nSteps+1
                
                % get interaction term
                F = F_SDE_Fn(x,optsF_SDE); %#ok

                % Euler-Maruyama method
                x(mask) = x(mask) - h*F(mask) + sigma*sqrt(h).*W(:,iStep-1);
                
                % apply the relevant boundary conditions
                x = applyBC(x);
                
                % save if at an output time
                if(iStep == savePos(iSave)) %#ok
                    xSaveRun(:,iSave) = x(mask);
                    iSave = iSave + 1;
                end

            end
            
            % update for this run
            xSave(:,:,iRun) = xSaveRun;

            % send data to update progress
            send(dataQueue, iRun);
            
        end
        
        % new line after output from printProgress
        fprintf(1,'\n');
            
        % allocate output
        data.x = xSave;
        if(~isempty(xR))
            data.xR = xR(:,ICMask);
        else
            data.xR = [];
        end
        data.t = plotTimes;
        data.tStoc = tSave;
        data.opts = opts;
        
        % progress in parfor loop
        function printProgress(~)
            % increment number completed and update counter
            nDone = nDone + 1;
            updateCount = updateCount + 1;

            % regular updates
            if(updateCount>=updateThreshold)
                updateCount = 0;
                
                % estimate time remaining
                tTaken = etime(clock,tStart);
                tLeft  = (nRuns - nDone)*(tTaken/nDone);
                tEst   = addtodate(now,round(tLeft),'second');
                tString    = datestr(tEst);
                
                % delete previous message and print new one
                message  = ['SDE: ' num2str(nDone) '/' num2str(nRuns) '; ' ...
                            'Est. finish ' tString];
                fprintf(repmat('\b',1,lineLength));
                lineLength = fprintf(1, message);
            end
        end
        
    end         

    %----------------------------------------------------------------------
    % Boundary condition functions
    %----------------------------------------------------------------------
    
    function x = PeriodicBC(x)
        x = mod(x,L); 
    end

    function x = NoFluxBC(x)
        x(x>yMax) = yMax - (x(x>yMax)-yMax); 
        x(x<yMin) = yMin + (yMin - x(x<yMin)); 
    end

    function x = Even2PeriodicBC(x)
        x(x>yMax) = yMax - (x(x>yMax)-yMax); 
        x(x<yMin) = yMin + (yMin - x(x<yMin)); 
    end

end