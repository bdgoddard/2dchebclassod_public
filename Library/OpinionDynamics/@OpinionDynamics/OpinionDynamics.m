classdef OpinionDynamics < Computation
    
    properties (Access = public)                    
        
% from Computation
%         optsNum,optsPhys
%         configName
%         IDC
%         
%         %subArea-parameters and function
%         subArea,doSubArea,Int_of_path,IP        

        optsStoc = [] % stochastic options

        Conv  % convolution matrix for bounded confidence (x-y)
        ConvQ = [] % convolution matrix for order parameter (1)
               

        rhoR
        rhoI
        
        DataStorageDir = 'OpinionDynamics'
        
        FilenameDyn
        dynamicsResult
        
        FilenameStocDyn
        stocResult
        
        dynamicsDir
        kernelDir
        outputDir
        pdfDir
        globalKernelDir = false;

        stocDir
        stocICDir
        stocRadicalsDir
        globalSamplingDir = false;
        
        even2periodic = false;
        L
        
        doPDE = false
        doSDE = false
        
    end
    
    methods (Access = public)          
        
    %------------------------------------------------------------------
    % Constructor
    %------------------------------------------------------------------

        function this = OpinionDynamics(configuration)      
            
            if(nargin == 0)
                 configuration = [];
            end            
                            
            this@Computation(configuration);            
            
            if(isfield(this.optsNum,'configDir'))
                this.optsNum = rmfield(this.optsNum,'configDir');
            end
            if(isfield(this.optsNum,'configSubDir'))
                this.optsNum = rmfield(this.optsNum,'configSubDir');
            end
            
            Preprocess(this);
                        
            if(isfield(configuration,'optsStoc'))
                this.optsStoc = configuration.optsStoc;
                PreprocessStoc(this);
            end
            
        end
        
    %----------------------------------------------------------------------
    % PDE
    %----------------------------------------------------------------------
        
        %------------------------------------------------------------------
        % Preprocessor
        %------------------------------------------------------------------
    
        function Preprocess(this)

            Preprocess@Computation(this);
            
            SetOptsNumValues(this);
            
            global dirData
                        
            if(isfield(this.optsNum.PhysArea,'even2periodic'))
                this.even2periodic = this.optsNum.PhysArea.even2periodic;
            end
            
            if(isfield(this.optsNum.PhysArea,'even2periodic') && this.even2periodic)
                this.L = this.optsNum.PhysArea.yMax;
            else
                this.L = this.optsNum.PhysArea.yMax - this.optsNum.PhysArea.yMin;
            end
                           
            if(isfield(this.optsNum,'dataDir'))
                this.DataStorageDir = this.optsNum.dataDir;
                this.optsNum = rmfield(this.optsNum,'dataDir');
            end
            
            if(isfield(this.optsNum,'dataSubDir'))
                if(~isempty(this.DataStorageDir))
                    this.DataStorageDir = [this.DataStorageDir filesep this.optsNum.dataSubDir];
                else
                    this.DataStorageDir = [this.optsNum.dataSubDir];
                end
                this.optsNum = rmfield(this.optsNum,'dataSubDir');
            end
            
            if(isfield(this.optsNum,'dynDir'))
                this.dynamicsDir = [this.DataStorageDir filesep this.optsNum.dynDir];
            else
                this.dynamicsDir = [this.DataStorageDir filesep 'Dynamics'];
            end
            if(isfield(this.optsNum,'dynSubDir'))
                this.dynamicsDir = [this.dynamicsDir filesep this.optsNum.dynSubDir];
            end
            
            if(isfield(this.optsNum,'kernelDirGlobal'))
                this.globalKernelDir = true;
                this.kernelDir = this.optsNum.kernelDirGlobal;                
            else
                if(~isempty(this.DataStorageDir))
                    this.kernelDir = [this.DataStorageDir filesep 'Kernels'];
                else
                    this.kernelDir = 'Kernels';
                end
            end            
            
            if(~isempty(this.DataStorageDir))
                this.outputDir = [dirData filesep this.DataStorageDir filesep 'Output'];
            else
                this.outputDir = [dirData filesep 'Output'];
            end
            this.pdfDir    = [this.outputDir filesep 'Pdfs' filesep];
                        
            GetInitialCondition(this);
            GetRadicals(this);
                                    
            if(isfield(this.optsPhys,'optsKernel'))
                this.Conv = PreprocessKernel(this,'HKKernel',this.optsPhys.optsKernel);
            else
                this.Conv = PreprocessKernel(this,'HKKernel');
            end

        end
        
        function SetOptsNumValues(this)
            
            % set defaults for ODE solver and convolution, if they're not
            % already set
            
            fieldList = {'relTol','absTol','kernelN','toEq'};
            defaults.relTol = 10^-8;
            defaults.absTol = 10^-8;
            defaults.kernelN = 50;
            defaults.toEq = false;

            nFields = length(fieldList);
            
            for iField = 1:nFields
                field = fieldList{iField};
                if(~isfield(this.optsNum,field))
                    this.optsNum.(field) = defaults.(field);
                end
            end       
            
            if(this.optsNum.toEq && ~isfield(this.optsNum,'eqTol'))
                this.optsNum.eqTol = 100*this.optsNum.absTol;
            end
                        
        end
                
        %------------------------------------------------------------------
        % Initial condition and radical distribution
        %------------------------------------------------------------------

        function GetInitialCondition(this)

            if(~isfield(this.optsPhys,'optsInitial'))
                this.optsPhys.optsInitial.initialFn = 'OD_Initial_Uniform';
                cprintf('*m','Using default uniform initial condition\n');
            end

            % allow the IC to be specified as if the domain was periodic
            % (optsPhys.optsInitial.periodic = true)
            this.optsPhys.optsInitial = AddPeriodicToOpts(this,this.optsPhys.optsInitial);
            
            this.rhoI = GetDensity(this,this.optsPhys.optsInitial.initialFn,this.optsPhys.optsInitial);
            
        end        

        function GetRadicals(this)
            
            if(~isfield(this.optsPhys,'optsRadicals'))
                this.optsPhys.optsRadicals.radicalsFn = 'OD_Radicals_None';
                cprintf('*m','Using default no radicals\n');
            end

            % allow rhoR to be specified as if the domain was periodic
            % (optsPhys.optsRadicals.periodic = true)
            this.optsPhys.optsRadicals = AddPeriodicToOpts(this,this.optsPhys.optsRadicals);
            
            this.rhoR = GetDensity(this,this.optsPhys.optsRadicals.radicalsFn,this.optsPhys.optsRadicals);
            
            % rhoR needs to be defined on the whole space
            if(this.even2periodic)
                this.rhoR = this.mirror(this.rhoR);
            end
                        
        end        
        
        function rho = GetDensity(this,fnString,params)
            f = str2func(fnString);
            
            if(this.even2periodic)
                yTemp = -flip(this.cut(this.IDC.Pts.y)); % [0,1] from [-1,0]
                rhoTemp = f(yTemp,params);
                rho = this.mirror( flip(rhoTemp) );
            else
                rho = f(this.IDC.Pts.y,params);
            end
            
            Int = this.IDC.Int;
            Z = Int*rho;
            if(abs(Z)>10^-10)
                rho = rho/Z;
            end

            % default is to only produce rho on half of the domain
            if(this.even2periodic)
                rho = 2*this.cut(rho);
            end
            
            if(isfield(params,'M')) 
                M = params.M;
            else
                M = 1;
            end
            
            rho = M*rho;

        end        
        
        %------------------------------------------------------------------
        % Kernel matrices
        %------------------------------------------------------------------

        function Conv = PreprocessKernel(this,kernel,params)

            cprintf('*black',['Computing kernel matrices for ' kernel '\n']);   

            shapeParams.yMax = this.optsPhys.R;
            shapeParams.N = this.optsNum.kernelN; % number of points in convolution domain
            
            paramsKernel.physArea    = this.optsNum.PhysArea;
            paramsKernel.shapeParams = shapeParams;
            paramsKernel.kernel      = kernel;
            if(nargin>2)
                paramsKernel.params = params;
            end
            
            if(isa(this.IDC,'FourierLine'))
                paramsKernel.convFn = 'ComputeConvolutionMatrix';
            elseif(isa(this.IDC,'SpectralLine'))
                paramsKernel.convFn = 'ComputeConvolutionMatrix_Pointwise';
            else
                error('Define unknown domain type for bounded confidence');
            end
            
            optsOther.IDC = this.IDC;
            optsOther.SkipLoadQuestion = true;
            kernelDirFull = [this.kernelDir filesep kernel];
            if(this.globalKernelDir)
                global dirData %#ok
                dirDataOld = dirData;
                ChangeDirData(kernelDirFull);
                kernelDirFull = [];
            end
            %(nameDir,func,Parameters,OtherInputs,recompute,ignoreList,addLoadTime)
            ConvData  = DataStorage(kernelDirFull,@KernelMatrices,paramsKernel,optsOther,[],[],false);
            Conv = ConvData.Conv;
            if(this.globalKernelDir)
                ChangeDirData(dirDataOld);
            end
            
        end

        %------------------------------------------------------------------
        % Dynamics
        %------------------------------------------------------------------

        function ComputeDynamics(this)
            
            cprintf('*black','Computing dynamics \n');   
            
            ComputeOpinionDynamics(this);
            
            this.doPDE = true;
            
        end
        ComputeOpinionDynamics(this);
        
        %------------------------------------------------------------------
        % Data analysis
        %------------------------------------------------------------------
        
        function ComputeMass(this)
                        
            if(this.even2periodic)
                prefactor = 0.5;
            else
                prefactor = 1;
            end

            rho_t = this.dynamicsResult.rho_t;
            
            if(this.even2periodic)
                rho_t = this.mirror(rho_t);
            end
            
            this.dynamicsResult.mass = prefactor * this.IDC.Int * rho_t;
                        
        end

        function ComputeOrderParameter(this)
                        
            if(isempty(this.ConvQ))
                this.ConvQ = PreprocessKernel(this,'HKKernelQ');
            end

            if(this.even2periodic)
                prefactor = 0.5;
            else
                prefactor = 1;
            end

            rho_t = this.dynamicsResult.rho_t;
            
            if(this.even2periodic)
                rho_t = this.mirror(rho_t);
            end
            
            this.dynamicsResult.Q = ...
                prefactor * this.IDC.Int * (rho_t .* (this.ConvQ*rho_t) );
                        
        end
        
        function ComputeModeOpinion(this)
            
            rho_t = this.dynamicsResult.rho_t;
            
            if(this.even2periodic)
                rho_t = this.mirror(rho_t);
            end
            
            Plot_Area = struct('N',1000,'yMin',0,'yMax',1);
            Interp = this.IDC.InterpolationPlot(Plot_Area);
            
            rho_t_Interp = Interp.InterPol * rho_t;
            
            yInterp = Interp.yPlot;
            
            [~,maxPos] = max(rho_t_Interp,[],1);
            
            this.dynamicsResult.modeOpinion = yInterp(maxPos)';
            
        end

        function ComputeMeanOpinion(this)
            
            rho_t = this.dynamicsResult.rho_t;
            
            if(this.even2periodic)
                rho_t = this.mirror(rho_t);
            end
            
            yMean = this.IDC.Pts.y;
            Int = this.IDC.Int;
            
            if(this.even2periodic)
                yMean(yMean<0) = 0;
            end
            
            this.dynamicsResult.meanOpinion = (yMean'.*Int)*rho_t;
                        
        end
        
        
        %------------------------------------------------------------------
        % Auxiliary functions for even2periodic and periodic
        %------------------------------------------------------------------

        function rho = mirror(~,rhoM)
            rho = [rhoM;flip(rhoM(2:end-1,:),1)];
        end

        function rhoM = cut(this,rho)
            rhoM = rho(1:this.IDC.Pts.N/2+1,:);
        end
        
        %------------------------------------------------------------------

        function opts = AddPeriodicToOpts(this,opts)
            if( isa(this.IDC,'FourierLine') && ~this.even2periodic)
                opts.periodic = true;
                opts.periodicL = this.L;
            elseif(isfield(opts,'periodic') && opts.periodic)
                opts.periodicL = this.L;
            else
                opts.periodic = false;
            end
        end

        
    %----------------------------------------------------------------------
    % SDE
    %----------------------------------------------------------------------
        
        %------------------------------------------------------------------
        % Preprocessor
        %------------------------------------------------------------------
            
        function PreprocessStoc(this)
            
            if(~isempty(this.DataStorageDir))
                this.stocDir = [this.DataStorageDir filesep 'Stochastics'];
            else
                this.stocDir = 'Stochastics';
            end
            
            if(isfield(this.optsNum,'dynDir'))
                this.stocDir = [this.stocDir filesep this.optsNum.dynDir];
            else
                this.stocDir = [this.stocDir filesep 'Dynamics'];
            end
            if(isfield(this.optsNum,'dynSubDir'))
                this.stocDir = [this.stocDir filesep this.optsNum.dynSubDir];
            end
            
            if(isfield(this.optsNum,'samplingDirGlobal'))
                this.globalSamplingDir = true;
                this.stocICDir = [this.optsNum.samplingDirGlobal filesep 'InitialConditions']; 
                this.stocRadicalsDir = [this.optsNum.samplingDirGlobal filesep 'Radicals'];
            else
                this.stocICDir = [this.stocDir filesep 'InitialConditions'];
                this.stocRadicalsDir = [this.stocDir filesep 'Radicals'];
            end
            
            this.stocICDir = [this.stocICDir filesep this.optsPhys.optsInitial.initialFn];
            this.stocRadicalsDir = [this.stocRadicalsDir filesep this.optsPhys.optsRadicals.radicalsFn];
            
            if(~isfield(this.optsStoc,'thin'))
                this.optsStoc.thin = 1;
            end
            
            if(~isfield(this.optsStoc,'burnin'))
                this.optsStoc.burnin = 0;
            end                        
            
            this.optsStoc.L = this.optsNum.PhysArea.yMax - this.optsNum.PhysArea.yMin;
            
            if(~isfield(this.optsStoc,'nBins'))
                this.optsStoc.nBins = 100;
            end
            if(this.even2periodic)
                yMin = 0;
            else
                yMin = this.optsNum.PhysArea.yMin;
            end
            yMax = this.optsNum.PhysArea.yMax;
            this.stocResult.dx = (yMax-yMin)/this.optsStoc.nBins;
            this.stocResult.edges = yMin:this.stocResult.dx:yMax;
            this.stocResult.mids = this.stocResult.edges(1:end-1)+this.stocResult.dx/2;
            
            shape = this.optsNum.PhysArea.shape;
            if(strcmp(shape,'FourierLine'))
                if(isfield(this.optsNum.PhysArea,'even2periodic') && this.optsNum.PhysArea.even2periodic)
                    F_SDE_Fn = 'F_SDE_Even2Periodic';
                    nRij_SDE_Fn = 'nRij_SDE_Even2Periodic';
                    optsF_SDE.yMin = 0;
                    optsF_SDE.yMax = this.optsNum.PhysArea.yMax;
                    optsF_SDE.L = this.optsStoc.L;
                else
                    F_SDE_Fn = 'F_SDE_Periodic';
                    nRij_SDE_Fn = 'nRij_SDE_Periodic';
                    optsF_SDE.L = this.optsStoc.L;
                end
            elseif(strcmp(shape,'SpectralLine'))
                F_SDE_Fn = 'F_SDE_NoFlux';
                nRij_SDE_Fn = 'nRij_SDE_NoFlux';
                optsF_SDE.yMin = this.optsNum.PhysArea.yMin;
                optsF_SDE.yMax = this.optsNum.PhysArea.yMax;
            end
                
            this.optsStoc.F_SDE_Fn = F_SDE_Fn;
            this.optsStoc.nRij_SDE_Fn = nRij_SDE_Fn;
            
            optsF_SDE.R          = this.optsPhys.R;
            optsF_SDE.nParticles = this.optsStoc.nParticles;
            this.optsStoc.optsF_SDE = optsF_SDE;
            
        end
        
        
        %------------------------------------------------------------------
        % Stochastic initial sampling
        %------------------------------------------------------------------

        function GetStocInitialCondition(this)
            
            cprintf('*black','Computing stochastic intitial condition \n');   
            
            opts.rhoFn = this.optsPhys.optsInitial.initialFn;
            opts.params = this.optsPhys.optsInitial;
            opts.params = rmfield(opts.params,'initialFn');
            if(this.even2periodic)
                opts.yMin = 0;
            else
                opts.yMin = this.optsNum.PhysArea.yMin;
            end
            opts.yMax = this.optsNum.PhysArea.yMax;
            opts.thin = this.optsStoc.thin;
            opts.burnin = this.optsStoc.burnin;
            opts.nSamples = this.optsStoc.nSamples;
            opts.nParticles = this.optsStoc.nParticles;

            opts.initialGuess = (opts.yMin + opts.yMax)/2;
            
            optsOther.SkipLoadQuestion = true;
            
            stocICDirFull = this.stocICDir;
            if(this.globalSamplingDir)
                global dirData %#ok
                dirDataOld = dirData;
                ChangeDirData(stocICDirFull);
                stocICDirFull = [];
            end            
            
            this.stocResult.IC = DataStorage(stocICDirFull,@SampleStocInitialCondition,opts,optsOther,[],[],false);
            if(this.globalSamplingDir)
                ChangeDirData(dirDataOld);
            end
            
        end

        function GetStocRadicals(this)
               
            if(strcmp(this.optsPhys.optsRadicals.radicalsFn,'OD_Radicals_None'))
                this.stocResult.Radicals.x = [];
            else
                
                cprintf('*black','Computing stochastic radicals \n');
                
                opts.rhoFn = this.optsPhys.optsRadicals.radicalsFn;
                opts.params = this.optsPhys.optsRadicals;
                opts.params = rmfield(opts.params,'radicalsFn');

                opts.thin = this.optsStoc.thin;
                opts.burnin = this.optsStoc.burnin;
                opts.nSamples = this.optsStoc.nSamples;

                opts.yMax = this.optsNum.PhysArea.yMax;
                
                if(this.even2periodic)
                    N = this.IDC.N;
                    rhoRPlus = this.rhoR(N/2+1:N);
                    [~,maxPos] = max(rhoRPlus);
                    maxPos = maxPos + N/2;
                    
                    radicalProportion = this.IDC.Int*this.rhoR/2;
                    
                    opts.yMin = 0;
                else
                    [~,maxPos] = max(this.rhoR);
                    
                    radicalProportion = this.IDC.Int*this.rhoR;
                    
                    opts.yMin = this.optsNum.PhysArea.yMin;
                end
                                
                opts.nParticles = round(radicalProportion*this.optsStoc.nParticles);
                opts.initialGuess = this.IDC.Pts.y(maxPos);

                optsOther.SkipLoadQuestion = true;
                
                stocRadicalsDirFull = this.stocRadicalsDir;
                if(this.globalSamplingDir)
                    global dirData %#ok
                    dirDataOld = dirData;
                    ChangeDirData(stocRadicalsDirFull);
                    stocRadicalsDirFull = [];
                end
                this.stocResult.Radicals = DataStorage(stocRadicalsDirFull,@SampleStocInitialCondition,opts,optsOther,[],[],false);
                if(this.globalSamplingDir)
                    ChangeDirData(dirDataOld);
                end
                
            end
                
        end
        
        
        %------------------------------------------------------------------
        % Stochastic dynamics
        %------------------------------------------------------------------

        function ComputeStocDynamics(this)
            
            if(~isfield(this.stocResult,'IC'))
                GetStocInitialCondition(this);
            end
            if(~isfield(this.stocResult,'Radicals'))
                GetStocRadicals(this);
            end

            cprintf('*black','Computing stochastic dynamics \n');   
            
            ComputeStocOpinionDynamics(this);
            
            ComputeStocDensitiesDynamics(this);
            
            xR = this.stocResult.dynamics.xR;
            this.stocResult.dynamics.rhoR = ComputeStocDensity(this,xR);
            
            this.doSDE = true;

        end
        ComputeStocOpinionDynamics(this);
        %ComputeStocOpinionDynamics_old(this);
        
        %------------------------------------------------------------------
        % Stochastic densities (histograms)
        %------------------------------------------------------------------

        function ComputeStocDensitiesDynamics(this)
            
            nPlots = length(this.optsNum.plotTimes);
            xFull = this.stocResult.dynamics.x;
            
            edges = this.stocResult.edges;
            dx = this.stocResult.dx;
            
            nParticles = this.optsStoc.nParticles;
            nRuns = this.optsStoc.nRuns;
            
            rho = zeros(length(this.stocResult.mids),nPlots);
            
            for iPlot = 1:nPlots
                x = xFull(:,iPlot,:);
                x = x(:);
                [bincounts,~] = histcounts(x,edges);

                bincounts = bincounts/nParticles/nRuns/dx;
                rho(:,iPlot) = bincounts;
            end
            %sum(bincounts*dx);
            
            this.stocResult.dynamics.rho_t = rho;

        end
        
        function rho = ComputeStocDensity(this,x)
            
            nRuns = size(x,2);
            nParticles = this.optsStoc.nParticles;
            x = x(:);
            edges = this.stocResult.edges;
            dx = this.stocResult.dx;
            [bincounts,~] = histcounts(x,edges);
            rho = bincounts/nParticles/nRuns/dx;
            rho = rho';
            
        end

        %------------------------------------------------------------------
        % Stochastic order parameter
        %------------------------------------------------------------------

        function ComputeStocOrderParameter(this)
            
            nParticles = this.optsStoc.nParticles;
            plotTimes = this.optsNum.plotTimes;
            nPlots = length(plotTimes);
            
            Q = zeros(nPlots,1);
            x = this.stocResult.dynamics.x;
            
            nRuns = size(x,3);
            
            nRij_SDE_Fn = str2func(this.optsStoc.nRij_SDE_Fn);
            for iPlot = 1:nPlots  
                QTemp = zeros(nRuns,1);
                for iRun = 1:nRuns
                    nRij = nRij_SDE_Fn(x(:,iPlot,iRun),this.optsStoc.optsF_SDE);
                    QTemp(iRun) = (nParticles^(-2))*sum(nRij);
                end
                Q(iPlot) = mean(QTemp);
            end
            
            this.stocResult.dynamics.Q = Q;
            
        end
        
        
    %----------------------------------------------------------------------
    % Plotting scalar variables
    %----------------------------------------------------------------------

        %------------------------------------------------------------------
        % Plot order paramter
        %------------------------------------------------------------------

        function hStruct = PlotOrderParameter(this,opts)
                                    
            % PDE Computation
            if(~isfield(this.dynamicsResult,'Q') && isfield(this.dynamicsResult,'t'))
                this.ComputeOrderParameter;
            end

            % SDE Computation
            if(isfield(this.stocResult,'dynamics'))
                if(~isfield(this.stocResult.dynamics,'Q'))
                    this.ComputeStocOrderParameter;
                end
            end
            
            if(nargin<2 || ~isfield(opts,'yLabel'))
                opts.yLabel = 'Order Parameter';
            end

            if(isfield(opts,'QLim'))
                opts.yLim = opts.QLim;
            else
                opts.yLim = [0,1];
            end
            
            opts.linestyle = {'-','--'};
            
            % Does PDE and/or SDE plotting
            hStruct = PlotDynamicValue(this,'Q',opts);            
        end
        
        %------------------------------------------------------------------
        % Plot mass
        %------------------------------------------------------------------

        function hStruct = PlotMass(this,opts)
            if(~isfield(this.dynamicsResult,'mass') && isfield(this.dynamicsResult,'t'))
                this.ComputeMass;
            end
            
            if(nargin<2 || ~isfield(opts,'yLabel'))
                opts.yLabel = 'Mass';
            end
            hStruct = PlotDynamicValue(this,'mass',opts);            
        end

        %------------------------------------------------------------------
        % Plot maximum opinion
        %------------------------------------------------------------------
        
        function hStruct = PlotModeOpinion(this,opts)
                                    
            % PDE Computation
            if(~isfield(this.dynamicsResult,'modeOpinion') && isfield(this.dynamicsResult,'t'))
                this.ComputeModeOpinion;
            end

%             % SDE Computation
%             if(isfield(this.stocResult,'dynamics'))
%                 if(~isfield(this.stocResult.dynamics,'Q'))
%                     this.ComputeStocOrderParameter;
%                 end
%             end
            
            if(nargin<2 || ~isfield(opts,'yLabel'))
                opts.yLabel = 'Mode Opinion';
            end

            opts.yLim = [0,1];

            hStruct = PlotDynamicValue(this,'modeOpinion',opts);
        end


        %------------------------------------------------------------------
        % Plot mean opinion
        %------------------------------------------------------------------
        
        function hStruct = PlotMeanOpinion(this,opts)
                                    
            % PDE Computation
            if(~isfield(this.dynamicsResult,'meanOpinion') && isfield(this.dynamicsResult,'t'))
                this.ComputeMeanOpinion;
            end

%             % SDE Computation
%             if(isfield(this.stocResult,'dynamics'))
%                 if(~isfield(this.stocResult.dynamics,'Q'))
%                     this.ComputeStocOrderParameter;
%                 end
%             end
            
            if(nargin<2 || ~isfield(opts,'yLabel'))
                opts.yLabel = 'Mean Opinion';
            end

            opts.yLim = [0,1];

            hStruct = PlotDynamicValue(this,'meanOpinion',opts);
        end
        
        %------------------------------------------------------------------
        % Plot mean opinion
        %------------------------------------------------------------------
        
        function hStruct = PlotMeanModeOpinion(this,opts)
                                    
            % PDE Computation
            if(~isfield(this.dynamicsResult,'meanOpinion') && isfield(this.dynamicsResult,'t'))
                this.ComputeMeanOpinion;
            end
            if(~isfield(this.dynamicsResult,'modeOpinion') && isfield(this.dynamicsResult,'t'))
                this.ComputeModeOpinion;
            end

            
%             % SDE Computation
%             if(isfield(this.stocResult,'dynamics'))
%                 if(~isfield(this.stocResult.dynamics,'Q'))
%                     this.ComputeStocOrderParameter;
%                 end
%             end
            
            if(nargin<2 || ~isfield(opts,'yLabel'))
                opts.yLabel = 'Opinion';
            end

            opts.yLim = [0,1];

            opts.linecolor = {'r','b'};
            opts.linestyle = {'-','--'};
            opts.legend = {'Mean','Mode'};
            
            hStruct = PlotDynamicValue(this,{'meanOpinion','modeOpinion'},opts);
        end

        
        %------------------------------------------------------------------
        % Plot general scalar value
        %------------------------------------------------------------------
        
        function hStruct = PlotDynamicValue(this,varName,opts)
            
            if(nargin < 3)
                opts = struct;
            end
            
            if(~iscell(varName))
                varName = {varName};
            end

            plotTimes = GetPlotTimes(this);
            
            opts.varName = varName;
            opts.nVars = length(varName);
            opts.name = 'Plot';
            opts.xLabel = 'Time';     
            
            [hStruct,opts] = SetUpPlots(this,opts);
            
            name = opts.name;
            ha = hStruct.ha;
            nVars = opts.nVars;
            
            hp = zeros(nVars,1);
            hpS = zeros(nVars,1);
                        
            if(isfield(opts,'tLog') && opts.tLog)
                plotFn = @semilogx;
                if(~isfield(opts,'xLim'))
                    opts.xLim = [plotTimes(2),plotTimes(end)];
                end
            else
                plotFn = @plot;
            end

            if(isfield(opts,'legend'))
                legend = opts.legend;
                if(~iscell(legend))
                    legend = {legend};
                end
                if(~(length(legend)==nVars))
                    legend = [];
                end    
            else
                legend = [];
            end
            
            for iVar = 1:nVars

                if(~isempty(legend))
                    legText = legend{iVar};
                else
                    legText = varName{iVar};
                end
                
                if(isfield(this.dynamicsResult,varName{iVar}))
                    hp(iVar) = plotFn(ha,plotTimes,this.dynamicsResult.(varName{iVar}), ...
                        'DisplayName',legText, ...
                        ...'linestyle',opts.linestyle{iVar}, ...
                        'linestyle','-', ...
                        'linewidth',opts.linewidth{iVar} );
                    if(opts.setColor)
                        set(hp(iVar),'color',opts.linecolor{iVar});
                    end
                end
                
                hold on
                
                if(isfield(this.stocResult,'dynamics') && isfield(this.stocResult.dynamics,varName{iVar}))
                    hpS(iVar) = plotFn(ha,plotTimes,this.stocResult.dynamics.(varName{iVar}), ...
                        'DisplayName',legText, ...
                        ... 'linestyle',opts.linestyle{iVar}, ...
                        'linestyle','--', ...
                        'linewidth',opts.linewidth{iVar});
                    if(opts.setColor)
                        set(hpS(iVar),'color',opts.linecolor{iVar});
                    end
                end
                
            end

            hStruct.hp = hp;
            hStruct.hpS = hpS;
            
            opts.latex = true;
            FixPlots(this,hStruct,opts);
                        
            if(isfield(opts,'save') && opts.save)
                SavePlot(this,name,hStruct.hf);
            end
            
        end
        
    %----------------------------------------------------------------------
    % Plot densities and radicals
    %----------------------------------------------------------------------

        %------------------------------------------------------------------
        % Plot final densities
        %------------------------------------------------------------------

        function PlotDensityRadicalsFinal(this,opts)
            if(nargin<2)
                opts = struct;
            end
            
            opts.name = 'Final';
            
            [~,opts.tPos] = GetPlotTimes(this);
            
            opts.setColor = true;
            opts.legTime  = false;
                        
            PlotSnapshotsDensityRadicals(this,opts);
            
            if(isfield(opts,'save') && opts.save)
                SavePlot(this,'Final',hStruct.hf);
            end
            
        end

        %------------------------------------------------------------------
        % Plot density snapshots
        %------------------------------------------------------------------

        function hStruct = PlotSnapshotsDensityRadicals(this,opts)
            
            if(nargin<2)
                opts = struct;
            end

            [opts,varName] = SetupDensityRadicalsPlot(this,opts);

            if(~isfield(opts,'xLabel'))
                opts.xLabel = 'Opinion';
            end
            if(~isfield(opts,'yLabel'))
                opts.yLabel = 'Density';
            end

            hStruct = PlotSnapshots(this,varName,opts);
            
            if(isfield(opts,'asymptoticsFunction'))
                opts.ha = hStruct.ha;
                PlotAsymptotics(this,opts);
                FixPlots(this,hStruct,opts);
            end
            
            if(isfield(opts,'save') && opts.save)
                SavePlot(this,'Snapshots',hStruct.hf);
            end
            
        end

        function hStruct = PlotAsymptotics(this,opts)
            
            if(nargin<2 || ~isfield(opts,'asymptoticsFunction'))
                error('Must specify opts.asymptoticsFunction');
            end
            
            aFn = str2func(opts.asymptoticsFunction);
            rhoA = aFn(this.IDC,this.optsPhys);
            opts.legend = 'Asymptotics';
            opts.linecolor = 'k';
            opts.linestyle = '--';
            hStruct = PlotDensity(this,rhoA,opts);
            
        end
        
        %------------------------------------------------------------------
        % Plot density movie
        %------------------------------------------------------------------

        function hStruct = PlotMovieDensityRadicals(this,opts)
            
            if(nargin<2)
                opts = struct;
            end
            
            if(strcmp(this.optsPhys.optsRadicals.radicalsFn,'OD_Radicals_None') ...
                    || (isfield(opts,'radicals') && ~opts.radicals))
                varName = {'rho_t'};
                nVars = 1;
            else
                varName = {'rho_t','rhoR'};
                nVars = 2;
            end
            
            default.linecolor = {'r','k'};
            default.linewidth = {1.5,1.5};
            default.linestyle = {'-','-'};
            default.legend = {'$\rho$','$\rho_r$'};
                        
            props = {'linestyle','linecolor','linewidth','legend'};
            nProps = length(props);
            for iProp = 1:nProps
                prop = props{iProp};
                if(~isfield(opts,prop))
                    opts.(prop) = default.(prop)(1:nVars);
                end
            end
            
            opts.yMin = 0;
            opts.latex = true;
            
            if(~isfield(opts,'xLabel'))
                opts.xLabel = 'Opinion';
            end
            if(~isfield(opts,'yLabel'))
                opts.yLabel = 'Density';
            end

            hStruct = PlotMovie(this,varName,opts);
        end

        %------------------------------------------------------------------
        % Set up density radical plots
        %------------------------------------------------------------------

        function [opts,varName] = SetupDensityRadicalsPlot(this,opts)
                        
            if(strcmp(this.optsPhys.optsRadicals.radicalsFn,'OD_Radicals_None') ...
                    || (isfield(opts,'radicals') && ~opts.radicals))
                varName = {'rho_t'};
                nVars = 1;
            else
                varName = {'rho_t','rhoR'};
                nVars = 2;
            end
            
            default.linecolor = {'r','k'};
            default.linewidth = {1.5,1.5};
            default.linestyle = {'-','-'};
            if(nVars == 2)
                default.legend = {'$\rho$','$\rho_r$'};
            else
                default.legend = {'$\rho$'};
            end
            
            props = {'linestyle','linecolor','linewidth','legend'};
            nProps = length(props);
            for iProp = 1:nProps
                prop = props{iProp};
                if(~isfield(opts,prop))
                    opts.(prop) = default.(prop)(1:nVars);
                end
            end
            
            if(~isfield(opts,'latex'))
                opts.latex = true;
            end
            
            opts.yMin = 0;
            
        end
        
        %------------------------------------------------------------------
        % Plot time-space densities
        %------------------------------------------------------------------

        function PlotTimeSpaceDensity(this,opts)
            
            nPlots = nnz([this.doPDE,this.doSDE]);
                        
            if( isfield(opts,'ha') && (length(opts.ha) == nPlots) )
                ha = opts.ha;
            else
                figure('Position',[100,100,1200,800]);
                ha(1) = subplot(1,nPlots,1);
                if(nPlots == 2)
                    ha(2) = subplot(1,nPlots,2);
                end
            end
            
            iPlot = 1;
            
            zMaxPDE = 0;
            zMaxSDE = 0;
            
            opts.zLabel = '$\rho(t)$';
            
            if(this.doPDE)
                opts.ha = ha(iPlot);
                opts.PDE = true;
                zLim = PlotTimeSpace(this,'rho_t',opts);
                iPlot = iPlot + 1;
%                 zLim = get(opts.ha,'zlim');
                zMaxPDE = zLim(2);
            end
            
            if(this.doSDE)
                opts.ha = ha(iPlot);
                opts.PDE = false;
                zLim = PlotTimeSpace(this,'rho_t',opts);
%                 zLim = get(opts.ha,'zlim');
                zMaxSDE = zLim(2);
            end
            
            if(~isfield(opts,'zLim'))
                zLim = [0;max(zMaxPDE,zMaxSDE)];
            else
                zLim = opts.zLim;
            end
            
            for iPlot = 1:nPlots
                axes(ha(iPlot)); %#ok
                set(ha,'zlim',zLim);
                set(ha,'clim',zLim);
            end
            
            for iha = 1:length(ha)
                colorbar(ha(iha))
            end
            
        end

    %----------------------------------------------------------------------
    % General plotting functions
    %----------------------------------------------------------------------
        %------------------------------------------------------------------
        % Plot PDE density
        %------------------------------------------------------------------

        function hStruct = PlotDensity(this,rho,opts)
                        
            opts.nVars = 1;
             
            [hStruct,opts] = SetUpPlots(this,opts);
            
            if(this.even2periodic)
                if(length(rho) == this.IDC.Pts.N/2+1)
                    rho = this.mirror(rho);
                end
            end

            if(~isfield(opts,'xLabel'))
                opts.xLabel = 'Opinion';
            end
            if(~isfield(opts,'yLabel'))
                opts.yLabel = 'Density';
            end
            
            props = {'linestyle','linecolor','linewidth'};
            nProps = length(props);
            for iProp = 1:nProps
                prop = props{iProp};
                opts.(prop) = opts.(prop){1};
            end
            
            opts.plain = true;
            
            if(~opts.setColor)
                opts = rmfield(opts,'linecolor');
            end
            
            if(isfield(opts,'legend'))
                opts.displayname = opts.legend;
            end
            
            hStruct.hPlot = this.IDC.plot(rho,opts);
            
            if(~isfield(opts,'external') || ~opts.external)
                FixPlots(this,hStruct,opts);
            end
            
            %drawnow
        
        end
        
        %------------------------------------------------------------------
        % Plot SDE density
        %------------------------------------------------------------------

        function hStruct = PlotDensityStoc(this,rho,opts)
            opts.nVars = 1;
             
            [hStruct,opts] = SetUpPlots(this,opts);
            
            mids = this.stocResult.mids;
            %hs = scatter(hStruct.ha,mids(1:2:end),rho(1:2:end));
            hs = plot(hStruct.ha,mids(1:2:end),rho(1:2:end),'o');
            
            hs.DisplayName = opts.legend;
            hs.LineWidth = opts.linewidth{1};
            hs.MarkerSize = 3*hs.LineWidth; 
            if(opts.setColor)
                hs.MarkerEdgeColor = opts.linecolor{1};
            end
            if(this.even2periodic)
                xlim([0,this.optsNum.PhysArea.yMax]);
            else
                xlim([this.optsNum.PhysArea.yMin,this.optsNum.PhysArea.yMax]);
            end
            
            if(~isfield(opts,'xLabel'))
                opts.xLabel = 'Opinion';
            end
            if(~isfield(opts,'yLabel'))
                opts.yLabel = 'Density';
            end

            if(~isfield(opts,'external') || ~opts.external)
                FixPlots(this,hStruct,opts);
            end
            
            hStruct.hPlot = hs;
                        
            %drawnow
            
        end
        
        %------------------------------------------------------------------
        % Plot given variable at given time
        %------------------------------------------------------------------

        function PlotVar(this,varSDE,varPDE,iVar,iT,optsT)
             
                oldVersion = verLessThan('matlab','9.8'); % Matlab 2020a
                
                if(this.doPDE)
                    varPDET = varPDE{iVar}(:,iT);
                    hStructPDE = PlotDensity(this,varPDET,optsT);
                    if(~oldVersion)
                        set(hStructPDE.hPlot,'SeriesIndex',optsT.SeriesIndex);
                    end
                    hold on
                end
                                
                if(this.doSDE)
                    if(this.doPDE && oldVersion)
                        set(gca, 'ColorOrder', circshift(get(gca, 'ColorOrder'), 1))
                    end                   
                    
                    varSDET = varSDE{iVar}(:,iT);
                    hStructSDE = PlotDensityStoc(this,varSDET,optsT);
                    
                    if(this.doPDE && ~oldVersion)
                        set(hStructSDE.hPlot,'SeriesIndex',optsT.SeriesIndex);
                    end
                    
                    hold on
                end
                                
                % combine legends if both are on
                if(this.doPDE && this.doSDE)
                    hPDE = hStructPDE.hPlot;
                    hSDE = hStructSDE.hPlot;
                    if(oldVersion)
                        set(gca, 'ColorOrder', circshift(get(gca, 'ColorOrder'), 1))
                        hLeg = plot(NaN,NaN);
                    else
                        hLeg = plot(NaN,NaN,'SeriesIndex',optsT.SeriesIndex);
                    end
                    set(hLeg,'LineStyle',hPDE.LineStyle,'LineWidth',hPDE.LineWidth',...
                        'Marker',hSDE.Marker','MarkerSize',3*hPDE.LineWidth,'Color',hPDE.Color,...
                        'MarkerEdgeColor',hPDE.Color,'DisplayName',hPDE.DisplayName);
                    set(hPDE,'HandleVisibility','off');
                    set(hSDE,'HandleVisibility','off');
                end
                
        end
        
        %------------------------------------------------------------------
        % Plot snapshots
        %------------------------------------------------------------------

        function hStruct = PlotSnapshots(this,varName,opts)
            
            if(nargin < 3)
                opts = struct;
            end
            
            if(~iscell(varName))
                varName = {varName};
            end
            
            if(~isfield(opts,'name'))
                opts.name = 'Snapshots';
            end
                
            autoSetColor = false;
            if(~isfield(opts,'setColor'))
                autoSetColor = true;
                opts.setColor = false;
            end

            if(~isfield(opts,'legTime'))
                opts.legTime = true;
            end
            
            [varPDE,varSDE,opts] = ExtractVarsPDESDE(this,varName,opts);            
            
            [plotTimes,nT] = GetPlotTimes(this);

            if(isfield(opts,'tPos'))
                tPos = opts.tPos;
            else
                tPos = 1:nT;
            end
            
            if(isfield(opts,'snapshotTimes'))
                tPos = GetTPos(this,opts.snapshotTimes,plotTimes);
            end
            
            opts.varName = varName;
            opts.nVars = length(varName);
            opts.xLabel = 'Opinion';                 
            opts.Position = [100 100 1200 800];
            
            [hStruct,opts] = SetUpPlots(this,opts);
            
            opts.ha = hStruct.ha;
            nVars = opts.nVars;
            timeDependent = opts.timeDependent;

            optsT = opts;
            props = {'linestyle','linewidth','linecolor'};
                                    
            opts.external = true; % prevent updating legend and axes every plot
            
            SeriesIndex = 0;
            
            for iT = tPos
                
                for iVar = 1:nVars
                    if(timeDependent(iVar))
                             
                        optsT = SetProps(this,opts,props,iVar);
                        
                        SeriesIndex = SeriesIndex + 1;
                        optsT.SeriesIndex = SeriesIndex;

                        if(opts.legTime && (opts.nVars == 1))
                            optsT.legend = ['t = ' num2str(plotTimes(iT))];
                        elseif(opts.legTime && (opts.nVars > 1))
                            optsT.legend = [opts.legend{iVar} ', t = ' num2str(plotTimes(iT))];
                        else
                            optsT.legend = opts.legend{iVar};
                        end
                        
                        PlotVar(this,varSDE,varPDE,iVar,iT,optsT);
                                                
                    end
                end
                
            end
            
            % fix colors for time-independent plots
            if(autoSetColor)
                opts.setColor = true;
            end
            [hStruct,opts] = SetUpPlots(this,opts);

            
            for iVar = 1:nVars
                
                SeriesIndex = SeriesIndex + 1;
                
                if(~timeDependent(iVar))

                    optsT = SetProps(this,opts,props,iVar);

                    optsT.SeriesIndex = SeriesIndex;
                    optsT.legend = opts.legend{iVar};

                    PlotVar(this,varSDE,varPDE,iVar,1,optsT);
                
                end
            end
                        
            FixPlots(this,hStruct,opts)
            
            if(isfield(opts,'title'))
                optsT.title = opts.title;
            end
            
            if(isfield(opts,'save') && opts.save)
                SavePlot(this,opts.name,hStruct.hf);
            end
                
        end
        
        %------------------------------------------------------------------
        % Plot time-space
        %------------------------------------------------------------------

        function zLim = PlotTimeSpace(this,varName,opts)

            % FIX PLOTS FOR NUMBER OF VARIABLES IN SUBPLOTS
            
            if(nargin < 3)
                opts = struct;
            end

            if(~isfield(opts,'PDE'))
                opts.PDE = true;
            end
            PDE = opts.PDE;
            
            if(~iscell(varName))
                varName = {varName};
            end

            opts.name = 'TimeSpace';
            
            [plotTimes,~] = GetPlotTimes(this);
            
            opts.varName = varName;
            opts.nVars = length(varName);
            opts.xLabel = 'Opinion';                 
            opts.Position = [100 100 1200 800];
            
            [hStruct,opts] = SetUpPlots(this,opts);
            
            opts.setColor = false;
            
            opts.ha = hStruct.ha;
            nVars = opts.nVars;

            zMin = NaN;
            zMax = NaN;
           
            for iVar = 1:nVars
            
                if(PDE)
                    var = this.dynamicsResult.(varName{iVar});
                    
                    if(this.even2periodic)
                        if(size(var,1) == this.IDC.Pts.N/2+1)
                            var = this.mirror(var);
                        end
                    end
                    
                    varVals = this.IDC.Interp.InterPol * var;            
                    [X,Y] = meshgrid(this.IDC.Interp.yPlot,plotTimes);

                else
                    var = this.stocResult.dynamics.(varName{iVar});
                    varVals = var;
                    [X,Y] = meshgrid(this.stocResult.mids,plotTimes);
                end       
                                
                surf(X,Y,varVals.');
                
                zMin = min(zMin,min(varVals(:)));
                zMax = max(zMax,max(varVals(:)));
                
                if(isfield(opts,'tLog') && opts.tLog)
                    set(opts.ha,'yscale','log');
                end
            end
                                    
            opts.xLabel = 'Opinion';
            opts.yLabel = 'Time';

            opts.xlim = [this.IDC.yMin,this.IDC.yMax];
            opts.yLim = [plotTimes(1),plotTimes(end)];
            FixPlots(this,hStruct,opts);
            legend(hStruct.ha,'off');
            
            if(isfield(opts,'zLabel'))
                zlabel(opts.zLabel,'interpreter','latex');
            else
                zlabel(varName);
            end
            
            zLim = get(gca,'zlim');
            
            if(isfield(opts,'xy') && opts.xy)
                zLim = [zMin;zMax];
                view([0,90]);
                if(isfield(opts,'interp') && opts.interp)
                    shading interp;
                end
            end
            
            if(isfield(opts,'save') && opts.save)
                SavePlot(this,opts.name,hStruct.hf);
            end
                
        end

        %------------------------------------------------------------------
        % Movie
        %------------------------------------------------------------------
        
        function hStruct = PlotMovie(this,varName,opts)
            
            if(nargin < 3)
                opts = struct;
            end
            
            if(~iscell(varName))
                varName = {varName};
            end
                        
            opts.name = 'Movie';

            [varPDE,varSDE,opts] = ExtractVarsPDESDE(this,varName,opts);

            [plotTimes,nT] = GetPlotTimes(this);
            
            doPdfs = (isfield(opts,'save') && opts.save);
            if(doPdfs)
                [nd,pdfFileNames] = setupPdfMovie(plotTimes,this.pdfDir);
            end
                        
            opts.varName = varName;
            opts.nVars = length(varName);
            opts.xLabel = 'Opinion';     
            
            opts.Position = [100 100 1200 800];
            
            [hStruct,opts] = SetUpPlots(this,opts);
            
            hf = hStruct.hf;
            opts.ha = hStruct.ha;
            nVars = opts.nVars;
            timeDependent = opts.timeDependent;
                        
            props = {'linestyle','linecolor','linewidth','legend'};
                     
            opts.external = true;
            
            for iT = 1:nT
                hold off

                opts.title = ['t = ' num2str(plotTimes(iT))];

                for iVar = 1:nVars

                    optsT = SetProps(this,opts,props,iVar);

                    if(timeDependent(iVar))
                        PlotVar(this,varSDE,varPDE,iVar,iT,optsT);
                    else
                        PlotVar(this,varSDE,varPDE,iVar,1,optsT);
                    end
                                        
                end        
                
                FixPlots(this,hStruct,opts)
                
                if(doPdfs)
                    pdfFileNames = savePdfFrame(this.pdfDir,pdfFileNames,iT,nd,hf);
                end
                                
                if(~doPdfs)
                    pause(0.1);
                end

            end
            
            if(doPdfs)
               makePdfMovie(this.pdfDir,pdfFileNames);
            end
            
        end


    %------------------------------------------------------------------
    % Auxiliary plotting functions
    %------------------------------------------------------------------

        %------------------------------------------------------------------
        % Extract PDE and SDE variables
        %------------------------------------------------------------------

        function [varPDE,varSDE,opts] = ExtractVarsPDESDE(this,varName,opts)

            if(this.doPDE)            
                opts.PDE = true;
                [varPDE,opts] = ExtractVars(this,varName,opts);
            else
                varPDE = [];
            end
            if(this.doSDE)
                opts.PDE = false;
                [varSDE,opts] = ExtractVars(this,varName,opts);
            else
                varSDE = [];
            end
            
            opts = rmfield(opts,'PDE');
        end
        
        %------------------------------------------------------------------
        % Extract variables
        %------------------------------------------------------------------

        function [var,opts] = ExtractVars(this,varName,opts)

            if(isfield(opts,'PDE'))
                PDE = opts.PDE;
            else
                PDE = true;
            end
            
            [~,nT] = GetPlotTimes(this);
                        
            if(~iscell(varName))
                varName = {varName};
            end

            opts.varName = varName;
            opts.nVars = length(varName);
            opts.xLabel = 'Opinion';     
                        
            nVars = opts.nVars;
            
            timeDependent = false(nVars,1);
            yMin = 0;
            yMax = 1;
            
            legendCell = {};
           
            for iVar = 1:nVars
                opts.name = [opts.name,'_',varName{iVar}];
                if(PDE)
                    var{iVar} = this.dynamicsResult.(varName{iVar}); %#ok
                else
                    var{iVar} = this.stocResult.dynamics.(varName{iVar}); %#ok
                end
                                
                if(size(var{iVar},2)>1)
                    nTi = size(var{iVar},2);
                    if(nTi ~= nT)
                        error('Variables must have plotTimes or 1 entries in time');
                    end
                    timeDependent(iVar) = true;
                else
                    timeDependent(iVar) = false;
                end
                
                yMax = max(yMax,max(var{iVar}(:)));
                yMin = min(yMax,min(var{iVar}(:)));
                
                legendCell = cat(2,legendCell,varName{iVar});
                
            end
            
            yMax = 1.1*yMax;
            
            opts.timeDependent = timeDependent;
                        
            if(~isfield(opts,'yLim'))
                if(isfield(opts,'yMin'))
                    yMin = opts.yMin;
                end
                if(isfield(opts,'yMax'))
                    yMax = opts.yMax;
                end
                opts.yLim = [yMin;yMax];
            end
            
            if(~isfield(opts,'legend'))
                opts.legend = legendCell;
            end
 
        end

        %------------------------------------------------------------------
        % Set plotting properties
        %------------------------------------------------------------------

        function optsOut = SetProps(~,opts,props,iVar)
            
            optsOut = opts;
            
            nProps = length(props);
            
            for iProp = 1:nProps
                prop = props{iProp};
                optsOut.(prop) = opts.(prop){iVar};
            end           
            
        end

        %------------------------------------------------------------------
        % Get plotTimes and number of plots
        %------------------------------------------------------------------
                
        function [plotTimes,nT] = GetPlotTimes(this)
            
            if(this.doPDE)
                plotTimes = this.dynamicsResult.t;
            else
                plotTimes = this.stocResult.dynamics.t;
            end
            
            nT = length(plotTimes);
        end
    
        function tPos = GetTPos(~,snapshotTimes,plotTimes)
            nT = length(snapshotTimes);
            tPos = zeros(1,nT);
            for iT = 1:nT
                [~,tPos(iT)] = min(abs(snapshotTimes(iT)-plotTimes));
            end            
        end
        
        %------------------------------------------------------------------
        % Set up plots (figure, axes, properties)
        %------------------------------------------------------------------
        
        function [hStruct,opts] = SetUpPlots(~,opts)
            
            props = {'linestyle','linecolor','linewidth'};
            defaults.linestyle = {'-'};
            defaults.linecolor = {'r'};
            defaults.linewidth = {1.5};
            
            if(~isfield(opts,'setColor'))
                if(isfield(opts,'linecolor'))
                    opts.setColor = true;
                else
                    opts.setColor = false;
                end
            end
            
            if(isfield(opts,'ha'))
                ha = opts.ha;
                axes(ha);
                hStruct.hf = gcf;
                hStruct.ha = ha;
            else
                if(isfield(opts,'Position'))
                    position = opts.Position;
                else
                    position = [100 100 800 800];
                end
                hStruct.hf = figure('Color','white','Position',position);
                hStruct.ha = axes;
            end
                        
            nProps = length(props);
            nVars = opts.nVars;
            for iProp = 1:nProps
                prop = props{iProp};
                if(isfield(opts,prop))
                    if(~iscell(opts.(prop)))
                        opts.(prop) = {opts.(prop)};
                    end
                    if(length(opts.(prop)) == 1)
                        opts.(prop) = repmat(opts.(prop),1,nVars);
                    end
                else
                    temp(1:nVars) = defaults.(prop);
                    opts.(prop) = temp;
                end
            end            

        end
           
        %------------------------------------------------------------------
        % Set limits, legend, labels, font
        %------------------------------------------------------------------

        function FixPlots(~,hStruct,opts)
                        
            ha = hStruct.ha;
            axes(ha);
            
            if(isfield(opts,'xLim'))
                xlim(opts.xLim);
            end
            
            if(isfield(opts,'yLim'))
                ylim(opts.yLim);
            end
            
            if(isfield(opts,'plain') && opts.plain)
                plain = true;
            else
                plain = false;
            end
            
            if(~plain)
                hl = legend('show');
                if(isfield(opts,'latex') && opts.latex)
                    set(hl, 'Interpreter','latex')
                end
                if(isfield(opts,'legPos'))
                    set(hl,'Location',opts.legPos);
                end


                if(isfield(opts,'yLabel'))
                    ylabel(opts.yLabel,'interpreter','latex');
                elseif(opts.nVars == 1 && isfield(opts,'varName'))
                    ylabel(opts.varName{1});
                end

                if(isfield(opts,'xLabel'))
                    xlabel(opts.xLabel,'interpreter','latex');
                end

                if(isfield(opts,'title'))
                    title(opts.title,'interpreter','latex');
                end
            end
            
            if(isfield(opts,'fontsize'))
                fontsize = opts.fontsize;
            else
                fontsize = 20;
            end
            if(isfield(opts,'linewidth'))
                if(iscell(opts.linewidth))
                    linewidth = opts.linewidth{1};
                else
                    linewidth = opts.linewidth;
                end
            else
                linewidth = 1.5;
            end
            
            set(ha,'fontsize',fontsize);
            set(ha,'linewidth',linewidth);
            
            %drawnow
        end
        
        %------------------------------------------------------------------
        % Saving plots
        %------------------------------------------------------------------

        function saveFile = SavePlot(this,fileName,hf)
            if(~exist(this.outputDir,'dir'))
                mkdir(this.outputDir);
            end
            saveFile = [this.outputDir filesep fileName '.pdf'];
            fprintf(1,['Saving pdf in' saveFile ' ...'])
            save2pdf(saveFile,hf);
            fprintf(1,' done.\n');
        end
            
        
    end
    
end