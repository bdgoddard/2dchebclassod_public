function ComputeOpinionDynamics(this) 

    optsPhys    = this.optsPhys;
    optsNum     = this.optsNum;
        
    plotTimes = optsNum.plotTimes;
    
    rho_ic = this.rhoI;
    rhoR = this.rhoR;
    
    Conv = this.Conv;

    relTol = optsNum.relTol;
    absTol = optsNum.absTol;

    toEq   = optsNum.toEq;
    if(toEq)
        eqTol  = optsNum.eqTol;
    end

    %----------------------------------------------------------------------
    % Initialisation
    %----------------------------------------------------------------------
    
    bound = this.IDC.Ind.bound;
    normal = this.IDC.Ind.normal;
    y = this.IDC.Pts.y;
    Dy = this.IDC.Diff.Dy;
    Int = this.IDC.Int;
    
    sigma = optsPhys.sigma;
    rhoR  = this.rhoR;
    
    %----------------------------------------------------------------------
    % Deal with even2periodic flag
    %----------------------------------------------------------------------
    
    if(this.even2periodic)
        rhsFn  = @drhoM_dt;
        mM     = ones(size(this.cut(y)));
    else
        rhsFn = @drho_dt;
        mM         = ones(size(y));
        mM(bound)  = 0;  % note that bound is zeros for periodic case
    end

    
    %---------------------------------------------------------------------%
    % Solve the PDE                                                       %
    %---------------------------------------------------------------------%
    
    optsF = v2struct(optsNum,optsPhys);
    ignoreList     = {'PlotArea'};
    optsOther.SkipLoadQuestion = true;
    
    [this.dynamicsResult,~,paramsDS] = DataStorage(this.dynamicsDir,...
                            @ComputeODDynamics,optsF,optsOther,[],ignoreList,false);
    this.dynamicsResult.t = plotTimes';
    this.FilenameDyn = paramsDS.Filename;
    
    function data = ComputeODDynamics(params,misc)
                
        if(toEq)
            optsODE = odeset('RelTol',relTol,'AbsTol',absTol,'Mass',diag(mM),'Events',@eventfun);
            [~,rho_t,tEq] = ode15s(rhsFn,plotTimes,rho_ic,optsODE);
            if(isempty(tEq))
                tEq = NaN;
            end
        else
            optsODE = odeset('RelTol',relTol,'AbsTol',absTol,'Mass',diag(mM));
            [~,rho_t] = ode15s(rhsFn,plotTimes,rho_ic,optsODE);
        end
            
        rho_t = rho_t.';
        
        if(toEq)
            nBeforeEq = size(rho_t,2);
            nSaves = length(plotTimes);
            if(nBeforeEq < nSaves)
                rho_t_Full = zeros(size(rho_t,1),length(plotTimes));
                rho_t_Full(:,1:nBeforeEq) = rho_t;
                for iT = nBeforeEq+1:nSaves
                    rho_t_Full(:,iT) = rho_t(:,nBeforeEq);
                end
                rho_t = rho_t_Full;
            end            
            data = v2struct(rho_t,plotTimes,rhoR,tEq);
        else
            data = v2struct(rho_t,plotTimes,rhoR);
        end
        
    end
        
    %----------------------------------------------------------------------
    % RHS of PDE
    %----------------------------------------------------------------------

    function drhodt = drho_dt(t,rho)
        
        j = flux(rho);
        drhodt  = - Dy*j;
        
        drhodt(bound) = normal*j;
                
    end

    function drhoMdt = drhoM_dt(t,rhoM)
                
        rho = this.mirror(rhoM);

        j = flux(rho);
        drhodt  = - Dy*j;
                
        drhoMdt = this.cut(drhodt);        

    end

    function j = flux(rho)        
        j = - ( sigma^2/2 * Dy*rho + rho.*(Conv*(rho+rhoR)) );
    end

    %----------------------------------------------------------------------
    % Equilibrium event function
    %----------------------------------------------------------------------

    function [err,isterm,dir] = eventfun(t,rho)
        drhodt = rhsFn(t,rho);
        err = max(abs(drhodt)) - eqTol;
        isterm = 1;
        dir = 0;
    end


end