function rhoR = OD_Radicals_TwoGaussians(y,params)        
%rhoR = lambda* exp( -(y - y0_1).^2 / (2*sigma_1^2)) + (1-lambda)* exp( -(y - y0_2).^2 / (2*sigma_2^2));

    y0_1    = params.y0_1;
    sigma_1 = params.sigma_1;
    y0_2    = params.y0_2;
    sigma_2 = params.sigma_2;
    lambda  = params.lambda;
    
    yTilde_1 = y-y0_1;
    yTilde_2 = y-y0_2;
    
    if(isfield(params,'periodic') && params.periodic)
        yTilde_1 = periodicDistance(yTilde_1,params.periodicL);
        yTilde_2 = periodicDistance(yTilde_2,params.periodicL);
    end

    rhoR = lambda*exp( -yTilde_1.^2 / (2*sigma_1^2)) + (1-lambda)*exp( -yTilde_2.^2 / (2*sigma_2^2));
end
