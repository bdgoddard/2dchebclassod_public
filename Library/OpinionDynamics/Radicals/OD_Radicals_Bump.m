function rhoR = OD_Radicals_Bump(y,params)        

    if(nargin == 1)
        params = struct;
    end

    if(isfield(params,'epsR'))
        epsR = params.epsR;
    else
        epsR = 0.1;
    end
    
    if(isfield(params,'y0'))
        y0 = params.y0;
    else
        y0 = 0.7;
    end
    
    z = abs(y-y0)/epsR;
    rhoR = zeros(size(y));
    mask = (y>y0-epsR) & (y<y0+epsR) ;
    z = z(mask);
    rhoR(mask) = exp(- 1./(1-z.^2));

end
