function rhoR = OD_Radicals_Triangle(y,params)        

    S = params.S;
    M = params.M;
    A = params.A;

    yTilde = y-A;
    if(isfield(params,'periodic') && params.periodic)
        yTilde = periodicDistance(yTilde,params.periodicL);
    end
    
    rhoR = M*(S - abs(yTilde))/S^2;
    rhoR(rhoR<0) = 0;

end
