function rhoR = OD_Radicals_Gaussian(y,params)        
%rhoR = exp( -(y - y0).^2 / (2*sigma^2));

    y0    = params.y0;
    sigma = params.sigma;
    
    yTilde = y-y0;
    
    if(isfield(params,'periodic') && params.periodic)
        yTilde = periodicDistance(yTilde,params.periodicL);
    end

    rhoR = exp( -yTilde.^2 / (2*sigma^2));
end
