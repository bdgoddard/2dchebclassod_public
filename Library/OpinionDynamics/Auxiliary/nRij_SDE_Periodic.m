function nRij = nRij_SDE_Periodic(x,opts)
%F_SDE_PERIODIC calculates the interaction term for the SDE solver
% with periodic boundary conditions
%
% Inputs:
% x - vector of positions
% opts - structure containing physical parameters
%        - R; confidence bound
%        - nParticles; number of particles
%        - L; length of the interval
%
% Outputs: 
% nRif - vector giving number of particles within R of particle i

    % physical parameters
    R = opts.R;
    nParticles = opts.nParticles;
    L = opts.L;

    % get signed distance between all pairs without radicals
    RijFull = getRijSigned1DPeriodic(x(1:nParticles),L);

    nRij = nRij_SDE_General(RijFull,R);
    
end
