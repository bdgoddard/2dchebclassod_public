function F = F_SDE_Even2Periodic(x,opts)
%F_SDE_EVEN2PERIODIC calculates the interaction term for the SDE solver
% with even 2-periodic boundary conditions
%
% Inputs:
% x - vector of positions
% opts - structure containing physical parameters
%        - R; confidence bound
%        - nParticles; number of particles
%        - L; length of the interval
%
% Outputs: 
% F   - interaction term

    % physical parameters
    R = opts.R;
    nParticles = opts.nParticles;
    L = opts.L; % should be twice the length of the physical region

    % make even
    x = [x;-x];
        
    % get signed distance between all pairs
    RijFull = getRijSigned1DPeriodic(x,L);

    F = F_SDE_General(RijFull,R,nParticles);
    
    % cut down due to even 2-periodic
    F = F(1:end/2);
    
end
