function data = SampleStocInitialCondition(opts,~) 
%SAMPLESTOCINITIALCONDITION samples an initial condition for stochastic
%simulations
%
% Inputs:
% opts - structure containing
%         - params: parameters for rho
%         - rhoFn: string function name of density to be sampled
%         - yMin; yMax: interval bounds
%         - nParticles: number of particles
%         - nSamples: number of samples to compute for each particle
%         - thin; burnin; options for slice sampling
%         - initialGuess: vector of initial guess for slice sampling
%
% Outputs:
% data - structure containing 
%         - x: matrix of samples and opts
%         - opts: copy of input opts

    %----------------------------------------------------------------------
    % Initialisation
    %----------------------------------------------------------------------      
     
    rho = str2func(opts.rhoFn);
    params = opts.params;
    yMin = opts.yMin;
    yMax = opts.yMax;
    thin = opts.thin;
    burnin = opts.burnin;
    nSamples = opts.nSamples;
    nParticles = opts.nParticles;
    initialGuess = opts.initialGuess;
    
    % compute nSamples for each particle
    nSamplesTotal = nSamples*nParticles;

    %---------------------------------------------------------------------%
    % Do Sampling                                                         
    %---------------------------------------------------------------------%
    
    tic
        x = myslicesample(initialGuess,nSamplesTotal,@logpdf,thin,burnin);  
        % reshape data to a useful size
        x = reshape(x,nParticles,nSamples);
    toc

    % set output
    data.x = x;
    data.opts = opts;
    
    %--------------------------------------------------------------------------
    % logpdf function generated from rho
    %--------------------------------------------------------------------------
    function logp = logpdf(x)
        % only sample in the interval
        if(any(x<yMin) || any(x>yMax))
            logp = -inf;
        else
            logp= log(rho(x,params)); % inside the interval sample rho
        end
    end % logpdf
    

end