function F = F_SDE_Periodic(x,opts)
%F_SDE_PERIODIC calculates the interaction term for the SDE solver
% with periodic boundary conditions
%
% Inputs:
% x - vector of positions
% opts - structure containing physical parameters
%        - R; confidence bound
%        - nParticles; number of particles
%        - L; length of the interval
%
% Outputs: 
% F   - interaction term

    % physical parameters
    R = opts.R;
    nParticles = opts.nParticles;
    L = opts.L;

    % get signed distance between all pairs
    RijFull = getRijSigned1DPeriodic(x,L);
    
    F = F_SDE_General(RijFull,R,nParticles);
end
