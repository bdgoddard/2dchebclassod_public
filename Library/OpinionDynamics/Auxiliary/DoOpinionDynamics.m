function OD = DoOpinionDynamics(opts,~)
%DOOPINIONDYNAMICS wrapper to run an OpinionDynamics computation
%
% Inputs:
% opts - structure of options to pass to OpinionDynamics
%
% Outputs:
% OD - the resulting OpinionDynamics

    % create OpinionDynamics
    OD = OpinionDynamics(opts);
    
    % run dynamics
    OD.ComputeDynamics;
    
    % compute order paramter and mass
    OD.ComputeOrderParameter;
    OD.ComputeMass;
    
    % optionally compute mean and mode opinions
    if(isfield(opts,'doMean') && opts.doMean)
        OD.ComputeMeanOpinion;
    end
    if(isfield(opts,'doMode') && opts.doMode)
        OD.ComputeModeOpinion;
    end
        
    % do stochastic computations
    if(isfield(opts,'optsStoc'))
        OD.ComputeStocDynamics;
        OD.ComputeStocOrderParameter;
    end

end