function FixTightSubplots(ha,hacb,optsPlot)
%FIXTIGHTSUBPLOTS neatens up TightSubplots
%
% Inputs:
% ha       - TightSubplot axes handles
% hacb     - optional TightSubplots colorbar axes handles
% optsPlot - structure containing:
%             - (optional) colorbar: flag to add extra colorbar
%             - (optional) keepColorbars: flag to keep colorbars on
%             individual plots
%             - (optional) keepXLabels: flag to keep x labels on plots
%             other than the bottom row
%             - (optional) keepYLabels: flag to keep y labels on plots
%             other than the leftmost
%             - (optional) title: cell of titles for columns
%             - (optional) square: flag to force square axes
%             - (optional) legendType: string to determine which legends to
%             keep.  Either 'off' to turn all off, 'first' to keep first
%             in a row, or 'right' to keep one outside to the right
%             - (optional) annotation: string to add as label in top left
%             of figure

    % deal with no options
    if(nargin<2)
        optsPlot = struct;
    end
        
    % number of rows and columns of axes to fix
    nRows = size(ha,1);
    nCols = size(ha,2);
    
    % deal with colorbars
    for iRow = 1:nRows
        % colorbar flags to add a separate colorbar
        if(isfield(optsPlot,'colorbar') && optsPlot.colorbar)
            
            % get limits for colorbar by running over all colorbars in the
            % row
            hcOld = ha(iRow,1).Colorbar;
            hc    = hacb(iRow,1).Colorbar;
            if(~isempty(hcOld) && isempty(hc))
                cMin = NaN;
                cMax = NaN;
                for iCol = 1:nCols
                    cLim = get(ha(iRow,iCol),'CLim');
                    cMin = min(cMin,cLim(1));
                    cMax = max(cMax,cLim(2));
                end
                cLim = [cMin,cMax];
                caxis(hacb(iRow,1),cLim);
                
                % update colormap
                cMap = colormap(ha(iRow,1));
                colormap(hacb(iRow,1),cMap);
                
                % update color limits
                hc = colorbar(hacb(iRow,1));
                hc.Limits = hcOld.Limits;
            end
        end
                
        % turn off colorbars for individual plots
        if(~isfield(optsPlot,'keepColorbars') || ~optsPlot.keepColorbars)
            for iCol = 1:nCols
                colorbar(ha(iRow,iCol),'off');
            end
        end
    end

    % turn off x labels in all but bottom row
    if(~isfield(optsPlot,'keepXLabels') || ~optsPlot.keepXLabels)
        for iCol = 1:nCols
            for iRow = 1:nRows-1
                xlabel(ha(iRow,iCol),'');
                xticklabels(ha(iRow,iCol),'');
            end 
        end
    end          
    
    % determine which y labels to keep
    if(isfield(optsPlot,'keepYLabels'))
        keepYLabels = optsPlot.keepYLabels;
        if(length(keepYLabels)==1)
            keepYLabels = repmat(keepYLabels,nRows,nCols);
        elseif(length(keepYLabels)<nCols)
            error('length(keepYLabels) must be 1 or nRows x nCols');
        end 
    else % default is just to keep the leftmost
        keepYLabels = false(nRows,nCols);
        keepYLabels(:,1) = true;
    end

    
    % turn off y labels
    for iRow = 1:nRows
        for iCol = 2:nCols
            ylabel(ha(iRow,iCol),'');
            if(~keepYLabels(iRow,iCol))
                yticklabels(ha(iRow,iCol),'');
            end
        end 
    end
    
    % add titles
    if(isfield(optsPlot,'title'))
        for iCol = 1:nCols
            title(ha(1,iCol),optsPlot.title(iCol));
        end
    end
            
    % set axes to square
    if(isfield(optsPlot,'square') && optsPlot.square)
        for iRow = 1:nRows
            for iCol = 1:nCols
                axis(ha(iRow,iCol),'square');
            end
        end    
    end
    
    % turn off legends
    if(isfield(optsPlot,'legendType'))
        leg = optsPlot.legendType;
        
        % deal with one input
        if(~iscell(leg))
            leg = {leg};
        end
        
        % replicate over rows, if needed
        if(length(leg)<nRows)
            legTemp = leg{1};
            leg = cell(1, nCols);
            leg(:) = legTemp;
        end
        
        %  off - all off
        %  first - just keep first
        %  right - add one outside the axes on the right
        nLegsRow = zeros(nRows,1); % keep track of the number of legends in each row
        for iRow = 1:nRows
            
            if(strcmp(leg{iRow},'off'))
                legMinOff = 1;
                legMaxOff = nCols;
            elseif(strcmp(leg{iRow},'first'))
                legMinOff = 2;
                legMaxOff = nCols;
                nLegsRow(iRow) = 1;
            elseif(strcmp(leg{iRow},'last'))
                legMinOff = 1;
                legMaxOff = nCols-1;
                nLegsRow(iRow) = 1;                
            elseif(strcmp(leg{iRow},'right'))
                legMinOff = 1;
                legMaxOff = nCols-1;
                nLegsRow(iRow) = 1;
            else
                legMinOff = nCols+1; % fudge keeping them all
                legMaxOff = nCols;
                nLegsRow(iRow) = nCols;
            end
            
            for iCol = legMinOff:legMaxOff
                legend(ha(iRow,iCol),'off')
            end
            
        end

        % move legends to the outside for 'right'
        legends = findobj(gcf,'type','Legend');
        legends = flip(legends); % legends are stored backwards
        iLegend = 0;
        for iRow = 1:nRows
            iLegend = iLegend + nLegsRow(iRow); % count through legends
            if(strcmp(leg{iRow},'right'))
                hLeg = legends(iLegend); 
                set(hLeg,'Units','points');
                hPos = get(hLeg,'Position');
                set(ha(iRow,nCols),'Units','points');
                aPos = get(ha(iRow,nCols),'Position');
                set(hLeg,'Units','points','Position',[aPos(1)+aPos(3)+20,hPos(2),hPos(3),hPos(4)]);
            end
        end
    end
       
    % add text annotation in top left
    if(isfield(optsPlot,'annotation'))
        hf = get(ha(1,1),'Parent');
        pos = get(hf,'Position');
        axW = pos(3);
        axH = pos(4);

        aX = 35;
        aY = 145;
        
        if(isfield(optsPlot,'fontsize'))
            fontsize = optsPlot.fontsize;
        else
            fontsize = 20;
        end
        
        annotation('textbox',[aX/axW, 1-aY/axH, .25, .15],'String',optsPlot.annotation, ...
            'EdgeColor','none','FitBoxToText','on','FontSize',fontsize);
    end
   
    
end
