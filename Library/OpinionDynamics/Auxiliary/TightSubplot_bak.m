function [ha,hacb,hf,optsPlot] = TightSubplot(optsPlot)
%TIGHTSUBPLOT creates a grid of axes of fixed size with spacing determined
%by options in optsPlot
%
% Inputs:
% optsPlot - structure containing:
%             - nRows, nCols: number of rows and columns of axes
%             - (optional) colorbar: flag to add extra axes for color bar
%             - (optional) wide: flag to make plots wide
%             - (optional) keepXLabels: flag to add extra space to keep x
%             labels on rows other than the bottom one
%             - (optional) keepYLabels: flag to add extra space to keep y
%             labels on columns other than the leftmost one
%             - (optional) nColors: number of colours in color order
%             - (optional) linewidth: default line width
%
% Outputs:
% ha       - axes handles
% hacf     - axes handles for (optional) invisible axes for overall color bar
%            for each row
% hf       - figure handle
% optsPlot - copy of input


    % number of rows and columns of axes to create
    nRows = optsPlot.nRows;
    nCols = optsPlot.nCols;

    % default margins and size of axes
    marX = 100;
    marY = 70;
    if(isfield(optsPlot,'wide') && optsPlot.wide)
        axW = 570;
    else
        axW = 380;
    end
    axH = 380;
    margin = 35;
            
    % extra space if we want to add a colorbar on the right
    if(isfield(optsPlot,'colorbar') && optsPlot.colorbar)
        cbar = 50;
    else
        cbar = 0;
    end
    
    % extra space if we want to keep x labels
    if(isfield(optsPlot,'keepXLabels') && optsPlot.keepXLabels)
        xlabel = 35;
    else
        xlabel = 0;
    end
    
    % determine which y labels to keep
    if(isfield(optsPlot,'keepYLabels'))
        keepYLabels = optsPlot.keepYLabels;
        if(length(keepYLabels)==1)
            keepYLabels = repmat(keepYLabels,nRows,nCols);
        elseif(length(keepYLabels)<nCols)
            error('length(keepYLabels) must be 1 or nRows x nCols');
        end 
    else
        keepYLabels = false(nRows,nCols);
    end
    keepYLabels = any(keepYLabels,1);
    % extra space if we want to keep y labels
    yLabelSpace = 10;

    
    % figure width and height
    fw = 2*marX + nCols*axW + (nCols-1)*margin + cbar;
    fh = 2*marY + nRows*axH + (nRows-1)*xlabel;
    
    % create figure
    hf = figure('Position',[150, max(0,(2-nRows)*axH), fw, fh]);
    
    % create color order
    if(isfield(optsPlot,'nColors'))
        colorOrder = linspecer(optsPlot.nColors,'sequential');
    else
        colorOrder = linspecer(8,'sequential');
    end
    set(hf,'defaultAxesColorOrder',colorOrder);
    
    % create axes
    posY = marY;
    for iRow = nRows:-1:1 % axes are labelled from top row
        posX = marX;
        for iCol = 1:nCols
            if(keepYLabels(iCol) && (iCol>1)) % add space for y labels
                posX = posX + yLabelSpace;
            end
            ha(iRow,iCol) = axes('Units','points','Position',[posX; posY; axW; axH]);
            posX = posX + axW + margin;
        end
        posY = posY + axH + margin + xlabel;
    end
    
    % create invisible axes for a colorbar on the right
    if(isfield(optsPlot,'colorbar') && optsPlot.colorbar)
        for iRow = nRows:-1:1
            posTemp = get(ha(iRow,nCols),'Position');
            posTemp(1) = posTemp(1) + 70;
            hacb(iRow,1) = axes('Units','points','Position',posTemp);
            set(hacb(iRow,1),'fontsize',20);
            set(hacb(iRow,1),'linewidth',1.5);
            hacb(iRow,1).Visible = 'off';
        end
    else
        hacb = [];
    end    
    
    % set line width
    if(isfield(optsPlot,'linewidth'))
        linewidth = optsPlot.linewidth;
    else
        linewidth = 1.5;
    end
    
    % set default font size and line width
    for iRow = 1:nRows
        for iCol = 1:nCols
            set(ha(iRow,iCol),'fontsize',20);
            set(ha(iRow,iCol),'linewidth',linewidth);
        end
    end
    
end