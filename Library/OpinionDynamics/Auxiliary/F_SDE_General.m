function F = F_SDE_General(Rij,R,nParticles)
%F_SDE_GENERAL calculates the interaction term for the SDE solver
%
% Inputs:
% Rij        - matrix of signed particle separations
% R          - confidence bound
% nParticles - number of particles
%
% Outputs: 
% F    - interaction term for all particles, including radicals and
%        'mirror' particles

    % exclude particles that are too far apart
    %  Rij(abs(Rij)>R) = 0; 
    
    % efficient way
    Rij = ( min(abs(Rij)-R,0) ) .* sign(Rij);
    Rij = Rij - (R * sign(Rij));
         
    sumRij = sum(Rij,2); % for row i, sum over x_i - x_j
    F = (sumRij/nParticles); % compute force
    
end
