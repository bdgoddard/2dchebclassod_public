function nRij = nRij_SDE_Even2Periodic(x,opts)
%F_SDE_EVEN2PERIODIC calculates the interaction term for the SDE solver
% with even 2-periodic boundary conditions
%
% Inputs:
% x - vector of positions
% opts - structure containing physical parameters
%        - R; confidence bound
%        - nParticles; number of particles
%        - L; length of the interval
%
% Outputs: 
% nRif - vector giving number of particles within R of particle i

    % physical parameters
    R = opts.R;
    nParticles = opts.nParticles;
    L = opts.L; % should be twice the length of the physical region

    % remove redicals
    x = x(1:nParticles);
    % make even
    x = [x;-x];
        
    % get signed distance between all pairs
    RijFull = getRijSigned1DPeriodic(x,L);

    % get number within R
    nRij = nRij_SDE_General(RijFull,R);
        
    % cut down due to even 2-periodic
    nRij = nRij(1:end/2); % this accounts for the factor of 1/2 in the paper definition
    
end
