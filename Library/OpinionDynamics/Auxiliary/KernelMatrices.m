function convStruct = KernelMatrices(opts,optsOther)
%KERNELMATRICES Compute kernel matrices (truncated convolutions)
%
% Inputs:
% opts      - structure including:
%             - convFn: string name of function used to compute convolution
%             - kernel: string name of function that evaluates kernel
%             - (optional) params: structure of parameters to pass to
%             kernel
%             - (optional) shapeParams: structure of parameters to
%             determine interval over which to evaluate convolution
%
% optsOther - structure including:
%              - IDC: class of type Interval
%
% Outputs:
% convStruct - stucture containing convolution information

    % interval
    IDC = optsOther.IDC;

    % convert strings to functions
    convFn = str2func(opts.convFn);
    kernel = str2func(opts.kernel);
    
    % set optional parameters for kernel function
    if(isfield(opts,'params'))
        params = opts.params;
    else
        params = struct;
    end
    
    % compute kernel convolution
    if(isfield(opts,'shapeParams'))
        Conv = convFn(IDC,@KernelUniversal,opts.shapeParams);  
    else
        Conv = convFn(IDC,@KernelUniversal);  
    end
    
    convStruct.Conv = Conv;
    
%--------------------------------------------------------------------------

    % auxiliary function to allow passing of parameters to convolution
    % computation
	function z = KernelUniversal(r)                 
        if(nargin(kernel)==1)
            z = kernel(r);   
        else
            z = kernel(r,params);   
        end
    end


end
