function [ha,haOut,hacb,hf] = AxesInAxes(optsPlot)
% AXESINAXES creates a grid of axes inside a given axes
%
% Inputs:
% optsPlot - structure containing:
%            - nRows, nCols: number of rows and columns of 'outer' axes
%            - nSubRows, nSubCols: number of rows and columns of 'sub-axes
%            to create inside the 'outer' ones
%            - (optional) haOut: axes handles for 'outer' axes
%
% Outputs:
% ha      - nSubRows x nSubCols xnRows x nCols handles of sub-axes
% haOut   - handles for 'outer' axes
% hacb    - handle for colorbar axes from TightSubplot
% ha      - figure handle

% use given outer axes, or construct some
if(isfield(optsPlot,'haOut'))
    haOut = optsPlot.haOut;
    axes(haOut);
    hf = gcf;
    if(isfield(optsPlot,'hacb'))
        hacb = optsPlot.hacb;
    else
        hacb = [];
    end
else
    [haOut,hacb,hf] = TightSubplot(optsPlot);
end

% number of rows and columns of outer axes
nRows = optsPlot.nRows;
nCols = optsPlot.nCols;

% number of rows and columns of sub-axes
nSubRows = optsPlot.nSubRows;
nSubCols = optsPlot.nSubCols;

% get size of sub-axes
temp = haOut(1).Position;
axW = temp(3);
axH = temp(4);

axSubW = axW/nSubCols;
axSubH = axH/nSubRows;

% preallocate axes handles
ha = zeros(nSubRows,nSubCols,nRows,nCols);

% create sub-axes
for iRow = nRows:-1:1 % gives sensible labelling so that the top row is row 1
    for iCol = 1:nCols
        axPos = haOut(iRow,iCol).Position;
        posX = axPos(1);
        posY = axPos(2);
        for iSubRow = 1:nSubRows
            for iSubCol = 1:nSubCols
                ha(iSubRow,iSubCol,iRow,iCol) = axes('Units','points','Position',[posX; posY; axSubW; axSubH]);
                posX = posX + axSubW;
            end
            posX = axPos(1);
            posY = posY + axSubH;
        end
    end
end

end