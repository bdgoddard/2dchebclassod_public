function save2png(pngFileName,handle,dpi)
%SAVE2PNG save figure to cropped png file
%
% Inputs:
% pngFileName - save file name
% handle      - figure handle to save
% dpi         - (optional) resolution

if(nargin<3)
    dpi=[];
end

% Backup previous settings
prePaperType = get(handle,'PaperType');
prePaperUnits = get(handle,'PaperUnits');
preUnits = get(handle,'Units');
prePaperPosition = get(handle,'PaperPosition');
prePaperSize = get(handle,'PaperSize');

% Make changing paper type possible
set(handle,'PaperType','<custom>');

% Set units to all be the same
set(handle,'PaperUnits','inches');
set(handle,'Units','inches');

% Set the page size and position to match the figure's dimensions
position = get(handle,'Position');
set(handle,'PaperPosition',[0,0,position(3:4)]);
set(handle,'PaperSize',position(3:4));

% Save figure as a png file
if(isempty(dpi))
    print(handle,'-dpng',pngFileName)
else
    print(handle,'-dpng',['-r' num2str(dpi)],pngFileName)
end

% Restore the previous settings
set(handle,'PaperType',prePaperType);
set(handle,'PaperUnits',prePaperUnits);
set(handle,'Units',preUnits);
set(handle,'PaperPosition',prePaperPosition);
set(handle,'PaperSize',prePaperSize);

% Remove border
toCrop = imread(pngFileName);

% max is sum of [255,255,255]
xSum=sum(sum(toCrop,3),1);
ySum=sum(sum(toCrop,3),2);

margin = 0;

% find first/last rows/columns with non-white pixels
leftEdge   = max(1,find(xSum<max(xSum),1,'first')-margin);
rightEdge  = min(length(xSum),find(xSum<max(xSum),1,'last')+margin);
topEdge    = max(1,find(ySum<max(ySum),1,'first')-margin);
bottomEdge = min(length(ySum),find(ySum<max(ySum),1,'last')+margin);

% save cropped png
cropped = toCrop(topEdge:bottomEdge,leftEdge:rightEdge,:);
imwrite(cropped,pngFileName);