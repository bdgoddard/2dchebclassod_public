function Rij = getRijSigned1D(x)
% GETRIJSIGNED Compute the (signed) distance between all pairs in the input vector
%
% Inputs:
% x - vector of positions
%
% Outputs:
% Rij - matrix of signed distances, Rij is x_i - x_j

    % efficient version
    Rij = bsxfun(@minus,x,x');

%     Ones = ones(1,length(x));
%     x = x(:); % column vector of positions
%     y = x';   % row vector of positions
%     
%     % (quick version of repmat with no error checking)
%     X = x(:,Ones);  % square matrix where each column is x
%     Y = y(Ones,:);  % square matrix where each row is y
%     
%     Rij = X-Y;      % compute distance
end