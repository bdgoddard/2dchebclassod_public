function F = F_SDE_NoFlux(x,opts)
%F_SDE_NOFLUX calculates the interaction term for the SDE solver
% with no-flux conditions
%
% Inputs:
% x - vector of positions
% opts - structure containing physical parameters
%        - R; confidence bound
%        - nParticles; number of particles
%        - L; length of the interval
%
% Outputs: 
% F   - interaction term

    % physical parameters
    R = opts.R;
    nParticles = opts.nParticles;

    % get signed distance between all pairs
    RijFull = getRijSigned1D(x);
    
    F = F_SDE_General(RijFull,R,nParticles);
end
