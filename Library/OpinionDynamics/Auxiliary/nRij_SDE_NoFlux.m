function nRij = nRij_SDE_NoFlux(x,opts)
%F_SDE_NOFLUX calculates the interaction term for the SDE solver
% with no-flux conditions
%
% Inputs:
% x - vector of positions
% opts - structure containing physical parameters
%        - R; confidence bound
%        - nParticles; number of particles
%        - L; length of the interval
%
% Outputs: 
% nRif - vector giving number of particles within R of particle i

    % physical parameters
    R = opts.R;
    nParticles = opts.nParticles;

    % get signed distance between all pairs without radicals
    RijFull = getRijSigned1D(x(1:nParticles));
    
    nRij = nRij_SDE_General(RijFull,R);
    
end
