function Rij = getRijSigned1DPeriodic(x,L)
% GETRIJSIGNEDPeriodic Compute the (signed) distance between all pairs in 
% the input vector in a periodic interval of length L
%
% Inputs:
% x - vector of positions
% L - length of periodic interval
%
% Outputs:
% Rij - matrix of signed periodic distances, Rij is x_i - x_j

    % get distances in infinite domain
    Rij = getRijSigned1D(x);
    
    % deal with periodicity
    Rij = periodicDistance(Rij,L);
        
%     % deal with periodicity
%     mask = ( abs(Rij) > L/2 ); % pairs which have a shorter distance
%                                % 'the other way around the circle'
%     Rij(mask) = Rij(mask) - sign(Rij(mask))*L; % modify those distances
end

