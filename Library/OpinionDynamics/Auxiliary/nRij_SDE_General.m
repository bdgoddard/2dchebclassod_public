function nRij = nRij_SDE_General(Rij,R)
%F_SDE_GENERAL calculates the interaction term for the SDE solver
%
% Inputs:
% Rij    - matrix of signed particle separations (excluding radicals)
% R      - confidence bound
%
% Outputs: 
% nRif - vector giving number of particles within R of particle i,


    nRij = sum(abs(Rij)<=R,2); 
    % find those in the confidence interval
    %nRij = ( abs(Rij)<=R );
    % compute the number of such particles
    %nRij = sum(nRij,2); 
    
end
