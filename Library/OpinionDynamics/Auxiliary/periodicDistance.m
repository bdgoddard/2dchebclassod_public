function d = periodicDistance(d,L)
%PERIODICDISTANCE Get periodic distance on interval of length L, 
% given distances in R    
% 
% Inputs:
% d - matrix of standard distances
% L - length of periodic interval
%
% Outputs:
% matrix - periodic distances

    % efficient version
    % elements with abs > L/2 assigned to 1 otherwise 0
    % multiply by sign to get original sign of distance
    
    d = d - sign(d).*(abs(d)>L/2)*L;
   
%     mask = ( abs(d) > L/2 ); % pairs which have a shorter distance
%                              % 'the other way around the circle'
%     dMask = d(mask);
%     d(mask) = dMask - sign(dMask)*L; % modify those distances
        
end
