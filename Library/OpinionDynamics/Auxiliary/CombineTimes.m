function allTimes = CombineTimes(inTimes1,inTimes2)
% COMBINETIMES combines two vectors of times and ouputs their (unique'd)
% union, sorted in increasing time

    allTimes = [inTimes1(:);inTimes2(:)];
    allTimes = sort(unique(allTimes));
    allTimes = allTimes';
    
end