function MOD = DoMultiOpinionDynamics(opts,~)
%DOMULTIOPINIONDYNAMICS wrapper to run a MultiOpinionDynamics computation
%
% Inputs:
% opts - structure of options to pass to MultiOpinionDynamics
%
% Outputs:
% MOD - the resulting MultiOpinionDynamics

    % create MultiOpinionDynamics
    MOD = MultiOpinionDynamics(opts);
    
    % do dynamics and compute order parameter and mass
    MOD.ComputeDynamics;
    MOD.ComputeOrderParameter;
    MOD.ComputeMass;
    
    % optionally compute mean and mode opinions
    if(isfield(opts,'doMean') && opts.doMean)
        MOD.ComputeMeanOpinion;
    end
    if(isfield(opts,'doMode') && opts.doMode)
        MOD.ComputeModeOpinion;
    end
        
    % do stochastic simulations
    if(isfield(opts,'optsStoc') || (isfield(opts,'OD') && isfield(opts.OD,'optsStoc')) )
        MOD.ComputeStocDynamics;
        MOD.ComputeStocOrderParameter;
    end
    
    fprintf('Saving ... ');
    
end