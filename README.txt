# README #

2DChebClassOD is an extension to the existing 2DChebClass library developed by Ben Goddard for the solution of models arising in opinion dynamics.

2DChebClass is a code Andreas Nold and Ben Goddard developed as a project for DFT and DDFT computations in 2012 at Imperial College London. Since then, it evolved into a library of classes and functions to solve 1D and 2D DFT and DDFT problems.  Its use is not restricted to DFT and DDFT - it can be used to solve a wide range of (integro)-PDE systems in various 1D and 2D geometries.


### How do I run tests? ###

Run any file in the subdirectories of Library/OpinionDynamics/Computations/FinalFigures

### How do I write my own examples? ###

See the guidance in PDECO Input Options.pdf

### Change Data Folder ###
 
Please open "AddPaths.m" in the main folder. Define via "dirData" a folder where the computational results should be saved. 

### Matlab-Versions

The code currently runs using Matlab versions from Matlab2016a to Matlab2020a.

### Contact ###

Ben Goddard
b.goddard@ed.ac.uk